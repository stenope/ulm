unit gspec;

{$MODE Delphi}

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     jmath;

type
  tform_spec = class(TForm)
    label_x_var: TLabel;
    edit_var: TEdit;
    label_expo: TLabel;
    edit_expo: TEdit;
    Label1: TLabel;
    button_run: TButton;
    button_init: TButton;
    edit_nb_val: TEdit;
    procedure erreur(s : string);
    procedure init_spec;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure button_runClick(Sender: TObject);
    procedure button_initClick(Sender: TObject);
    procedure run_spec(x,n : integer);
    function  check_param : boolean;
  private
    x_var : integer; { variable pour spectre }
    expo  : integer; { exposant 2^expo = nb_val }
    nb_val : integer;
  public
  end;

var form_spec: tform_spec;

implementation

uses jglobvar,jutil,jsyntax,jsymb,jeval,
     ggraph,gulm;

{$R *.lfm}

procedure tform_spec.erreur(s : string);
begin
  erreur_('Spectrum - ' + s);
end;

procedure tform_spec.FormCreate(Sender: TObject);
begin
  Left   := 0;
  Top    := 0;
  Height := 138;
  Width  := 244;
  adjust(self);
  Caption := 'SPECTRUM';
  x_var := 0;
  expo  := 10;
  nb_val := 1024;
end;

procedure tform_spec.init_spec;
begin
  with form_graph do x_var := vargraph_y[1];
end;

procedure spectre(x,n : integer;var p : vecvec_type);
{ spectre de puissance de la variable x sur n points }
{ spectre retourne dans le tableau p                 }
var  i,isign,nn,j,n4 : integer;
     den,facn,facp,sumw : extended;
     tab : vecvec_type;

function window(i : integer;facn,facp : extended) : extended;
begin
  window := 1.0 - abs(((i-1) - facn)*facp);  { Parzen }
end;

begin
  nn := n + n;
  n4 := nn + nn;
  facn := n - 0.5;
  facp := 1.0/(n + 0.5);
  sumw := 0.0;
  for i := 1 to nn do sumw := sumw + sqr(window(i,facn,facp));
  SetLength(tab,n4+1);
  for i := 1 to n4 do tab[i] := 0.0;
  tab[1] := variable[x].val*window(0,facn,facp);
  for i := 1 to n-1  do
    begin
      run_t;
      tab[i+i+1] := variable[x].val*window(i,facn,facp);
      with form_ulm do
        begin
          procproc;
          if runstop then
            begin
              runstop := false;
              exit;
            end;
          if ( t__ mod 100 = 0 ) then status_time;
        end;
    end;
  isign := 1;
  fft(tab,nn,isign);
  den := nn*sumw;
  p[0] := sqr(tab[1]) + sqr(tab[2]);
  for i := 2 to n do
    begin
      j := i + i;
      p[i-1] := sqr(tab[j]) + sqr(tab[j-1]) + sqr(tab[n4+4-j]) + sqr(tab[n4+3-j]);
    end;
  for i := 0 to n-1 do p[i] := p[i]/den;
end;

procedure tform_spec.FormActivate(Sender: TObject);
begin
  edit_var.Text  := s_ecri_var(x_var);
  edit_expo.Text := IntToStr(expo);
  edit_nb_val.Text := IntToStr(nb_val);
end;

procedure tform_spec.run_spec(x,n : integer);
var i,ms : integer;
    a,b : extended;
    p : vecvec_type;
    f : vecvec_type;
begin
  with form_ulm do status_run('Spectrum ' + IntToStr(n));
  ms := clock;
  SetLength(p,n);
  spectre(x,n,p);
  with form_graph do
    begin
      status_spec(x,n);
      for i := 0 to n-1 do
        begin
          valgraph_x[i] := i/(n+n);
          p[i] := ln0(p[i]);
          valgraph_y[1][i] := p[i]/ln(10.0);
          { p[i] correspond a la frequence i/2*nb_val }
        end;
      if not gplus then efface(nil);
      gcourb(n-1);
    end;
  SetLength(f,n);
  with form_graph do
    for i := 1 to n-1 do
      begin
        f[i-1] := ln(valgraph_x[i]);
        p[i-1] := p[i];
      end;
  coeff_regress(n-1,f,p,a,b);
  {a := -a; }
  iwriteln('Spectral exponent = ' + Format('%10.6g',[a]));
  with form_ulm do
    begin
      status_t_exec(clock - ms);
      status_time;
    end;
end;

procedure tform_spec.button_runClick(Sender: TObject);
begin
  if not check_param then exit;
  run_spec(x_var,nb_val);
end;

procedure tform_spec.button_initClick(Sender: TObject);
begin
  with form_ulm do run_initExecute(nil);
end;

function  tform_spec.check_param : boolean;
var x,e : integer;
begin
  check_param := false;
  if not test_variable(edit_var.Text,x) then
    begin
      erreur('unknown variable name');
      edit_var.Text := s_ecri_var(x_var);
      exit;
    end;
  x_var := x;
  if not est_entier(edit_expo.Text,e) then
    begin
      erreur('Number of values 2^: integer expected');
      edit_expo.Text := IntToStr(expo);
      exit;
    end;
  if ( e < 2 ) or ( e > 13 ) then
    begin
      erreur('Number of values 2^: 2 <= exponent <= 13');
      edit_expo.Text := IntToStr(expo);
      exit;
    end;
  expo := e;
  nb_val := round(exp(expo*ln(2.0)));
  FormActivate(nil);
  check_param := true;
end;

end.
