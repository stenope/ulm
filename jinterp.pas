unit jinterp;

{$MODE Delphi}

{  @@@@@@   interpreteur   @@@@@@  }

interface

uses   Classes;

const  maxvartexte_interp = 10; { nombre max de variables affichees  }

var    lines_interp : TStrings;
       lines_run    : TStrings;
       texte_interp : boolean; { affichage on/off }
       dt_texte_interp : integer;{ frequence des affichages fenetre interp }
       nb_vartexte_interp : integer; { nombre de variables a afficher }
       vartexte_interp : array[1..maxvartexte_interp] of integer;

procedure interp(s : string);
procedure init_interp;
procedure init_texte_interp;

implementation

uses   SysUtils,Dialogs,LazUTF8,
       jglobvar,jutil,jsymb,jsyntax,jcompil,jmath,jmatrix,jrun,jcarlo,
       gulm,ggraph,gtext,gprop,gsensib,gstocsensib,glandscape,gspec,gcorrel,
       glyap,gviewall;

procedure init_interp;
begin
  lines_interp := TStringList.Create;
  lines_run    := TStringList.Create;
end;

procedure erreur_interp(s : string);
begin
  erreur_('Interp - ' + s);
end;


procedure init_texte_interp;

var i,k,x : integer;
begin
  texte_interp := true;
  k := 0;
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then
      for i := 1 to size do
        if ( k <= maxvartexte_interp-1 ) then
          begin
            k := k + 1;
            vartexte_interp[k] := vec[xvec].exp[i];
          end
        else
    else
      for i := 1 to size do
        if ( k <= maxvartexte_interp-1 ) then
          begin
            k := k + 1;
            vartexte_interp[k] := rel[xrel[i]].xvar;
          end;
  nb_vartexte_interp := k;
end;

procedure interp_texte(s : string);
var i,x,tx : integer;
    xx : array[1..maxvartexte_interp] of integer;
begin
  if ( s = '' ) then
    begin
      texte_interp := false;
      iwriteln('Text OFF');
      exit;
    end;
  separe(s,' ');
  with lines_separe do
    begin
      if ( Count > maxvartexte_interp ) then
        begin
          erreur_interp('too many variables for text interp');
          exit;
        end;
      for i := 0 to Count - 1 do
        begin
          trouve_obj(tronque(lines_separe[i]),x,tx);
          if ( x = 0 ) or ( tx <> type_variable ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          xx[i+1] := x;
        end;
      nb_vartexte_interp := Count;
      for i := 1 to nb_vartexte_interp do vartexte_interp[i] := xx[i];
    end;
  texte_interp := true;
  iwriteln('Text ON');
end;

procedure interp_info;
var truc : packed record case integer of
             0 : ( ii : integer );
             1 : ( c4,c3,c2,c1 : char );
           end;
begin
  iwriteln(Format('dic_nb    = %6d',[dic_nb]));
  iwriteln(Format('char_nb   = %6d',[char_nb]));
  iwriteln(Format('lis_nb    = %6d   [%6d]  %12d',
                  [lis_nb,lis_nb_max,lis_nb_max*SizeOf(lis_type)]));
  iwriteln(Format('ree_nb    = %6d   [%6d]  %12d',
                  [ree_nb,ree_nb_max,ree_nb_max*SizeOf(ree_type)]));
  iwriteln(Format('var_nb    = %6d   [%6d]  %12d',
                  [variable_nb,variable_nb_max,variable_nb_max*SizeOf(variable_type)]));
  iwriteln(Format('fun_nb    = %6d   [%6d]  %12d',
                  [fun_nb,fun_nb_max,fun_nb_max*SizeOf(fun_type)]));
  iwriteln(Format('  fun_nb_predef = %6d',[fun_nb_predef]));
  iwriteln(Format('arg_nb    = %6d   [%6d]  %12d',
                  [arg_nb,arg_nb_max,arg_nb_max*SizeOf(arg_type)]));
  iwriteln(Format('vec_nb    = %6d   [%6d]  %12d',
                  [vec_nb,vec_nb_max,vec_nb_max*SizeOf(vec_type)]));
  iwriteln(Format('mat_nb    = %6d   [%6d]  %12d',
                  [mat_nb,mat_nb_max,mat_nb_max*SizeOf(mat_type)]));
  iwriteln(Format('model_nb  = %6d   [%6d]  %12d',
                  [modele_nb,modele_nb_max,modele_nb_max*SizeOf(modele_type)]));
  iwriteln(Format('  mod_rel_nb_max = %6d',[matmax]));
  iwriteln(Format('rel_nb    = %6d   [%6d]  %12d',
                  [rel_nb,rel_nb_max,rel_nb_max*SizeOf(rel_type)]));
  iwriteln(Format('fic_nb    = %6d   [%6d]  %12d',
                  [fic_nb,fic_nb_max,fic_nb_max*SizeOf(fic_type)]));
  iwriteln(Format('maxmat      = %6d',[matmax]));
  iwriteln(Format('maxvargraph = %6d',[maxvargraph]));
  iwriteln(Format('maxgraph    = %6d',[maxgraph]));
  iwriteln(Format('maxvartext  = %6d',[maxvartext]));
  iwriteln(Format('maxtext     = %6d',[maxtext]));
  {iwriteln('AllocMemSize -> ' + IntToStr(AllocMemSize));}
  with truc do
    begin
      ii := 1937007984;
      iwriteln(c1 + c2 + c3 + c4);
      ii := 1751215717;
      iwriteln(c1 + c2 + c3 + c4);
      ii := 1818584933;
      iwriteln(c1 + c2 + c3 + c4);
      ii := 1852076645;
      iwriteln(c1 + c2 + c3 + c4);
    end;
end;

procedure voir(x,tx : word);
begin
  if ( x = 0 ) then
    begin
      b_ecri_list_modele;
      bwriteln(lines_syntax);
      iwriteln('VARIABLES:');
      b_ecri_list_variable;
      bwriteln(lines_syntax);
      b_ecri_list_rel_indep;
      if ( lines_syntax.Count <> 0 ) then
        begin
          iwriteln('OTHER RELATIONS:');
          bwriteln(lines_syntax);
        end;
      b_ecri_list_fun;
      if ( lines_syntax.Count <> 0 ) then
        begin
          iwriteln('FUNCTIONS:');
          bwriteln(lines_syntax);
        end;
      exit;
    end;
  b_ecri(x,tx);
  bwriteln(lines_syntax);
end;

procedure interp_voir(s : string);
var i,x,tx : integer;
begin
  separe(s,' ');
  with lines_separe do
    begin
      if ( Count = 0 ) then
        begin
          voir(0,0);
          exit
        end;
      for i := 0 to Count - 1 do
        begin
          trouve_obj(tronque(lines_separe[i]),x,tx);
          voir(x,tx);
        end;
    end;
end;

procedure interp_newvar(s : string);
var x,v,tv,pos : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos = 0 ) then exit;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := propre(s2);
  if ( s2 = '' ) then exit;
  if not est_nom(s1) then
    begin
      erreur_interp(s1 + ' unproper name for a variable');
      exit;
    end;
  x := trouve_dic(s1);
  if ( x <> 0 ) then
    begin
      x := trouve_variable(x);
      if ( x <> 0 ) then
        begin
          erreur_interp('variable already exists');
          exit;
        end
      else
        begin
          erreur_interp('name already exists');
          exit;
        end;
    end;
  if est_reserve(s1) then
    begin
      erreur_interp('reserved word');
      exit;
    end;
  lirexp(s2,v,tv);
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  x := cre_dic(s1);
  x := cre_variable(x);
  set_variable(x,v,tv);
  form_ulm.run_initExecute(nil);
  form_view_all.init_viewall;
end;

procedure interp_chg_variable(s : string);
var x,v,tv,pos,nom : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos = 0 ) then exit;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := propre(s2);
  nom := trouve_dic(s1);
  if ( nom = 0 ) then
    begin
      erreur_interp('unknown variable');
      exit;
    end;
  x := trouve_variable(nom);
  if ( x = 0 ) then
    begin
      erreur_interp('unknown variable');
      exit;
    end;
  if ( x = xtime ) then
    begin
      erreur_interp('time cannot be changed');
      exit;
    end;
  lirexp(s2,v,tv);
  if err_syntax then
    begin
      err_syntax := false;
      exit;
    end;
  if ( variable[x].xrel <> 0 ) then
    begin
      if ( tv <> type_ree ) then
        begin
          erreur_interp('relation-variable must be set to a real value');
          exit;
        end;
    end;
  set_variable(x,v,tv);
  form_ulm.run_initExecute(nil);
end;

{ ------ run ------ }

procedure interp_init(s : string);
var n : integer;
begin
  if ( s <> '' ) then
    if est_entier(s,n) then
      begin
        graine0 := graine00 + (n-1);
        if ( n <= 0 ) then graine0 := graine00;
        graine := graine0;
        iwriteln('random generator seed -> ' + IntToStr(graine0-graine00+1));
      end;
  form_ulm.run_initExecute(nil);
end;

procedure interp_run(s : string);
var n1,n2,pos : integer;
    s1,s2 : string;
begin
  if ( s = '' ) then
    begin
      form_ulm.run_runExecute(nil);
      exit;
    end;
  pos := position(s,' ');
  with form_ulm do
    begin
      if ( pos = 0 ) then
        if est_entier(s,n1) then
          nb_cycle := n1
        else
          begin
            erreur_interp('integer expected');
            exit;
          end
      else
        begin
          coupe(s,pos,s1,s2);
          s2 := tronque(s2);
          if not est_entier(s1,n1) or not est_entier(s2,n2) then
            begin
              erreur_interp('2 integer values expected');
              exit;
            end;
          nb_cycle := n1;
          dt_texte_interp := n2;
        end;
      run_runExecute(nil);
    end;
end;

procedure interp_param(s : string);
var x,nom,pos : integer;
    s1,s2,s3 : string;
    a,b,c : extended;
begin
  if ( s = '' ) then with form_ulm do
    begin
      param := false;
      variable[xparam].exp_type := param_exp_type_sav;
      variable[xparam].exp := param_exp_sav;
      iwriteln('Parameter OFF');
      exit;
    end;
  pos := position(s,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s,pos,s1,s2);
      s1 := tronque(s1);
      s2 := tronque(s2);
      s  := s1;
    end;
  nom := trouve_dic(s);
  if ( nom = 0 ) then
    begin
      erreur_interp('unknown variable name');
      exit;
    end;
  x := trouve_variable(nom);
  if ( x = 0 ) then
    begin
      erreur_interp('unknown variable name');
      exit;
    end;
  if ( pos = 0 ) then
    begin
      erreur_interp('Min,Max and Step values expected');
      exit;
    end;
  s := s2;
  pos := position(s,' ');
  if ( pos = 0 ) then
    begin
      erreur_interp('Min,Max and Step values expected');
      exit;
    end;
  coupe(s,pos,s1,s2);
  s1 := tronque(s1);
  s2 := tronque(s2);
  pos := position(s2,' ');
  if ( pos = 0 ) then
    begin
      erreur_interp('Min,Max and Step values expected');
      exit;
    end;
  coupe(s2,pos,s2,s3);
  s2 := tronque(s2);
  s3 := tronque(s3);
  if ( not est_reel(s1,a) ) or
     ( not est_reel(s2,b) ) or
     ( not est_reel(s3,c) ) then
    begin
      erreur_interp('real value expected');
      exit;
    end;
  if ( a >= b ) then
    begin
      erreur_interp('Min >= Max');
      exit;
    end;
  if ( c <= 0.0 ) then
    begin
      erreur_interp('step <= 0');
      exit;
    end;
  with form_ulm do
    begin
      param_exp_type_sav := variable[x].exp_type;
      param_exp_sav := variable[x].exp;
      variable[x].exp_type := type_ree;
      variable[x].exp := cre_ree(a);
      xparam    := x;
      param_min := a;
      param_max := b;
      param_pas := c;
      param  := true;
    end;
  iwriteln('Parameter ON');
end;

procedure interp_run_carlo(s : string);
var pos,nb_cycle_carlo1,nb_run_carlo1 : integer;
    seuil_ext1,seuil_div1 : extended;
    s1,s2,s3,s4 : string;
begin
  pos := position(s,' ');
  if ( pos = 0 ) then with form_ulm do
    begin
      montecarlo_runExecute(nil);
      exit;
    end;
  coupe(s,pos,s1,s2);
  s2 := tronque(s2);
  if not est_entier(s1,nb_cycle_carlo1) then
    begin
      erreur_interp('number of time steps expected');
      exit;
    end;
  if ( nb_cycle_carlo1 < 1 ) then
    begin
      erreur_interp('number of time steps < 1');
      exit;
    end;
  if ( s2 = '' ) then
    begin
      erreur_interp('arguments expected');
      exit;
    end;
  pos := position(s2,' ');
  if ( pos = 0 ) then
    begin
      if not est_entier(s2,nb_run_carlo1) then
        begin
          erreur_interp('number of trajectories expected');
          exit;
        end;
      if ( nb_run_carlo1 < 1 ) then
        begin
          erreur_interp('number of trajectories < 1');
          exit;
        end;
      with form_ulm do
        begin
          nb_cycle_carlo := nb_cycle_carlo1;
          nb_run_carlo   := nb_run_carlo1;
          montecarlo_runExecute(nil);
        end;
      exit;
    end;
  seuil_ext1 := seuil_ext;
  seuil_div1 := seuil_div;
  coupe(s2,pos,s2,s3);
  if not est_entier(s2,nb_run_carlo1) then
    begin
      erreur_interp('number of trajectories expected');
      exit;
    end;
  if ( nb_run_carlo1 < 1 ) then
    begin
      erreur_interp('number of trajectories < 1');
      exit;
    end;
  s3 := tronque(s3);
  pos := position(s3,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s3,pos,s3,s4);
      if not est_reel(s3,seuil_ext1) then
        begin
          erreur_interp('extinction threshold expected');
          exit;
        end;
      s4 := tronque(s4);
      if not est_reel(s4,seuil_div1) then
        begin
          erreur_interp('divergence threshold expected');
          exit;
        end;
      if ( seuil_ext1 < 0.0 ) then
        begin
          erreur_interp('extinction threshold < 0');
          exit;
        end;
      if ( seuil_div1 <= 0.0 ) then
        begin
          erreur_interp('divergence threshold <= 0');
          exit;
        end;
      if ( seuil_ext1 >= seuil_div1 ) then
        begin
          erreur_interp('extinction threshold >= divergence threshold');
          exit;
        end;
    end
  else
    begin
      if not est_reel(s3,seuil_ext1) then
        begin
          erreur_interp('extinction threshold expected');
          exit;
        end;
      if ( seuil_ext1 < 0.0 ) then
        begin
          erreur_interp('extinction threshold < 0');
          exit;
        end;
      seuil_div1 := seuil_div_def
    end;
  with form_ulm do
    begin
      nb_cycle_carlo := nb_cycle_carlo1;
      nb_run_carlo   := nb_run_carlo1;
      seuil_ext := seuil_ext1;
      seuil_div := seuil_div1;
      montecarlo_runExecute(nil);
    end;
end;

{ ------ proprietes des matrices ------ }

procedure interp_prop(s : string);
var x,tx : integer;
begin
  if ( s = '' ) then
    begin
      x := modele[1].xmat;
      if ( x = 0 ) then
        begin
          erreur_interp('matrix-type models only');
          exit;
        end;
    end
  else
    begin
      trouve_obj(s,x,tx);
      if ( x = 0 ) or ( tx <> type_mat ) then
        begin
          erreur_interp('unknown matrix name');
          exit;
        end;
    end;
  with form_prop do
    begin
      interp_ := true;
      proprietes(x);
      interp_ := false;
    end;
end;

procedure interp_sensibilite(s : string);
var x,y,a,pos,tx,ty,nb_cyc : integer;
    u,v,xmin,xmax,ymin,ymax : extended;
    s1,s2,s3 : string;
begin
  nb_cyc := 100;
  if ( s = '' ) then
    begin
      a := modele[1].xmat;
      if ( a = 0 ) then
        begin
          erreur_interp('matrix-type models only');
          exit;
        end;
      x := 0;
      form_sensib.sensibilites_interp(a,x);
      if ( matrandom(a) or mattimedep(a) or
           matvecdep(a,modele[mat[a].xmodele].xvec) ) then
        form_stocsensib.stoc_sensib_interp(a,x,nb_cyc);
      exit;
    end;
  pos := position(s,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      trouve_obj(s1,x,tx);
      if ( x = 0 ) then exit;
      if ( tx = type_mat ) then
        begin
          pos := position(s2,' ');
          if ( pos = 0 ) then
            begin
              trouve_obj(s2,y,ty);
              if ( y = 0 ) then exit;
              if ( ty = type_variable ) then
                begin
                  a := x;
                  x := y;
                  form_sensib.sensibilites(a,x);
                  if ( matrandom(a) or mattimedep(a) or
                       matvecdep(a,modele[mat[a].xmodele].xvec) ) then
                    form_stocsensib.stoc_sensib_interp(a,x,nb_cyc);
                  exit;
                end;
              exit;
            end;
          a := x;
          coupe(s2,pos,s2,s3);
          s3 := tronque(s3);
          trouve_obj(s2,x,tx);
          if ( x = 0 ) then exit;
          trouve_obj(s3,y,ty);
          if ( y = 0 ) then exit;
          if ( tx = type_variable ) and ( ty = type_variable ) then
            begin
              if not matnonneg(mat[a].size,mat[a].val) then
                begin
                  erreur_interp('matrix is negative');
                  exit;
                end;
              if mattimedep(a) then
                begin
                  erreur_interp('matrix is time-dependent');
                  exit;
                end;
              if matvecdep(a,modele[mat[a].xmodele].xvec) then
                begin
                  erreur_interp('matrix is vector-dependent');
                  exit;
                end;
              if matrandom(a) then
                begin
                  erreur_interp('matrix is random');
                  exit;
                end;
              u := variable[x].val;
              v := variable[y].val;
              xmin := 0.5*u;
              xmax := 1.5*u;
              ymin := 0.5*v;
              ymax := 1.5*v;
              if ( u = 0 ) then xmax := 1.0;
              if ( v = 0 ) then ymax := 1.0;
              form_landscape.exec_landscape(a,x,y,xmin,xmax,ymin,ymax);
            end;
          exit;
        end;
      {if ( tx = type_vec ) then
        begin
          y := vec[x].xmodele;
          a := modele[y].xmat;
          trouve_obj(s2,x,tx);
          if ( x = 0 ) then exit;
          if ( tx <> type_variable ) then exit;
          sensibilite_vecprop_x(a,x);
          exit;
        end; }
      trouve_obj(s2,y,ty);
      if ( y = 0 ) then exit;
      if ( tx = type_variable ) then
        if ( ty = type_variable ) then
          begin
            a := modele[1].xmat;
            if ( a = 0 ) then
              begin
                erreur_interp('matrix-type models only');
                exit;
              end;
            if not matnonneg(mat[a].size,mat[a].val) then
              begin
                erreur_interp('matrix is negative');
                exit;
              end;
            if mattimedep(a) then
              begin
                erreur_interp('matrix is time-dependent');
                exit;
              end;
            if matvecdep(a,modele[mat[a].xmodele].xvec) then
              begin
                erreur_interp('matrix is vector-dependent');
                exit;
              end;
            if matrandom(a) then
              begin
                erreur_interp('matrix is random');
                exit;
              end;
            u := variable[x].val;
            v := variable[y].val;
            xmin := 0.5*u;
            xmax := 1.5*u;
            ymin := 0.5*v;
            ymax := 1.5*v;
            if ( u = 0 ) then xmax := 1.0;
            if ( v = 0 ) then ymax := 1.0;
            form_landscape.exec_landscape(a,x,y,xmin,xmax,ymin,ymax);
            exit;
          end;
      exit;
    end;
  trouve_obj(s,x,tx);
  if ( x = 0 ) then exit;
  if ( tx = type_variable ) then
    begin
      a := modele[1].xmat;
      if ( a = 0 ) then
        begin
          erreur_interp('matrix-type models only');
          exit;
        end;
      form_sensib.sensibilites_interp(a,x);
      if ( matrandom(a) or mattimedep(a) or
           matvecdep(a,modele[mat[a].xmodele].xvec) ) then
        form_stocsensib.stoc_sensib_interp(a,x,nb_cyc);
      exit;
    end;
  if ( tx = type_mat ) then
    begin
      a := x;
      x := 0;
      form_sensib.sensibilites_interp(a,x);
      if ( matrandom(a) or mattimedep(a) or
           matvecdep(a,modele[mat[a].xmodele].xvec) ) then
        form_stocsensib.stoc_sensib_interp(a,x,nb_cyc);
      exit;
    end;
  if ( tx = type_vec ) then
    begin
      erreur_interp('variable name expected');
      exit;
    end;
end;


{ ------ utilitaires ------ }

procedure interp_spectre(s : string);
var x,tx,n : integer;
begin
  if ( s = '' ) then
    begin
      erreur_interp('variable name expected');
      exit;
    end;
  trouve_obj(s,x,tx);
  if ( x = 0 ) or ( tx <> type_variable ) then
    begin
      erreur_interp('unknown variable name');
      exit;
    end;
  n := 1024;
  with form_spec do run_spec(x,n);
end;

procedure interp_correl(s : string);
var  x,y,tx,ty,pos,n,m : integer;
     s1,s2 : string;
begin
  if ( s = '' ) then
    begin
      erreur_interp('variable name expected');
      exit;
    end;
  n := 400;
  m := 100;
  pos := position(s,' ');
  if ( pos <> 0 ) then
    begin
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      trouve_obj(s1,x,tx);
      if ( x = 0 ) or ( tx <> type_variable ) then
        begin
          erreur_interp('not a variable name');
          exit;
        end;
      trouve_obj(s2,y,ty);
      if ( y = 0 ) or ( ty <> type_variable ) then
        begin
          erreur_interp('not a variable name');
          exit;
        end;
    end
  else
    begin
      trouve_obj(s,x,tx);
      if ( x = 0 ) or ( tx <> type_variable ) then
        begin
          erreur_interp('not a variable name');
          exit;
        end;
      y := x;
    end;
  with form_correl do run_correl(x,y,n,m);
end;

procedure interp_lyap(s : string);
var x,tx,nb_cyc,dt : integer;
begin
  nb_cyc := 1000;
  dt := 100;
  x := 1;
  if ( s = '' ) then with form_lyap do
    begin
      interp_ := true;
      run_lyap(x,nb_cyc,dt);
      interp_ := false;
      exit;
    end;
  trouve_obj(s,x,tx);
  if ( x = 0 ) or ( tx <> type_modele ) then
    begin
      erreur_interp('unknown model name');
      exit;
    end;
  with form_lyap do
    begin
      interp_ := true;
      run_lyap(x,nb_cyc,dt);
      interp_ := false;
    end;
end;

procedure interp_fic(s : string);
{ s = nom_fic x1 ... xn }
{ avec  xi = nom_variable ou xi = nom_variable:precision }
var  nom,x,i,n,ific,pos,p : integer;
     xx,pp : var_fic_type;
     nom_fic,s1,s2 : string;
     b : boolean;
begin
  if ( s = '' ) then exit;
  separe(s,' ');
  with lines_separe do
    begin
      n := Count - 1;
      nom_fic := tronque(lines_separe[0]);
      if ( n = 0 ) then { close fic }
        begin
          b := false;
          for i := 1 to fic_nb do with fic[i] do
            if ( nam = nom_fic ) then
              begin
                b := true;
                CloseFile(f);
                iwriteln('File ' + nam + ' closed');
                fic[i] := fic[fic_nb];
                fic_nb := fic_nb - 1;
                exit;
              end;
          if not b then
            begin
              erreur_interp('unknown file name');
              exit;
            end;
        end;
      if ( n > var_fic_nb_max ) then
        begin
          erreur_interp('too many variables for file');
          exit;
        end;
      for i := 1 to n do
        begin
          s1 := tronque(lines_separe[i]);
          pos := position(s1,':');
          if ( pos > 0 ) then
            begin
              coupe(s1,pos,s1,s2);
              if  est_entier(s2,p) then
                if ( p > 0 ) then
                  pp[i] := p
                else
                  begin
                    erreur_interp('precision: positive integer expected');
                    exit;
                  end
              else
                begin
                  erreur_interp('precision: positive integer expected');
                  exit;
                end;
            end
          else
            pp[i] := 4;
          nom := trouve_dic(tronque(s1));
          if ( nom = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          x := trouve_variable(nom);
          if ( x = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          xx[i] := x;
        end;
      ific := trouve_fic(nom_fic);
      if ( ific = 0 ) then
        ific := cre_fic(nom_fic)
      else
        begin
          erreur_interp('file already opened');
          exit;
        end;
      with fic[ific] do
        begin
          AssignFile(f,nam);
          rewrite(f);
          nam := nom_fic;
          iwriteln('File ' + nam + ' opened');
          var_fic_nb := n;
          var_fic := xx;
          precis  := pp;
        end;
    end;
end;

{ ------ graphique ------ }

procedure interp_efface;
begin
  tab_form_graph[i_form_graph].efface(nil);
end;

procedure interp_addgraph;
begin
  with tab_form_graph[i_form_graph] do
    begin
      gplus := not gplus;
      if gplus then
        iwriteln('Addgraph ON (graphic window #' + IntToStr(i_form_graph) + ')')
      else
        iwriteln('Addgraph OFF (graphic window #' + IntToStr(i_form_graph) + ')');
      status_gplus;
    end;
end;

procedure interp_graph1(s : string);
var  xx,nom,i,x : integer;
     yy : array[1..maxvargraph] of integer;
begin
  if ( s = '' ) then exit;
  separe(s,' ');
  with tab_form_graph[i_form_graph],lines_separe do
    begin
      if ( Count-1 > maxvargraph ) then
        begin
          erreur_interp('too many variables for graphics');
          exit;
        end;
      if ( Count < 2 ) then
        begin
          erreur_interp('Y-variables missing');
          exit;
        end;
      nom := trouve_dic(tronque(lines_separe[0]));
      if ( nom = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      xx := trouve_variable(nom);
      if ( xx = 0 ) then
        begin
          erreur_interp('unknown variable name');
          exit;
        end;
      for i := 1 to Count - 1 do
        begin
          nom := trouve_dic(tronque(lines_separe[i]));
          if ( nom = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          x := trouve_variable(nom);
          if ( x = 0 ) then
            begin
              erreur_interp('unknown variable name');
              exit;
            end;
          yy[i] := x;
        end;
      vargraph_x := xx;
      nb_vargraph_y := Count - 1;
      for i := 1 to nb_vargraph_y do vargraph_y[i] := yy[i];
      for i := nb_vargraph_y + 1 to maxvargraph do vargraph_y[i] := 0;
      for i := 1 to maxvargraph do
        vargraph_y_col[i] := var_color(vargraph_y[i]);
    end;
end;

procedure interp_graph(s : string);
begin
  if ( s = '' ) then exit;
  interp_graph1(s);
  with tab_form_graph[i_form_graph] do
    begin
      status_var;
      gscatter := false;
      gregress := false;
    end;
end;

procedure interp_scatter(s : string);
begin
  if ( s = '' ) then exit;
  interp_graph1(s);
  with tab_form_graph[i_form_graph] do
    begin
      status_var;
      gscatter := true;
      gregress := true;
      distrib  := false;
    end;
end;

procedure interp_graph_window(s : string);
var i : integer;
begin
  if ( s = '' ) then
    begin
      erreur_interp('positive integer expected');
      exit;
    end;
  if not est_entier(s,i) then
    begin
      erreur_interp('positive integer expected');
      exit;
    end;
  if ( i < 1 ) then
    begin
      erreur_interp('positive integer expected');
      exit;
    end;
  if ( i > maxform_graph ) then
    begin
      erreur_interp('No more than ' + IntToStr(maxform_graph) +
                    ' graphic windows available');
      exit;
    end;
  i_form_graph := i;
  with tab_form_graph[i_form_graph] do
    if not Visible then Show;
  iwriteln('Graphic window #' +  IntToStr(i) +' selected');
end;

procedure interp_xscale(s : string);
var  pos : integer;
     s1,s2 : string;
     a,b : extended;
     fg : tform_graph;
begin
  fg := tab_form_graph[i_form_graph];
  if ( s = '' ) then
    if ( fg.xscale ) then
      begin
        fg.xscale := false;
        iwriteln('Xscale OFF (graphic window #' + IntToStr(i_form_graph) + ')');
        exit;
      end
    else
  else
    begin
      pos := position(s,' ');
      if ( pos = 0 ) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      if not est_reel(s1,a) or not est_reel(s2,b) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      fg.xmin := min(a,b);
      fg.xmax := max(a,b);
    end;
  fg.xscale := true;
  iwriteln('Xscale ON  [' + FloatToStr(fg.xmin) + ', '
                          + FloatToStr(fg.xmax) +
           '] (graphic window #' + IntToStr(i_form_graph) + ')');
end;

procedure interp_yscale(s : string);
var  pos : integer;
     s1,s2 : string;
     a,b : extended;
     fg : tform_graph;
begin
  fg := tab_form_graph[i_form_graph];
  if ( s = '' ) then
    if ( fg.yscale ) then
      begin
        fg.yscale := false;
        iwriteln('Yscale OFF (graphic window #' + IntToStr(i_form_graph) + ')');
        exit;
      end
    else
  else
    begin
      pos := position(s,' ');
      if ( pos = 0 ) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      coupe(s,pos,s1,s2);
      s2 := tronque(s2);
      if not est_reel(s1,a) or not est_reel(s2,b) then
        begin
          erreur_interp('2 real values expected');
          exit;
        end;
      fg.ymin := min(a,b);
      fg.ymax := max(a,b);
    end;
  fg.yscale := true;
  iwriteln('Yscale ON  [' + FloatToStr(fg.ymin) + ', '
                          + FloatToStr(fg.ymax) +
           '] (graphic window #' + IntToStr(i_form_graph) + ')');
end;

procedure interp_line(s : string);
var col : integer;
begin
  with tab_form_graph[i_form_graph] do
    begin
      if ( s = '' ) then
        line0 := not line0
      else
        if est_entier(s,col) then
          begin
            if ( col < 1 ) or ( col > maxcolors ) then
              col := col mod maxcolors + 1;
            vargraph_y_col[1] := colors[col];
            line0 := true;
          end
        else
          begin
            erreur_interp('positive integer expected');
            exit;
          end;
      if line0 then
        iwriteln('Line ON (graphic window #' + IntToStr(i_form_graph) + ')')
      else
        iwriteln('Line OFF (graphic window #' + IntToStr(i_form_graph) + ')');
    end;
end;

procedure interp_bord(s : string);
begin
  with tab_form_graph[i_form_graph] do
    begin
      bord := not bord;
      if bord then
        iwriteln('Border ON (graphic window #' + IntToStr(i_form_graph) + ')')
      else
        iwriteln('Border OFF (graphic window #' + IntToStr(i_form_graph) + ')');
    end;
end;

procedure interp_skip(s : string);
var nbs : integer;
begin
  with tab_form_graph[i_form_graph] do
  if ( s = '' ) then
    begin
      nb_skip := 0;
      iwriteln('Skip OFF (graphic window #' + IntToStr(i_form_graph) + ')');
    end
  else
    if est_entier(s,nbs) then
      begin
        nb_skip := nbs;
        iwriteln('Skip ON (graphic window #' + IntToStr(i_form_graph) + ')')
      end
    else
      begin
        erreur_interp('integer expected');
        exit;
      end;
end;

procedure interp_savegraph(s : string);
begin
  with tab_form_graph[i_form_graph] do
    begin
      nomficgraph := s;
      file_saveExecute2(nil); {jjjjj}
    end;
end;

procedure interp_distrib(s : string);
var delta : extended;
begin
  with tab_form_graph[i_form_graph] do
    begin
      if ( s = '') then
        if distrib then
          distrib := false
        else
          begin
            distrib := true;
            d_distrib := 1.0;
          end
      else
        if est_reel(s,delta) then
          if ( delta > 0.0 ) then
            begin
              distrib := true;
              d_distrib := delta;
            end
          else
            begin
              erreur_interp('positive real number expected');
              exit;
            end
        else
          begin
            erreur_interp('positive real number expected');
            exit;
          end;
      if distrib then
        iwriteln('Distribution mode ON (graphic window #' + IntToStr(i_form_graph) + ')')
      else
        iwriteln('Distribution mode OFF (graphic window #' + IntToStr(i_form_graph) + ')');
      status_distrib;
    end;
end;

{ ------ autres ------ }

procedure interp_comment(s : string);
begin
  iwriteln(s);
end;

procedure interp_tempo(s : string);
var sec,nms,c : integer;
begin
  if ( s = '' ) then
    exit
  else
    if est_entier(s,sec) then
      begin
        nms := sec*1000;
        c := clock;
        while (clock - c) < nms do;
      end
    else
      erreur_interp('integer expected');
end;

procedure interp_notext;
begin
  notext := not notext;
end;

{ccccc}
{procedure interp_carlo_n0;
var i,x : integer;
begin
  carlo_n0 := not carlo_n0;
  if carlo_n0 then
    begin
      iwriteln('-> carlo distrib ON');
      for i := 1 to 100 do
        iwriteln('ii ' + IntToStr(i) + ' ' + FloatToStr(carlo_tab_n0[i]));
    end
  else
    begin
      x := modele[1].xrel[1];
      x := rel[x].xvar;
      variable[x].val0 := 0;
      iwriteln('-> carlo distrib OFF');
    end;
end;}

procedure interp_tobs(s : string);
var t1 : integer;
begin
  if ( s = '' ) then
    exit
  else
    if est_entier(s,t1) then
      begin
        tobs := t1;
        iwriteln('tobs = ' + IntToStr(tobs));
      end
    else
      erreur_interp('tobs: integer expected');
end;

{const kolmax = 100000;
var   e,a,b : array[0..kolmax] of integer;

procedure kolak(nmax : integer);
var nkol : integer;
    kol : string;

procedure construc_kol;
var n : integer;
    s : string;
    c : char;
begin
   kol := '';
   kol := kol + '122';
   for n := 3 to nmax do
     begin
       c := kol[n];
       s := IntToStr(1 + (n-1) mod 2);
       kol := kol + s;
       if ( c = '2' ) then kol := kol + s;
     end;
   nkol := length(kol);
   iwriteln('nkol = ' + IntToStr(nkol));
end;

procedure exponents;
var n,p,ne,q : integer;
begin
  a[0] := 0;
  b[0] := 1;
  e[0] := 2;
  p := 0;
  q := 0;
  n := 2;
  repeat
    p := p + 1;
    a[p] := n;
    if ( kol[n+3] = kol[n+2] ) then
      begin
        e[p] := 2;
        n := n + 2;
        q := q + 1;
        b[q] := n-1;
      end
    else
      begin
        e[p] := 1;
        n := n + 1;
      end;
  until n >= nkol;
  ne := p;
end;

procedure conjecture1;
label 1;
var n,k,u,v,delta,mi,ma : integer;
begin
  n := 0;
  mi := bigint;
  ma := -bigint;
  iwriteln('   n    k(n)   a(n)   a(n+2a(n))   4a(n)   delta');
  repeat;
    k := n + 2*a[n];
    u := a[k];
    if ( n > 0 ) and ( u = 0 ) then goto 1;
    v := 4*a[n];
    delta := u - v;
    mi := imin(delta,mi);
    ma := imax(delta,ma);
    iwriteln(Format('%4d',[n])    + '    ' + kol[n+2] + ' ' +
             Format('%8d',[a[n]]) + ' ' + Format('%8d',[u]) + ' ' +
             Format('%8d',[v])    + ' ' + Format('%8d',[delta]));
    n := n + 1;
  until k >= nkol;
1:
  iwriteln('min = ' + IntToStr(mi) + '  max = ' + IntToStr(ma));
end;

procedure conjecture2;
var n,k,u,v,delta : integer;
begin
  n := 1;
  u := 1;
  while ( u > 0 ) do
    begin
      k := n + b[n];
      u := a[k];
      v := 4*a[n];
      delta := u - v;
      n := n + 1;
      form_graph.valgraph_x[n] := n;
      form_graph.valgraph_y[1][n] := delta;
    end;
  with form_graph do
    begin
      if not gplus then efface(nil);
      gcourb(n-2);
    end;
end;

procedure conjecture3;
var n,u,v,delta : integer;

function aaa(x : integer): integer;
var a,b,s1,s2 : integer;
begin
  a  := x;
  s1 := 0;
  b  := x div 2;
  s2 := 0;
  while ( a > 0 ) do
    begin
      s1 := s1 + a;
      a  := a div 4;
    end;
  while ( b > 0 ) do
    begin
      s2 := s2 + b;
      b  := b div 4;
    end;
  aaa := s1 - s2;
end;

begin
  n := 1;
  u := 1;
  while ( u > 0 ) do
    begin
      u := a[n];
      v := aaa(u);
      delta := n - v;
      iwriteln(Format('%4d',[n]) + '   ' + kol[n+2] + ' ' +
               Format('%4d',[a[n]]) + ' ' + Format('%4d',[b[n]]) + ' ' +
               Format('%4d',[v]) + ' ' + Format('%4d',[delta]));
      form_graph.valgraph_x[n] := n;
      form_graph.valgraph_y[1][n] := u;
      n := n + 1;
    end;
  form_graph.gcourb(n-2);
end;

begin
   construc_kol;
   exponents;
   conjecture1;
end;}

{procedure timb_sym(m : integer);
const nmax = 60;
var i,j,i1,n,nbj,k : integer;
    bb : boolean;
    pli,mem,memw : array[0..nmax] of integer;
    nb_sol : array[0..nmax] of Int64;
    w : array[1..nmax,0..nmax] of Int64;
    s : string;

procedure init;
var i,j : integer;
begin
  for i := 0 to nmax do pli[i] := 0;
  for i := 0 to nmax do mem[i] := 0;
  for i := 1 to nmax do nb_sol[i] := 0;
  for i := 0 to n do memw[i] := 0;
  for i := 1 to n do for j := 0 to n do w[i,j] := 0;
  pli[0] := 1;
  nb_sol[1] := 1;
end;

procedure sym;
var r,k,x : integer;
begin
  i1 := i1 + 1;
  pli[0] := pli[0] + 1;
  for  k := i1 downto 2  do
    begin
      x := pli[k-1];
      if  x > 0  then  pli[k] := x+1  else  pli[k] := 0;
    end;
  r := 1;
  x := pli[0];
  while  (x <> i1) do
    begin
      r := r + 1;
      x := pli[x];
    end;
  if  (r < (i1+1) div 2) then
    k := i1+1-r
  else
    k := i1-r;
  x := 0;
  for r := 1 to k-1 do x := pli[x];
  pli[1] := pli[x];
  pli[x] := 1;
  i := i + 1;
  mem[i] := x;
end;

procedure inv;
var k,x : integer;
begin
  pli[mem[i]] := pli[1];
  pli[0] := pli[0] - 1;
  for  k := 1 to i  do
    begin
      x := pli[k+1];
      if (x > 0) then  pli[k] := x-1  else  pli[k] := 0;
    end;
  i := i - 1;
end;

begin
  n := 2*m + 1;
  init;
  i := 1;
  j := i;
  repeat
    i1 := i + 1;
    mem[i] := j;
    memw[i] := nbj;
    pli[i1] := pli[j];
    pli[j] := i1;
    sym;
    nb_sol[i1] := nb_sol[i1] + 1;
    if ( i + 2 < n ) then
      begin
        i := i1;
        j := i;
        nbj := 1;
      end
    else
      repeat
        inv;
        j := pli[i+1];
        pli[mem[i]] := j;
        nbj := memw[i];
        bb := j < i;
        if bb then
          begin
            nbj := nbj + 1;
            if ( j > 0 ) then
              if odd(j) then j := succ(j) else j := pred(j)
            else
          end
        else
          begin
            w[i,nbj-2] := w[i,nbj-2] + 1;
            i := pred(i);
          end;
      until bb;
  until i <= 2;
  for i := 3 to n do
    if odd(i) then
      begin
        k := i div 2;
        s := Format('%2d',[k]) + '  ' + Format('%12d',[nb_sol[i]]) + ' : ';
        if ( k < m ) then
          for j := 0 to k do s := s + Format('%4d',[w[i,j]]) + ' ';
        iwriteln(s);
      end;
end;}

procedure interp_perso(s : string);
var i,nmax : integer;
begin
  if ( s = '' ) then
    exit
  else
    if est_entier(s,nmax) then
      begin
        {timb_sym(nmax);}
        {kolak(nmax);}
        for i := 1  to nmax do iwriteln(':-) ');
      end
    else
      erreur_interp('integer expected');
end;

function  traduc(s : string) : char;
begin
  if ( s = 'help' ) or ( s = 'h' ) or ( s = '?' ) then traduc := 'h'
  else
  if ( s = 'param' ) or ( s = 'parameter' )  or ( s = 'a' ) then traduc := 'a'
  else
  if ( s = 'border' )  or ( s = 'b' ) then traduc := 'b'
  else
  if ( s = 'change' ) or ( s = 'changevar' ) or ( s = 'c' ) then traduc := 'c'
  else
  if ( s = 'clear' ) or ( s = 'e' ) then traduc := 'e'
  else
  if ( s = 'file' ) or ( s = 'f' ) then traduc := 'f'
  else
  if ( s = 'graph' ) or ( s = 'g' ) then traduc := 'g'
  else
  if ( s = 'init' ) or ( s = 'i' ) then traduc := 'i'
  else
  if ( s = 'scatter' ) or ( s = 'j' ) then traduc := 'j'
  else
  if ( s = 'line' ) or ( s = 'l' ) then traduc := 'l'
  else
  if ( s = 'montecarlo' ) or ( s = 'm' ) then traduc := 'm'
  else
  if ( s = 'prop' ) or ( s = 'property' ) or ( s = 'p' ) then traduc := 'p'
  else
  if ( s = 'lyap' ) or ( s = 'lyapunov' ) or ( s = 'q' ) then traduc := 'q'
  else
  if ( s = 'new' ) or ( s = 'newvar' ) or ( s = 'n' ) then traduc := 'n'
  else
  if ( s = 'run' ) or ( s = 'r' ) then traduc := 'r'
  else
  if ( s = 'sens' ) or ( s = 'sensitivity' ) or ( s = 's' ) then traduc := 's'
  else
  if ( s = 'text' ) or ( s = 't' ) then traduc := 't'
  else
  if ( s = 'distribution' ) or ( s = 'distrib' ) or ( s = 'u' ) then traduc := 'u'
  else
  if ( s = 'view' ) or ( s = 'v' ) then traduc := 'v'
  else
  if ( s = 'window' ) or ( s = 'w' ) then traduc := 'w'
  else
  if ( s = 'xscale' ) or ( s = 'x' ) then traduc := 'x'
  else
  if ( s = 'yscale' ) or ( s = 'y' ) then traduc := 'y'
  else
  if ( s = 'spectrum' ) or ( s = 'spec' ) or ( s = 'k' ) then traduc := 'k'
  else
  if ( s = 'correlation' ) or ( s = 'correl' ) or ( s = 'o' ) then traduc := 'o'
  else
  if ( s = 'add' ) or ( s = 'addgraph' ) or ( s = '+' ) then traduc := '+'
  else
  if ( s = '_' ) then traduc := '_'
  else
  if ( s = 'save' ) or ( s = 'savegraph' ) or ( s = '!' ) then traduc := '!'
  else
  if ( s = 'skip' ) or ( s = '>' ) then traduc := '>'
  else
  if ( s = '{' )  then traduc := '{'
  else
  if ( s = 'notext' ) or ( s = '(' )  then traduc := '('
  else
  if ( s = 'tempo' ) or ( s = '&' )  then traduc := '&'
  else
  if ( s = '§' )  then traduc := '#'
  else
  if ( s = ';' )  then traduc := ';' {ccccc}
  else
    traduc := '=';
end;

procedure interp_help(s : string);
var c : char;
begin
  if ( s = '' ) then
    begin
      iwriteln('COMMANDS:');
      iwriteln('  Help  Text  View');
      iwriteln('  Run  Montecarlo  Init');
      iwriteln('  pArameter File');
      iwriteln('  Property  Sensitivity');
      iwriteln('  speKtrum  cOrrelation lyapunov');
      iwriteln('  Changevar  Newvar');
      iwriteln('  Graph  scatter addgraph  savegraph');
      iwriteln('  clEar  Line  distribUtion  Border');
      iwriteln('  Window Xscale  Yscale  skip'#13#10);
      iwriteln('BINARY OPERATORS:');
      iwriteln('  +      -     *     /      ^');
      iwriteln('  >      <     \     @'#13#10);
      iwriteln('UNARY OPERATORS:');
      iwriteln('  -      sqrt  abs   trunc   round');
      iwriteln('  sin    asin  cos   acos    tan    atan');
      iwriteln('  ln     ln0   log   exp     fact');
      iwriteln('  gauss  rand  ber   gamm    poisson');
      iwriteln('  geom   expo'#13#10);
      iwriteln('FUNCTIONS:');
      iwriteln('  if      min        max     stepf');
      iwriteln('  gaussf  lognormf   binomf  poissonf');
      iwriteln('  nbinomf nbinom1f');
      iwriteln('  betaf   beta1f     bicof   tabf');
      iwriteln('  bdf     gratef  lambdaf');
      iwriteln('  prevf   textf');
      iwriteln('  meanf   variancef   skewnessf cvf');
      iwriteln('  meanzf  variancezf  cvzf');
      iwriteln('  nzf     nef         nif');
      iwriteln('  extratef    immratef'#13#10);
      iwriteln('type help xxx for help about operator xxx');
      iwriteln('> Press Ctrl-Alt simultaneously to break execution <');
      exit;
    end;
  c := traduc(s);
  case c of
  'a'  : begin
           iwriteln('parameter or param or a (on/off)');
           iwriteln('  param x Min Max Step');
           iwriteln('variable x will be use as a parameter for the next run command');
           iwriteln('x is varied between Min and Max with increment Step');
         end;
  'b'  : begin
           iwriteln('border or b (on/off)');
           iwriteln('  if off hide axis on graphics');
         end;
  'c'  : begin
           iwriteln('changevar or change or c');
           iwriteln('  change x expr');
           iwriteln('set existing variable x with expression expr');
         end;
  'e'  : begin
           iwriteln('clear or e');
           iwriteln('clear current graphics');
         end;
  'f'  : begin
           iwriteln('file or f (on/off)');
           iwriteln('  file file_name x1 ... xn');
           iwriteln('save values of variables x1, ..., xn');
           iwriteln('in text file with name file_name');
         end;
  'g'  : begin
           iwriteln('graph or g');
           iwriteln('  graph x y1 .. yn');
           iwriteln('plot y1(x), ..., yn(x) on the next run');
           iwriteln('plot distributions of y1, .., yn if distrib mode on');
         end;
  'h'  : begin
           iwriteln('help or h or ?  -> help');
         end;
  'i'  : begin
           iwriteln('init or i');
           iwriteln('set t = 0 and set all variables to their initial values');
           iwriteln('  init n');
           iwriteln('initialize and set the random generator seed to n');
           iwriteln('  init 1');
           iwriteln('initialize and back to the default seed');
         end;
  'j'  : begin
           iwriteln('scatter or j');
           iwriteln('  scatter x y1 .. yn');
           iwriteln('on run command: plot y1(x), ..., yn(x) with regression line');
           iwriteln('on montecarlo command m T M : plot y1(T), ..., yM(T) with regression line');
         end;
  'k'  : begin
           iwriteln('spectrum or spec or k');
           iwriteln('  spectrum x');
           iwriteln('plot power spectrum of variable x over 1024 points');
         end;
  'l'  : begin
           iwriteln('line or l (on/off)');
           iwriteln('plot lines between points on graphics');
         end;
  'm'  : begin
           iwriteln('montecarlo or m');
           iwriteln('  montecarlo T M');
           iwriteln('run the models for T time steps, over M trajectories');
           iwriteln('  montecarlo T M Ext Esc');
           iwriteln('trajectories such that n < Ext are declared extinct,');
           iwriteln('  but are computed (default Ext threshold = 1)');
           iwriteln('trajectories such that n > Esc are declared to escape,');
           writeln('  but are computed (default Esc threshold = 10^7)');
         end;
  'n'  : begin
           iwriteln('newvar or new or n');
           iwriteln('  newvar x Expr');
           iwriteln('create new variable x with expression Expr');
         end;
  'o'  : begin
           iwriteln('correlation or correl or o');
           iwriteln('  correl x y');
           iwriteln('plot correlation of variables x and y over 100 points');
           iwriteln('  correl x');
           iwriteln('plot autocorrelation of variable x over 100 points');
         end;
  'p'  : begin
           iwriteln('property or prop or p');
           iwriteln('  property a');
           iwriteln('display properties of matrix a: eigenvalues, eigenvectors...');
         end;
  'q'  : begin
           iwriteln('lyap or lyapunov or q');
           iwriteln('  lyap m');
           iwriteln('give estimations of the lyapunov exponent of model m');
         end;
  'r'  : begin
           iwriteln('run or r');
           iwriteln('  run T ');
           iwriteln('run the models for T time steps');
           iwriteln('  run T S');
           iwriteln('run the models  for T time steps,');
           iwriteln('  with output every S time steps');
         end;
  's'  : begin
           iwriteln('sensitivity or sens or s');
           iwriteln('  sensitivity a x');
           iwriteln('display sensitivity of matrix a to changes in variable x');
           iwriteln('  sensitivity a x y');
           iwriteln('plot constant fitness curves, variables x and y being varied');
           iwriteln('  sensitivity v x');
           iwriteln('display sensitivity of eigenvectors to changes in variable x');
           iwriteln('v is the name of the model vector');
         end;
  't'  : begin
           iwriteln('text or t (on/off)');
           iwriteln('  text x1 .. xn');
           iwriteln('display values of variables x1, ..., xn');
         end;
  'u'  : begin
           iwriteln('distribution or distrib or u (on/off)');
           iwriteln('  distrib  D');
           iwriteln('distribution mode for graphics');
           iwriteln('plot distributions of graphic variables (command graph)');
           iwriteln(' D*j <= x < D*(j+1) (default D = 1)');
         end;
  'v'  : begin
           iwriteln('view or v');
           iwriteln('  view');
           iwriteln('display all objects');
           iwriteln('  view x1 ... xn');
           iwriteln('display objects x1, ..., xn');
         end;
  'w'  : begin
           iwriteln('window or w');
           iwriteln('  window i');
           iwriteln('create and select graphic window #i');
           iwriteln('next graph commands will apply to this window');
           iwriteln('-> graph xscale yscale addgraph clear distrib savegraph');
         end;
  'x'  : begin
           iwriteln('xscale or x (on/off)');
           iwriteln('  xscale Min Max');
           iwriteln('set bounds Min and Max on X axis');
         end;
  'y'  : begin
           iwriteln('yscale or y (on/off)');
           iwriteln('  yscale Min Max');
           iwriteln('set bounds Min and Max on Y axis');
         end;
  '+'  : begin
           iwriteln('addgraph or add or + (on/off)');
           iwriteln('plot next graph without erasing the previous one');
         end;
  '!'  : begin
           iwriteln('save or savegraph or !');
           iwriteln('  save xxx.bmp');
           iwriteln('store graphics in file xxx.bmp');
         end;
  '>'  : begin
           writeln('skip or > (on/off)');
           writeln('  skip n');
           writeln('first n time steps are not displayed in graphics');
         end;
   else
     begin
       if (s = '+') or (s = '-') or (s = '*') or (s = '/') or (s = '^') then
         begin
           iwriteln('a + b  sum of a and b');
           iwriteln('a - b  difference of a and b');
           iwriteln('a * b  product of a and b');
           iwriteln('a / b  quotient of a and b');
           iwriteln('a ^ b  a power b');
           iwriteln('-a     opposite of a');
         end
       else
       if (s = '\') then
         begin
           iwriteln('a \ b  real remainder');
           iwriteln('  7.4 \ 2 = 1.4');
         end
       else
       if (s = '<') or (s = '>' ) then
         begin
           iwriteln('a < b  comparison');
           iwriteln('  a < b = 1 if a < b and 0 otherwise');
           iwriteln('a > b  comparison');
           iwriteln('  a > b = 1 if a > b and 0 otherwise');
           iwriteln('  2 < 2 + 3 = 3; 2 < (2 + 3) = 1');
         end
       else
       if (s = '@') then
         begin
           iwriteln('x @ n = sum of n samples of x');
           iwriteln('  poisson(f)@n = poissonf(n,f)');
           iwriteln('  ber(p)@n = binomf(n,p)');
         end
       else
       if (s = 'sqrt') then
         begin
           iwriteln('sqrt(a) = square root of a');
           iwriteln('  domain: a >= 0');
         end
       else
       if (s = 'abs') then
         begin
           iwriteln('abs(a) = absolute value of a');
           iwriteln('  abs(1.5) = 1.5; abs(-2.1) = 2.1');
         end
       else
       if (s = 'trunc') then
         begin
           iwriteln('trunc(a) = integer part of a');
           iwriteln('  trunc(1.2)  =  1; trunc(1.9)  =  1');
           iwriteln('  trunc(-1.2) = -2; trunc(-1.9) = -2');
         end
       else
       if (s = 'round') then
         begin
           iwriteln('round(a) = nearest integer of a');
           iwriteln('  round(1.2)  =  1; round(1.9)  =  2');
           iwriteln('  round(-1.2) = -1; round(-1.9) = -2');
         end
       else
       if (s = 'sin') or (s = 'cos') or (s = 'tan') then
         begin
           iwriteln('sin(a = sinus of a (rad)');
           iwriteln('cos(a) = cosinus of a (rad)');
           iwriteln('tan(a) = tangent of a (rad)');
         end
       else
       if (s = 'asin') or ( s = 'acos') or (s = 'atan') then
         begin
           iwriteln('asin(a) = inverse sinus of a');
           iwriteln('acos(a) = inverse cosinus of a');
           iwriteln('atan(a) = inverse tangent of a');
         end
       else
       if (s = 'ln') or (s = 'ln0') or (s = 'log') or (s = 'exp') or (s = 'fact') then
         begin
           iwriteln('ln(a) = neperian logarithm of a');
           iwriteln('  domain: a > 0');
           iwriteln('ln0(a) = if a > 0 then ln(a) else 0');
           iwriteln('log(a) = decimal logarithm of a');
           iwriteln('  domain: a > 0');
         end
       else
       if (s = 'exp') or (s = 'fact') then
         begin
           iwriteln('exp(a) = exponential of a');
           iwriteln('fact(n) = n! = factorial n');
         end
       else
       if (s = 'gauss') or (s = 'gaussf') then
         begin
           iwriteln('gauss(s)');
           iwriteln('  normal distribution with mean 0 and standard deviation s');
           iwriteln('  domain: s >= 0');
           iwriteln('gaussf(m,s)');
           iwriteln('  normal distribution with mean m and standard deviation s');
           iwriteln('  domain: s >= 0');
           iwriteln('gaussf(m,s) = m + gauss(s)');
         end
       else
       if (s = 'lognormf') then
         begin
           iwriteln('lognormf(m,s)');
           iwriteln('  lognormal distribution with mean m and standard deviation s');
           iwriteln('  domain: m, s > 0');
         end
       else
       if (s = 'rand') then
         begin
           iwriteln('rand(a)');
           iwriteln('  uniform distribution over [0, a[');
           iwriteln('  rand(-a) = rand(a)');
           iwriteln('  trunc(rand(a)) -> random integer between 0 and a-1');
         end
       else
       if (s = 'ber') then
         begin
           iwriteln('ber(p)');
           iwriteln('  bernouilli distribution');
           iwriteln('  ber(p) = 0 with probability 1-p');
           iwriteln('         = 1 with probability p');
           iwriteln('  domain: 0 <= p <= 1');
         end
       else
       if (s = 'gamm') then
         begin
           iwriteln('gamm(m)');
           iwriteln('  gamma distribution with mean m');
           iwriteln('  domain: m >= 0');
         end
       else
       if (s = 'poisson') then
         begin
           iwriteln('poisson(f)');
           iwriteln('  poisson distribution with mean f');
           iwriteln('  domain: f >= 0');
         end
       else
       if (s = 'geom') then
         begin
           iwriteln('geom(p)');
           iwriteln('  geometric distribution with parameter p');
           iwriteln('  P(X = k) = p(1-p)^k');
           iwriteln('  domain: 0 <= p <= 1');
         end
       else
       if (s = 'expo') then
         begin
           iwriteln('expo(a)');
           iwriteln('  exponential distribution with parameter a');
           iwriteln('  domain: a > 0');
         end
       else
       if (s = 'if') then
         begin
           iwriteln('if(a,b1,b2)');
           iwriteln('  conditionnal: if a <> 0 then b1 else b2');
           iwriteln('  if(t>3,1,2) = 2 for t <= 3');
           iwriteln('  if(t>3,1,2) = 1 for t >  3');
         end
       else
       if (s = 'min') or (s = 'max' ) then
         begin
           iwriteln('min(a1, ..., an)   minimum of a1, ..., an');
           iwriteln('max(a1, ..., an)   maximum of a1, ..., an');
         end
       else
       if (s = 'stepf') then
         begin
           iwriteln('stepf(x,a,b)');
           iwriteln('  x variable name; a,b real values');
           iwriteln('  1 if a <= v <= b, 0 otherwise; v value of x');
         end
       else
       if (s = 'binomf') then
         begin
           iwriteln('binomf(n,p)');
           iwriteln('  binomial distribution with parameter p');
           iwriteln('  domain: n integer >= 0, 0 <= p <= 1');
         end
       else
       if (s = 'poissonf') then
         begin
           iwriteln('poissonf(n,f)');
           iwriteln('  sum of n trials of the Poisson distribution with mean f');
           iwriteln('  domain: n integer >= 0, f >= 0');
         end
       else
       if (s = 'nbinomf') or (s = 'nbinom1f') then
         begin
           iwriteln('binomf(r,p)');
           iwriteln('  negative binomial distribution');
           iwriteln('  domain: r real > 0, 0 < p <= 1');
           iwriteln('binom1f(m,s)');
           iwriteln('  negative binomial distribution with mean m and standard deviation s');
           iwriteln('  domain: 0 < m < s^2');
         end
       else
       if (s = 'betaf') or (s = 'beta1f' ) then
         begin
           iwriteln('betaf(a,b)');
           iwriteln('  beta distribution');
           iwriteln('  domain: a > 0, b > 0');
           iwriteln('beta1f(m,s)');
           iwriteln('  beta distribution with mean m and standard deviation s');
           iwriteln('  domain: 0 < m < 1, 0 < s^2 < m(1-m)');
           iwriteln('beta, beta1f are 0 outside [0,1]');
         end
       else
       if (s = 'bicof') then
         begin
           iwriteln('bicof(n,k)');
           iwriteln('  binomial coefficient C(n,k)');
           iwriteln('  domain: n >= 0, k >= 0');
         end
       else
       if (s = 'tabf') then
         begin
           iwriteln('tabf(p0, ...,pn)');
           iwriteln('  tabulated integer distribution');
           iwriteln('  domain: 0 <= pk <= 1, p0 + ... + pn = 1');
           iwriteln('  return k with probability pk');
           iwriteln('  tabf(0.4,0.6) = ber(0.6)');
         end
       else
       if (s = 'gratef') then
         begin
           iwriteln('gratef(x)');
           iwriteln('  growth rate of variable x');
           iwriteln('  computed as exp((ln(x(t))-ln(x(0)))/t)');
         end
       else
       if (s = 'bdf') then
         begin
           iwriteln('bdf(n,b,d,delta)');
           iwriteln('  sum of n trials of special geom distribution');
           iwriteln('  domain: n integer >= 0, b,d,delta >= 0');
           iwriteln('  b = birth rate; d = death rate; delta = time step');
           iwriteln('  used for discrete time version of continuous time birth-death process');
         end
       else
       if ( s = 'lambdaf' )then
         begin
           iwriteln('lambdaf(i,j)');
           iwriteln('  modulus of jth eigenvalue of ith model');
           iwriteln('  (in the order of declaration)');
           iwriteln('  domain: 1 <= i <= model_nb;');
           iwriteln('          1 <= j <= size of ith model');
           iwriteln('  lambdaf(i,1) = dominant eigenvalue of ith model');
         end
       else
       if ( s = 'prevf') then
         begin
           iwriteln('prevf(x,i)');
           iwriteln('  previous value of variable x');
           iwriteln('  i integer 0 <= i <= 100');
           iwriteln('  value of variable x at time t-i');
         end
       else
       if ( s = 'textf') then
         begin
           iwriteln('textf(x)');
           iwriteln('  extinction time of variable x');
           iwriteln('  textf(x) = t as soon as x(t) < extinction_threshold (default 1)');
           iwriteln('  otherwise textf(x) = 0');
         end
       else
       if ( s = 'meanf') then
         begin
           iwriteln('meanf(x)');
           iwriteln('  mean of variable x along time');
         end
       else
       if ( s = 'variancef') then
         begin
           iwriteln('variancef(x)');
           iwriteln('  variance of variable x along time');
         end
       else
       if ( s = 'skewnessf') then
         begin
           iwriteln('skewnessf(x)');
           iwriteln('  skewness of variable x along time');
         end
       else
       if ( s = 'cvf') then
         begin
           iwriteln('cvf(x)');
           iwriteln('  coefficient of variation of variable x along time');
           iwriteln('  CV(x) = sqrt(variancef(x)/meanf(x)');
         end
       else
       if ( s = 'meanzf') then
         begin
           iwriteln('meanzf(x)');
           iwriteln('  mean of variable x along time');
           iwriteln('  with "zero" values excluded');
           iwriteln('  i. e. values < extinction_threshold (default 1)');
         end
       else
       if ( s = 'variancezf') then
         begin
           iwriteln('variancezf(x)');
           iwriteln('  mean of variable x along time');
           iwriteln('  with "zero" values excluded');
           iwriteln('  i. e. values < extinction_threshold (default 1)');
         end
       else
       if ( s = 'cvzf') then
         begin
           iwriteln('cvzf(x)');
           iwriteln('  coefficient of variation of variable x along time');
           iwriteln('  with "zero" values excluded');
           iwriteln('  i. e. values < extinction_threshold (default 1)');
           iwriteln('  CVZ(x) = sqrt(variancezf(x)/meanzf(x)');
         end
       else
       if ( s = 'nzf') then
         begin
           iwriteln('nzf(x)');
           iwriteln('  number of "zero" values of variable x along time');
           iwriteln('  i. e. number of values < extinction_threshold (default 1)');
         end
       else
       if ( s = 'nef') then
         begin
           iwriteln('nef(x)');
           iwriteln('  number of "extinctions" of variable x along time');
           iwriteln('  i. e. number of dates s <= t such that');
           iwriteln('  x(s-1) >= extinction_threshold and');
           iwriteln('  x(s)< extinction_threshold (default 1)');
         end
       else
       if ( s = 'nif') then
         begin
           iwriteln('nif(x)');
           iwriteln('  number of "immigrations" of variable x along time');
           iwriteln('  i. e. number of dates s <= t such that');
           iwriteln('  x(s-1) < extinction_threshold and');
           iwriteln('  x(s) >= extinction_threshold (default 1)');
         end
       else
       if ( s = 'extratef') then
         begin
           iwriteln('extratef(x)');
           iwriteln('  extinction rate of variable x along time');
           iwriteln('  = probability that x(t) < extinction_threshold (default 1)');
           iwriteln('  and x(t-1) >= extinction_threshold (default 1)');
         end
       else
       if ( s = 'immratef') then
         begin
           iwriteln('immratef(x)');
           iwriteln('  immigration rate of variable x along time');
           iwriteln('  = probability that x(t-1) < extinction_threshold (default 1)');
           iwriteln('  and x(t) >= extinction_threshold (default 1)');
         end;
     end;
  end;
end;

procedure interp(s : string);
var  pos : integer;
     c : char;
     s1,s2 : string;
begin
  s := tronque(minuscule(s));
  if ( s = '' ) then exit;
  pos := position(s,' ');
  if ( pos > 0 ) then
    begin
      coupe(s,pos,s1,s2);
      c := traduc(s1);
      s := tronque(s2);
    end
  else
    begin
      c := traduc(s);
      s := '';
    end;
  case c of
  'a'   : interp_param(s);
  'b'   : interp_bord(s);
  'c'   : interp_chg_variable(s);
  'e'   : interp_efface;
  'f'   : interp_fic(s);
  'g'   : interp_graph(s);
  'h'   : interp_help(s);
  'i'   : interp_init(s);
  'j'   : interp_scatter(s);
  'k'   : interp_spectre(s);
  'l'   : interp_line(s);
  'm'   : interp_run_carlo(s);
  'n'   : interp_newvar(s);
  'o'   : interp_correl(s);
  'p'   : interp_prop(s);
  'q'   : interp_lyap(s);
  'r'   : interp_run(s);
  's'   : interp_sensibilite(s);
  't'   : interp_texte(s);
  'u'   : interp_distrib(s);
  'v'   : interp_voir(s);
  'w'   : interp_graph_window(s);
  'x'   : interp_xscale(s);
  'y'   : interp_yscale(s);
  '+'   : interp_addgraph;
  '_'   : interp_info;
  '!'   : interp_savegraph(s);
  '>'   : interp_skip(s);
  '{'   : interp_comment(s);
  '&'   : interp_tempo(s);
  '('   : interp_notext;
  '#'   : interp_perso(s);
  {';'   : interp_carlo_n0;} {ccccc}
  ';'   : interp_tobs(s); {ccccc}
  else;
  end;
end;

end.

