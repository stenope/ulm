unit gviewvar;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  Grids, ExtCtrls, ComCtrls,
  jglobvar;

type
  tform_view_variables = class(TForm)
    stringgrid1: TStringGrid;
    statusbar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure status;
    procedure FormActivate(Sender: TObject);
    procedure stringgrid1SelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
    procedure change_variable(x : integer; s : string);
    procedure stringgrid1SetEditText(Sender: TObject; ACol, ARow: Integer;
      const Value: WideString);
    procedure stringgrid1KeyPress(Sender: TObject; var Key: Char);
    procedure statusbar1PanelClick(Sender: TObject; Panel: TStatusPanel);
  private
    alpha : boolean;
    ordre_var : array[1..variable_nb_max] of integer;
    x_var : integer;
    s_exp : string;
    procedure erreur(s : string);
    procedure ordre_alphabetique;
    procedure ordre_evaluation;
  public
    err_viewvar : boolean;
  end;

var  form_view_variables: tform_view_variables;

implementation

uses jutil,jsymb,jsyntax,jeval,gulm;

{$R *.lfm}

procedure tform_view_variables.erreur(s : string);
begin
  erreur_('View Variables - ' + s);
  err_viewvar := true;
end;

procedure tform_view_variables.status;
begin
  statusbar1.Panels[0].Text := 'Nb_var = ' + IntToStr(variable_nb);
  if ( x_var <> 0 ) then
    statusbar1.Panels[1].Text := 'Change: ' +
      s_ecri_var(x_var) + ' = ' + s_exp
  else
    statusbar1.Panels[1].Text :=  '';
  if alpha then
    statusbar1.Panels[2].Text := 'Alphabetic order'
  else
    statusbar1.Panels[2].Text := 'Evaluation order'
end;

procedure tform_view_variables.FormCreate(Sender: TObject);
begin
  Left   := 114;
  Top    := 100;
  Height := 442;
  Width  := 800;
  adjust(self);
  with stringgrid1 do
    begin
      RowCount  := 2;
      FixedRows := 1;
      ColCount  := 4;
      FixedCols := 1;
      ColWidths[0] := 80;
      ColWidths[1] := 80;
      ColWidths[2] := 80;
      ColWidths[3] := form_view_variables.Width-3*(80+4);
      cells[0,0] := 'Name';
      cells[1,0] := 'Initial value';
      cells[2,0] := 'Actual value';
      cells[3,0] := 'Expression';
    end;
  x_var := 0;
  s_exp := '';
  alpha := false;
  err_viewvar := false;
end;

procedure tform_view_variables.ordre_alphabetique;
label 1;
var x,y,z,u,v,w,h,ln2 : integer;
    s1,s2 : string;
begin
  for x := 1 to variable_nb do ordre_var[x] := x;
  ln2 := trunc(ln(variable_nb)*1.442695022 + 0.00001);
  z := variable_nb;
  for x := 1 to ln2 do
    begin
      z := z div 2;
      u := variable_nb - z;
      for y := 1 to u do
        begin
          v := y;
1:
          w := v + z;
          s1 := s_ecri_var(ordre_var[w]);
          s2 := s_ecri_var(ordre_var[v]);
          if ( s1 < s2 ) then
            begin
              h := ordre_var[v];
              ordre_var[v] := ordre_var[w];
              ordre_var[w] := h;
              v := v - z;
              if ( v >= 1 ) then goto 1;
            end;
        end;
    end;
end;

procedure tform_view_variables.ordre_evaluation;
var k,i,x : integer;
begin
  ordre_var[1] := xtime; { le temps est la première variable... }
  k := 1;
  for x := 2 to variable_nb do with variable[x] do
    if ( exp_type = type_ree ) then
      if ( xrel = 0 ) and ( xvec = 0 ) then
        begin
          k := k + 1;
          ordre_var[k] := x;
        end;
  for x := 1 to modele_nb do with modele[x] do
    if ( xmat <> 0 ) then
      for i := 1 to size do with vec[xvec] do
        begin
          k := k + 1;
          ordre_var[k] := exp[i];
        end
    else
      for i := 1 to size do with rel[xrel[i]] do
        begin
          k := k + 1;
          ordre_var[k] := xvar;
        end;
  for x := 1 to rel_nb do with rel[x] do
    if ( xmodele = 0 ) then
      begin
        k := k + 1;
        ordre_var[k] := xvar;
      end;
  for x := 1 to ordre_eval_nb do
    begin
      k := k + 1;
      ordre_var[k] := ordre_eval[x];
    end;
end;

procedure tform_view_variables.FormActivate(Sender: TObject);
var i,x,max : integer;
begin
  with stringgrid1 do
    begin
      RowCount := 1 + variable_nb;
      if alpha then
        ordre_alphabetique
      else
        ordre_evaluation;
      for i := 1 to variable_nb do
        begin
          x := ordre_var[i];
          with variable[x] do
            begin
              Cells[0,i] := s_ecri_var(x);
              Cells[1,i] := s_ecri_val(val0);
              Cells[2,i] := s_ecri_val_variable(x);
              Cells[3,i] := s_ecri(exp,exp_type);
            end;
        end;
      max := 0;
      for i := 0 to RowCount -1 do
        begin
          x := Canvas.TextWidth(Cells[3,i]);
          if ( x > max ) then max := x;
        end;
      ColWidths[3] := max + GridLineWidth + 10;
    end;
  status;
end;

procedure tform_view_variables.change_variable(x : integer; s : string);
var v,tv : integer;
begin
  lirexp(s,v,tv);
  if err_syntax then
    begin
      err_syntax  := false;
      exit;
    end;
  if ( variable[x].xrel <> 0 ) then
    if ( tv <> type_ree ) then
      begin
        erreur('relation-variable must be set to a real value');
        exit;
      end;
  set_variable(x,v,tv);
  form_ulm.run_initExecute(nil);
end;

procedure tform_view_variables.StringGrid1SelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( arow > 1 ) and ( acol = 3 );
end;

procedure tform_view_variables.stringgrid1SetEditText(Sender: TObject;
  ACol, ARow: Integer; const Value: WideString);
begin
  with stringgrid1 do
    begin
      s_exp := tronque(minuscule(value));
      x_var := trouve_variable(trouve_dic(Cells[0,arow]));
    end;
end;

procedure tform_view_variables.stringgrid1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if ( key = chr(13) ) then
    if ( x_var <> 0 ) then
      begin
        change_variable(x_var,s_exp);
        FormActivate(nil);
      end;
  x_var := 0;
  s_exp := '';
end;

procedure tform_view_variables.statusbar1PanelClick(Sender: TObject;
  Panel: TStatusPanel);
begin
  if ( panel.index = 2 ) then
    begin
      alpha := not alpha;
      FormActivate(nil);
    end;
end;

end.
