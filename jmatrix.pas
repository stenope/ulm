unit jmatrix;

{$MODE Delphi}

interface

uses jglobvar,jmath;

function  mattimedep(x : integer) : boolean;
function  matvecdep(x,y : integer) : boolean;
function  matrandom(x : integer): boolean;
function  matleslie(n : integer;a : rmat_type ) : boolean;
function  matleslie2(n : integer;a : rmat_type ) : boolean;
function  matclassetaille(n : integer;a : rmat_type ) : boolean;
function  matmultisite(n : integer;a : rmat_type;var p : integer) : boolean;
function  test_matmultisite(n,p : integer;a : rmat_type) : boolean;

implementation

uses SysUtils,
     jutil,jsymb,jsyntax,jeval,
     gulm;

{ ------ proprietes des matrices ------ }

function  matleslie(n : integer;a : rmat_type ) : boolean;
{  teste si a est une matrice de leslie }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i-1] >= 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n do
    for j := 1 to n do
      if ( j <> i-1 ) then b := b and ( a[i,j] = 0.0 );
  matleslie := b;
end;

function  matleslie2(n : integer;a : rmat_type ) : boolean;
{ teste si a est une matrice de leslie etendue ( 0 < a[n,n] < 1 ) }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i-1] > 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n-1 do
    for j := i to n do
      b := b and ( a[i,j] = 0.0 );
  for j := 1 to n-2 do
    for i := j+2 to n do
      b := b and ( a[i,j] = 0.0 );
  b := b and ( a[n,n] > 0.0  ) and ( a[n,n] < 1.0 );
  matleslie2 := b;
end;

function  matclassetaille(n : integer;a : rmat_type ) : boolean;
{ teste si a est une matrice de modele en classes de taille }
var i,j : integer;
    b : boolean;
begin
  b := true;
  for j := 1 to n do b := b and ( a[1,j] >= 0.0 );
  for i := 2 to n do b := b and ( a[i,i] > 0.0 ) and ( a[i,i] < 1.0 );
  for i := 2 to n do b := b and ( a[i,i-1] > 0.0 ) and ( a[i,i-1] <= 1.0 );
  for i := 2 to n-1 do
    for j := i+1 to n do
      b := b and ( a[i,j] = 0.0 );
  for i := 3 to n do
    for j := 1 to i-2 do
      b := b and ( a[i,j] = 0.0 );
  matclassetaille := b;
end;

function  test_matmultisite(n,p : integer;a : rmat_type) : boolean;
{ teste si a est une matrice multisite dont les blocs sont leslie ou leslie2 }
{ avec p = nombre de sites  }
var  i,j,m,ig,jg,k : integer;
     b,b1,b2 : boolean;
     g : rmat_type;
begin
  m := n div p; { nb de classes d'age des sous-populations }
  b := true;
  b1 := true;
  b2 := true;
  { test que les blocs diagonaux sont tous leslie ou leslie2: }
  for k := 1 to p do
    begin
      ig := 0;
      for i := (k-1)*m + 1 to k*m do
        begin
          ig := ig + 1;
          jg := 0;
          for j := (k-1)*m + 1 to k*m do
            begin
              jg := jg + 1;
              g[ig,jg] := a[i,j];
            end;
        end;
      b1 := b1 and matleslie(m,g);
      b2 := b2 and matleslie2(m,g);
    end;
  { test que les blocs non diagonaux ne contiennent que des termes entre 0 et 1: }
  b := b and (b1 or b2);
  for k := 1 to p do
    for i := (k-1)*m + 1 to k*m do
      for j := k*m + 1 to n do
        begin
          b := b and ( a[i,j] >= 0.0 ) and ( a[i,j] <= 1.0 );
          b := b and ( a[j,i] >= 0.0 ) and ( a[j,i] <= 1.0 );
        end;
  test_matmultisite := b;
end;

function  matmultisite(n : integer;a : rmat_type;var p : integer) : boolean;
{ a est une matrice multisite dont les blocs sont leslie ou leslie2 }
{ retourne p = nombre de sites }
{ correction 10/04/2002 }
{ correction 05/07/2004 }
label 1;
var q : integer;
begin
  { chercher p diviseur de n }
  p := 1;
  matmultisite := false;
  q := 1;
1:
  q := q + 1;
  {if ( q > round(sqrt(n) + 1 ) then exit;}
  if ( n div q < 2 ) then exit;
  if ( n mod q <> 0 ) then goto 1;
  if test_matmultisite(n,q,a) then
    begin
      p := q;
      matmultisite := true;
      exit;
    end;
  goto 1;
end;

function  mattimedep(x : integer) : boolean;
{ teste si la matrice d'indice x est une matrice time-dependent }
var i,j : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do
      b := b or occur2(exp[i,j],exp_type[i,j],xtime);
  mattimedep := b;
end;

function  matvecdep(x,y : integer) : boolean;
{ tsetes si la matrice d'indice x est une matrice vector-dependent }
{ = density-dependent }
var i,j,k : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do 
      for k := 1 to size do 
        b := b or occur2(exp[i,j],exp_type[i,j],vec[y].exp[k]);
  matvecdep := b;
end;

function  matrandom(x : integer): boolean;
{ teste si la matrice d'indice x est une matrice }
{ dependant de fonctions aleatoires }
var i,j,z,tz : integer;
    b : boolean;
begin
  b := false;
  with mat[x] do
  for i := 1 to size do
    for j := 1 to size do
      begin 
        z  := exp[i,j];
        tz := exp_type[i,j];
        b  := b or occur_op1(z,tz,op1_rand);
        b  := b or occur_op1(z,tz,op1_gauss);
        b  := b or occur_op1(z,tz,op1_ber);
        b  := b or occur_op1(z,tz,op1_poisson);
        b  := b or occur_op1(z,tz,op1_geom);
        b  := b or occur_op1(z,tz,op1_expo);
        b  := b or occur_op1(z,tz,op1_gamm);
        b  := b or occur_fun(z,tz,fun_betaf);
        b  := b or occur_fun(z,tz,fun_beta1f);
        b  := b or occur_fun(z,tz,fun_binomf);
        b  := b or occur_fun(z,tz,fun_nbinomf);
        b  := b or occur_fun(z,tz,fun_nbinom1f);
        b  := b or occur_fun(z,tz,fun_gaussf);
        b  := b or occur_fun(z,tz,fun_lognormf);
        b  := b or occur_fun(z,tz,fun_tabf);
      end;
  matrandom := b;
end;


{ ------  calculs de sensibilite  ------ }

procedure sensibilite_rho_period(n : integer;a : rmat_type;lambda1 : extended; v1,w1 : rvec_type;lambda2 : cmx);
{ v1,w1 vecteurs propres a gauche et a droite associes         }
{ a la valeur propre dominante reelle lambda1 de la matrice a  }
{ lambda2 deuxieme valeur propre de la matrice a               }
{ b est la matrice des sensibilites pour le damping ratio      }
{ puis pour la periode                                         }
var i,j : integer;
    r,a2,h,rho,k2,theta : extended;
    b1,b : rmat_type;
    w2,v2 : cvec_type;
    z,u : cmx;
    b2 : cmat_type;
    s : string;
begin
  r := vecpscal(n,v1,w1);
  for i := 1 to n do
    for j := 1 to n do
      b1[i,j] := v1[i]*w1[j]/r; { b1[i,j] = d(lambda1)/d(a[i,j]) }
  lambda2.im := abs(lambda2.im);
  a2 := cmxmod(lambda2);
  if ( a2 = 0.0 ) then
    begin
      iwriteln('-> Eigenvalue lambda2 is 0');
      exit;
    end;
  rho := lambda1/a2;  
  matvecpropdroite(n,a,lambda2,w2);
  if err_math then
    begin
      err_math := false;
      exit;
    end;
  matvecpropgauche(n,a,lambda2,v2);
  if err_math then
    begin
      err_math := false;
      exit;
    end;
  cvecpscal(n,v2,w2,z);
  for i := 1 to n do
    for j := 1 to n do
      begin
        cmxconj(v2[i],u);
        cmxprod(u,w2[j],u);
        cmxdiv(u,z,b2[i,j]); { b2[i,j] = d(lambda2)/d(a[i,j]) }
      end;
  r := rho/a2;
  for i := 1 to n do
    for j := 1 to n do
      begin
        h := lambda2.re*b2[i,j].re + lambda2.im*b2[i,j].im;
        b[i,j] := (b1[i,j] - r*h)/a2;
      end;
  iwriteln('Sensitivities to damping ratio:');
  for i := 1 to n do
    begin
      s := '';
      for j := 1 to n do s := s + Format('%10.4f',[b[i,j]]);
      iwriteln(s);
    end;
  for i := 1 to n do
    for j := 1 to n do
      begin
        r := a[i,j]/rho;
        b[i,j] := r*b[i,j];
      end;
  iwriteln('Elasticities to damping ratio:');
  for i := 1 to n do
    begin
      s := '';
      for j := 1 to n do s := s + Format('%10.4f',[b[i,j]]);
      iwriteln(s);
    end;

  theta := cmxarg(lambda2);
  if ( theta = 0.0 ) then
    iwriteln('-> Eigenvalue lambda2 is real');
  k2 := -dpi/(theta*theta*a2*a2);
  for i := 1 to n do
    for j := 1 to n do
      begin
        h := lambda2.re*b2[i,j].im - lambda2.im*b2[i,j].re;
        b[i,j] := k2*h;
      end;
  iwriteln('Sensitivities to period:');
  for i := 1 to n do
    begin
      s := '';
      for j := 1 to n do s := s + Format('%10.4f',[b[i,j]]);
      iwriteln(s);
    end;
  for i := 1 to n do
    for j := 1 to n do
      begin
        h := theta*a[i,j]/dpi;
        b[i,j] := h*b[i,j];
      end;
  iwriteln('Elasticities to period:');
  for i := 1 to n do
    begin
      s := '';
      for j := 1 to n do s := s + Format('%10.4f',[b[i,j]]);
      iwriteln(s);
    end;
end;

procedure sensibilite_vecprop_x(a,x : integer);
label 10;
var i,j,y,ty,n,u,k,l,h,ii : integer;
    somw,somv : rvec_type;
    dw,dv,lambda,w,v,vj,wj,w1,v1 : cvec_type;
    r : extended;
    p,q,z,sw,sv,m : cmx;
    bbb   : ^rmat_type;
    cm,dm : ^cmat_type;
    s : string;
begin
  new(bbb);
  new(cm);
  new(dm);
  n := mat[a].size;
  for i := 1 to n do
    for j := 1 to n do with mat[a] do
      begin
        deriv(exp[i,j],exp_type[i,j],x,y,ty);
        if err_eval then
          begin
            err_eval := false;
            goto 10;
          end;
        bbb^[i,j] := eval(y,ty); { matrice des derivees }
      end;
  matvalprop(n,mat[a].val,lambda);
  if err_math then
    begin
      err_math := false;
      goto 10;
    end;
  matvecpropdroite(n,mat[a].val,lambda[1],w);
  if err_math then
    begin
      err_math := false;
      goto 10;
    end;
  matvecpropgauche(n,mat[a].val,lambda[1],v);
  if err_math then
    begin
      err_math := false;
      goto 10;
    end;
  r := 0.0;
  for u := 1 to n do r := r + w[u].re;
  for u := 1 to n do w[u].re := w[u].re/r;
  r := v[1].re;
  for u := 1 to n do v[u].re := v[u].re/r;
  for j := 2 to n do
    begin
      matvecpropdroite(n,mat[a].val,lambda[j],wj);
      if err_math then
        begin
          err_math := false;
          goto 10;
        end;
      for ii := 1 to n do cm^[ii,j] := wj[ii];
      matvecpropgauche(n,mat[a].val,lambda[j],vj);
      if err_math then
        begin
          err_math := false;
          goto 10;
        end;
      for ii := 1 to n do dm^[ii,j] := vj[ii];
    end;
  iwriteln('Sensitivities of eigenvectors of matrix ' + s_ecri_mat(a) + ' to ' + s_ecri_var(x) +':');
  iwriteln('          left          right');
  for u := 1 to n do
    begin
     somw[u] := 0.0;
     somv[u] := 0.0;
     for k := 1 to n do
       for l := 1 to n do
         begin
           for h := 1 to n do
             begin
               sw.re := 0.0;
               sw.im := 0.0;
               sv.re := 0.0;
               sv.im := 0.0;
               for j := 2 to n do
                 begin
                   for ii := 1 to n do wj[ii] := cm^[ii,j];
                   for ii := 1 to n do vj[ii] := dm^[ii,j];
                   cvecpscal(n,vj,wj,p);
                   cmxdiff(lambda[1],lambda[j],m);
                   cmxconj(vj[k],q);
                   cmxdiv(q,m,z);
                   cmxdiv(z,p,z);
                   cmxprod(z,wj[h],z);
                   cmxsom(sw,z,sw);
                   cmxconj(wj[l],q);
                   cmxdiv(q,m,z);
                   cmxconj(p,q);;
                   cmxdiv(z,q,z);
                   cmxprod(z,vj[h],z);
                   cmxsom(sv,z,sv);
                 end;
               cmxprod(sw,w[l],dw[h]);
               cmxprod(sv,v[k],dv[h]);
             end;
           sw.re := 0.0;
           sw.im := 0.0;
           for h := 1 to n do cmxsom(sw,dw[h],sw);
           for h := 1 to n do cmxprod(sw,w[h],w1[h]);
           for h := 1 to n do cmxdiff(dw[h],w1[h],dw[h]);
           somw[u] := somw[u] + dw[u].re*bbb^[k,l];
           for h := 1 to n do cmxprod(v[h],dv[1],v1[h]);
           for h := 1 to n do cmxdiff(dv[h],v1[h],dv[h]);
           somv[u] := somv[u] + dv[u].re*bbb^[k,l];
         end;
      iwriteln(Format('%3d%10.4f%10.4f',[u,somv[u],somw[u]]));
      {iwriteln(Format('%3d%10.4f',[u,somw[u]])); }
    end;
10:
  dispose(bbb);
  dispose(cm);
  dispose(dm);
end;

end.
