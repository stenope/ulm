unit jglobvar;

{$MODE Delphi}


{  @@@@@@  variables globales   @@@@@@  }


interface

const  pisur2 = pi/2.0;
       bigint = 2147483647;
       hortab = chr(9);

{ ------  types  utilitaires  ------ }

const  maxextended = 1.0e+4932;
       minextended = 4.0e-4932;

const  matmax = 100;

type   rmat_type = array[1..matmax,1..matmax] of extended;
       imat_type = array[1..matmax,1..matmax] of integer;
       rvec_type = array[1..matmax] of extended;
       ivec_type = array[1..matmax] of integer;
       rvec3_type = array[1..matmax,1..matmax,1..matmax] of extended;

{ ------  types internes  ------ }

const  type_lis      =  0;
       type_ree      =  1;
       type_op1      =  3;
       type_op2      =  4;
       type_variable =  5;
       type_fun      =  6;
       type_arg      =  7;
       type_vec      =  8;
       type_mat      =  9;
       type_rel      = 10;
       type_modele   = 11;
       type_char     = 20;
       type_inconnu  = 100;


{ ------ dictionnaire ------ } 
 
var    dic,dic_nb,char_nb : integer; 


{ ------  listes  ------ }

const  lis_nb_max = 50000;

type   lis_type = record
                    car_type : integer;
                    car      : integer;
                    cdr      : integer;
                  end;
                     
type   alis       = array[1..lis_nb_max] of lis_type;
var    lis        : alis;
       lis_nb     : integer;
       lis_lib    : integer;


{ ------  operateurs   ------ }

const  op2_plus    =  1;
       op2_mult    =  2;
       op2_moins   =  3;
       op2_div     =  4;
       op2_puis    =  5;
       op2_infe    =  6;
       op2_supe    =  7;
       op2_mod     =  8;
       op2_convol  = 10;
       
const  op1_moins   =  101;
       op1_cos     =  102;
       op1_sin     =  103;
       op1_tan     =  104;
       op1_acos    =  105;
       op1_asin    =  106;
       op1_atan    =  107;
       op1_ln      =  108;
       op1_log     =  109;
       op1_exp     =  110;
       op1_fact    =  111;
       op1_sqrt    =  112;
       op1_abs     =  113;
       op1_trunc   =  114;
       op1_round   =  115;
       op1_ln0     =  116;

       op1_gauss   =  130;
       op1_rand    =  131;
       op1_ber     =  132;
       op1_gamm    =  133;
       op1_poisson =  134;
       op1_geom    =  135;
       op1_expo    =  136;

                      
{ ------  reels  ------ }  

const  ree_nb_max = 2000;

type   ree_type = record
                     val  : extended;
                     mark : integer;
                     suiv : integer;
                   end;

var    ree        : array[1..ree_nb_max] of ree_type;
       ree_nb     : integer;
       ree_lib    : integer;
       ree_deb    : integer;
       ree_zero   : integer;
       ree_un     : integer;
       ree_deux   : integer;
       ree_reg1   : integer;
       ree_reg2   : integer;


{ ------  variables  ------ }  

const  variable_nb_max = 5000;

const  maxprev = 100;
       maxprev1 = maxprev + 1;

type   prev_type = array[0..maxprev] of extended;

type   variable_type = record
                         nom    : integer;
                         xrel   : integer;   { relation associee si <> 0 }
                         xvec   : integer;   { vecteur associe si <> 0 }
                         exp_type : integer; { type expression associee }
                         exp    : integer;   { expression associee }
                         val0   : extended;  { valeur initiale }
                         val    : extended;  { valeur courante }
                         prev   : prev_type; { memoire des dernieres valeurs }
                         te     : integer;   { temps d'extinction val < seuil_ext }
                         sum    : extended;  { somme des valeurs }
                         sumz   : extended;  { somme des valeurs non nulles, ou >= seuil_ext }
                         sum2   : extended;  { somme des carres des valeurs }
                         sum2z  : extended;  { somme des carres des valeurs non nulles, ou >= seuil_ext }
                         sum3   : extended;  { somme des cubes des valeurs }
                         moy    : extended;  { moyenne }
                         variance  : extended; { variance }
                         skewness  : extended; { skewness }
                         moyz   : extended;  { moyenne des valeurs non nulles, ou >= seuil_ext }
                         variancez : extended; { variance des valeurs non nulles, ou >= seuil_ext }
                         nz     : integer;   { nombre de valeurs 0, ou < seuil-ext }
                         ne     : integer;   { nombre d'extinctions (seuil_ext) }
                         ni     : integer;   { nombre d'immigrations }
                         er     : extended;  { extinction rate }
                         ir     : extended;  { immigration rate }
                       end;

var    variable    : array[1..variable_nb_max] of variable_type;
       variable_nb : integer;
       variable_nb_predef : integer;
       xtime : integer;
       pprev : integer;


{ ------  fonctions  ------ }

const  fun_nb_max   = 100;
       larg_nb_max  = 30;

type   larg_type  = array[1..larg_nb_max] of integer;

       rlarg_type = array[1..larg_nb_max] of extended;

type   fun_type = record
                    nom       : integer;
                    nb_arg    : integer;   { nombre d'arguments }
                    xarg      : larg_type; { tableau des arguments }
                    exp_type  : integer;   { type expression associee }
                    exp       : integer;   { expression associee }
                  end;

var    fun           : array[1..fun_nb_max] of fun_type;
       fun_nb        : integer;
       fun_nb_predef : integer;
       fun_if        : integer;
       fun_min       : integer;
       fun_max       : integer;
       fun_stepf     : integer;
       fun_gaussf    : integer;
       fun_lognormf  : integer;
       fun_binomf    : integer;
       fun_poissonf  : integer;
       fun_nbinomf   : integer;
       fun_nbinom1f  : integer;
       fun_betaf     : integer;
       fun_beta1f    : integer;
       fun_tabf      : integer;
       fun_bicof     : integer;
       fun_gratef    : integer;
       fun_bdf       : integer;
       fun_lambdaf   : integer;
       fun_prevf     : integer;

       fun_textf      : integer;
       fun_meanf      : integer;
       fun_variancef  : integer;
       fun_skewnessf  : integer;
       fun_cvf        : integer;
       fun_meanzf     : integer;
       fun_variancezf : integer;
       fun_cvzf       : integer;
       fun_nzf        : integer;
       fun_nef        : integer;
       fun_nif        : integer;
       fun_extratef   : integer;
       fun_immratef   : integer;


{ ------  arguments des fonctions  ------ }

const  arg_nb_max = 1000;

type   arg_type = record
                    nom : integer;
                    val : extended;
                  end;

var    arg      : array[1..arg_nb_max] of arg_type;
       arg_nb   : integer;


{ ------  modeles  ------ }

const  modele_nb_max   = 5;

type   modele_type = record
                       nom    : integer;
                       size   : integer;    { taille = nombre de relations ou taille matrice et vecteur }
                       xmat   : integer;    { matrice associee }
                       xvec   : integer;    { vecteur associe  }
                       xrel   : ivec_type;  { tableau des relations associees }
                       pop0   : extended;   { nombre initial d'individus }
                       pop    : extended;   { nombre courant d'individus }
                     end;

var    modele      : array[1..modele_nb_max] of modele_type;
       modele_nb   : integer;


{ ------  vecteurs  ------ }                       

const  vec_nb_max = modele_nb_max;

type   vec_type = record
                    nom       : integer;
                    xmodele   : integer;
                    size      : integer;
                    exp_type  : ivec_type;
                    exp       : ivec_type;
                    val0      : rvec_type;
                    val       : rvec_type;
                  end;

var    vec      : array[1..vec_nb_max] of vec_type;
       vec_nb   : integer;


{ ------  matrices  ------ }                       

const  mat_nb_max = modele_nb_max;

type   mat_type = record
                    nom       : integer;
                    xmodele   : integer;
                    size      : integer;
                    exp_type  : imat_type;
                    exp       : imat_type;
                    val0      : rmat_type;
                    val       : rmat_type;
                    repro_nb  : integer; // number of reproductive transitions f_ij
                    repro_i   : ivec_type;  // i of f_ij
                    repro_j   : ivec_type;  // j of f_ij
                  end;

var    mat      : array[1..mat_nb_max] of mat_type;
       mat_nb   : integer;
       

{ ------  relations  ------ }                       

const  rel_nb_max = 500;

type   rel_type = record
                     nom       : integer;
                     xmodele   : integer;  { modele associe si <> 0 }
                     exp_type  : integer;  { type expression associee }
                     exp       : integer;  { expression associee }
                     xvar      : integer;  { variable associee }
                     val0      : extended; { valeur initiale }
                     val       : extended; { valeur courante }
                   end;

var    rel      : array[1..rel_nb_max] of rel_type;
       rel_nb   : integer;


{ ------  fichiers de sortie  ------ }

const  fic_nb_max = 5;
       var_fic_nb_max = 10;

type   var_fic_type = array[1..var_fic_nb_max] of integer;

type   fic_type = record
                    nam : string;
                    f   : TextFile;
                    var_fic_nb : integer;   { nombre de variables }
                    var_fic : var_fic_type; { variables }
                    precis  : var_fic_type; { precision associee }
                  end;

var    fic : array[1..fic_nb_max] of fic_type;
       fic_nb : integer;

       
{ ------  variables globales  ------ }

const  seuil_ext_def = 1.0;
       seuil_div_def = 10000000.0;

var    t__     : integer; { temps courant }
       traj__  : integer; { trajectoire courante (montecarlo) }

       nomfic : string;           { nom fichier modele courant }
       modelfileopened : boolean; { indicateur ouverture fichier modele }
       nomficin : string;         { nom fichier commande }
       inputfileopened : boolean; { indicateur ouverture fichier commande }
       nomficout : string;        { nom fichier output }
       outputfile : boolean;      { indicateur ouverture fichier output }
       ficout : text;
       nb_cycle : integer;       { nombre de cycles run }
       nb_cycle_carlo : integer; { nombre de cycles montecarlo }
       nb_run_carlo : integer;   { nombre de trajectoires montecarlo }
       seuil_ext : extended;     { seuil d'extinction montecarlo }
       seuil_div : extended;     { seuil de divergence montecarlo }
       param  : boolean;         { indicateur de variation de parametre  on/off }
       xparam : integer;         { variable parametre courante }
       param_exp_type_sav : integer; { sauvegarde type variable param }
       param_exp_sav : integer;      { sauvegarde exp  variable param }
       param_min : extended;
       param_max : extended;
       param_pas : extended;     { valeurs extremes et pas du parametre }
       notext : boolean;         { affichage texte interp on/off }
       runstop : boolean;        { indicateur interrupt exec }

       {carlo_n0 : boolean;}  {ccccc}
       {carlo_tab_n0 : array[1..10000] of extended;} {ccccc} { voir vecvec_type 0..M }

       tobs : integer; { voir jeval, maj_var2  }

implementation

end.
