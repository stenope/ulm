unit gtext;

{$MODE Delphi}

interface

uses
  LazFileUtils, SysUtils, Types, Classes, Variants, FileUtil, Graphics, Controls, Forms,
  Dialogs, Menus, ExtCtrls, ComCtrls, Grids, ActnList, ImgList;

const maxvartext = 18;
      maxtext    = 10000;

type  tab_text_type = array[0..maxtext] of extended;

type
  tform_text = class(TForm)
    StatusBar1: TStatusBar;
    MainMenu1: TMainMenu;
    ToolBar1: TToolBar;
    stringgrid1: TStringGrid;
    file_save_button: TToolButton;
    ToolButton2: TToolButton;
    text_settings_button: TToolButton;
    ActionList1: TActionList;
    File1: TMenuItem;
    Save1: TMenuItem;
    Settings1: TMenuItem;
    TextSettings1: TMenuItem;
    Clear1: TMenuItem;
    Clear2: TMenuItem;
    file_save: TAction;
    text_settings: TAction;
    text_clear: TAction;
    ToolButton4: TToolButton;
    text_clear_button: TToolButton;
    Exit1: TMenuItem;
    N1: TMenuItem;
    file_exit: TAction;
    SaveDialog1: TSaveDialog;
    ImageList1: TImageList;
    procedure init_text;

    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure status;
    procedure init_text_run;
    procedure maj_text_run;
    procedure fin_text_run;
    procedure init_text_carlo;
    procedure maj_text_carlo(it : integer; pe : extended);
    procedure text_clearExecute(Sender: TObject);
    procedure file_saveExecute(Sender: TObject);
    procedure text_settingsExecute(Sender: TObject);
    procedure file_exitExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
  public
    nomfictext : string;
    nb_vartext : integer; { nombre de variables affichees }
    nb_vartext_carlo : integer; { nombre de variables affichees (montecarlo) }
    vartext : array[0..maxvartext] of integer; { variables affichees }
    dt_text : integer; { intervalle d'echantillonnage en temps }
    t_text  : integer; { pas en temps pour text }
    valtext_moy,valtext_var,
    valtext_moy_ne,valtext_var_ne : array[1..maxvartext] of tab_text_type;
    ext_tt : array[0..maxtext] of integer; { pour calcul proba ext }
    ift : integer; { numero de la forme }
  end;

var  form_text: tform_text;

implementation

uses jglobvar,jutil,jmath,jsyntax,gtextset,gulm;

{$R *.lfm}

procedure tform_text.status;
begin
  statusbar1.Panels[0].Text := nomfictext;
  statusbar1.Panels[1].Text := 'Dt_text = ' + IntToStr(dt_text);
  with stringgrid1 do
    statusbar1.Panels[2].Text := 'Rows = ' + IntToStr(RowCount-1) + ' ' +
                                 'Cols = ' + IntToStr(ColCount);
end;

procedure tform_text.init_text;
var i,k,x : integer;
begin
  with stringgrid1 do
    begin
      vartext[0] := xtime;
      k := 0;
      for x := 1 to modele_nb do with modele[x] do
        if ( xmat <> 0 ) then
          for i := 1 to size do
            if ( k < maxvartext ) then
              begin
                k := k + 1;
                vartext[k] := vec[xvec].exp[i];
              end
            else
        else
          for i := 1 to size do
            if ( k < maxvartext ) then
              begin
                k := k + 1;
                vartext[k] := rel[xrel[i]].xvar;
              end;
      nb_vartext := k;
      nb_vartext_carlo := imin(4,nb_vartext);
      FixedCols := 1;
    end;
  init_text_run;
  dt_text := 10;
  status;
end;

procedure tform_text.init_text_run;
var k : integer;
begin
  with stringgrid1 do
    begin
      ColCount := 1 + nb_vartext;
      for k := 0 to nb_vartext do
        Cells[k,0] := s_ecri_var(vartext[k]);
      RowCount  := 2;
      FixedRows := 1;
      Cells[0,1] := IntToStr(t__);
      for k := 1 to nb_vartext do with variable[vartext[k]] do
        Cells[k,1] := s_ecri_val(val);
    end;
end;

procedure tform_text.maj_text_run;
var k,r : integer;
begin
  with stringgrid1 do
    begin
      r := RowCount;
      RowCount := RowCount + 1;
      Cells[0,r] := IntToStr(t__);
      for k := 1 to nb_vartext do with variable[vartext[k]] do
        Cells[k,r] := s_ecri_val(val);
    end;
end;

procedure tform_text.fin_text_run;
begin
  status;
end;

procedure tform_text.init_text_carlo;
var k,c : integer;
    s : string;
begin
  with stringgrid1 do
    begin
      nb_vartext_carlo := imin(4,nb_vartext);
      ColCount := 2 + 4*nb_vartext_carlo;
      Cells[1,0] := 'P_ext';
      Cells[1,1] := '0';
      Cells[0,1] := IntToStr(t__);
      c := 1;
      for k := 1 to nb_vartext_carlo do with variable[vartext[k]] do
        begin
          s := s_ecri_var(vartext[k]);
          c := c + 1;
          Cells[c,0] := s;
          Cells[c,1] := s_ecri_val(val);
          c := c + 1;
          Cells[c,0] := 'SE';
          Cells[c,1] := '0';
          c := c + 1;
          Cells[c,0] := s + '*';
          Cells[c,1] := s_ecri_val(val);
          c := c + 1;
          Cells[c,0] := 'SE*';
          Cells[c,1] := '0';
        end;
      RowCount  := 2;
      FixedRows := 1;
    end;
end;

procedure tform_text.maj_text_carlo(it : integer; pe : extended);
var k,c,r : integer;
begin
  with stringgrid1 do
    begin
      r := RowCount;
      RowCount := RowCount + 1;
      Cells[0,r] := IntToStr(it*dt_text);
      Cells[1,r] := s_ecri_val(pe);
      c := 1;
      for k := 1 to nb_vartext_carlo do
        begin
          c := c + 1;
          Cells[c,r] := s_ecri_val(valtext_moy[k][it]);
          c := c + 1;
          Cells[c,r] := s_ecri_val(valtext_var[k][it]);
          c := c + 1;
          Cells[c,r] := s_ecri_val(valtext_moy_ne[k][it]);
          c := c + 1;
          Cells[c,r] := s_ecri_val(valtext_var_ne[k][it]);
        end;
    end;
end;

procedure tform_text.FormCreate(Sender: TObject);
begin
  Left   := 332;
  Top    := 80;
  Height := 330;
  Width  := 676;
  adjust(self);
  nomfictext := '';
end;

procedure tform_text.FormActivate(Sender: TObject);
begin
  status;
end;

procedure tform_text.text_clearExecute(Sender: TObject);
begin
  init_text_run;
  status;
end;

procedure tform_text.file_saveExecute(Sender: TObject);
var r,c : integer;
    s : string;
    ls : TStrings;
begin
  if ( nomfictext = '' ) then
    nomfictext := s_ecri_model(1) + '.txt';
  with savedialog1 do
    begin
      FileName := nomfictext;
      savedialog1.Filter := 'Result text file (*.txt)|All files(*)';
      InitialDir := ExtractFilePath(nomfictext);
      if Execute then with stringgrid1 do
        begin
          if LazFileUtils.fileexistsUTF8(FileName) { *Converted from FileExists* } then
            if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
               mtConfirmation,[mbYes,mbNo],0) <> mrYes then exit;
          nomfictext := FileName;
          ls := TStringList.Create;
          for r := 1 to RowCount-1 do
            begin
              s := '';
              for c := 0 to ColCount-1 do
                s := s + Format('%10s',[Cells[c,r]]) + ' ';
                {if ( c < Colcount-1) then
                  s := s + Format('%10s',[Cells[c,r]]) + hortab
                else
                  s := s + Format('%10s',[Cells[c,r]]);}
              ls.Append(s);
            end;
          ls.SaveToFile(nomfictext);
          ls.Free;
          status;
        end;
    end;
end;

procedure tform_text.text_settingsExecute(Sender: TObject);
begin
  with form_textset do
    begin
      ft := Self;
      Show;
    end;
end;

procedure tform_text.file_exitExecute(Sender: TObject);
begin
  nb_form_text := nb_form_text - 1;
  Visible := false;
end;

procedure tform_text.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  file_exitExecute(nil);
end;

end.
