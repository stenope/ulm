unit jutil;

{$MODE Delphi}

{  @@@@@@  procedures utilitaires   @@@@@@  }

interface

uses SysUtils,Classes,Forms,Grids,StdCtrls;

procedure iwriteln(s : string);
procedure bwriteln(slist : TStrings);
procedure erreur_(s : string);
procedure init_form(f : TForm);
function  clock : integer;
function  s_ecri_t_exec(ms : integer) : string;
procedure adjust(f : TForm);
procedure resolution;

implementation

uses Dialogs,jglobvar,gulm;

{const height_dev = 1024;
      width_dev  = 1280;
      pixels_per_inch_dev0 = 96;}

const height_dev = 768;
      width_dev  = 1024;
      pixels_per_inch_dev0 = 96;

var   screen_ratio_h : extended;     { rapport hauteur ecran utilisateur/hauteur ecran developpement }
      screen_ratio_w : extended;     { rapport largeur ecran utilisateur/largeur ecran developpement }
      pixels_per_inch_dev : integer; { valeur pixelperinch au developpement }

procedure iwriteln(s : string);
begin
  if not notext then form_ulm.memo_interp.Lines.Add(s);
  if outputfile then writeln(ficout,s);
end;

procedure bwriteln(slist : TStrings);
var i : integer;
begin
  with form_ulm do with memo_interp.Lines do
    begin
      if ( Count > 10000 ) then Clear; {iiiii}
      if not notext then AddStrings(slist);
      if outputfile then
        for i := 0 to slist.Count - 1 do writeln(ficout,slist[i]);
    end;
end;

procedure erreur_(s : string);
begin
  MessageDlg(s,mtError,[mbOk],0);
end;

procedure init_stringgrid(w : TStringGrid);
var i,j : integer;
begin
  with w do
    begin
      for i := 1 to RowCount do
        for j := 1 to ColCount do Cells[j,i] := '';
    end;
end;

procedure init_edit(w : TEdit);
begin
  with w do Text := '';
end;

procedure init_memo(w : TMemo);
begin
  with w do Clear;
end;

procedure init_form(f : TForm);
var i : integer;
begin
  with f do
    for i := 0 to ComponentCount-1 do
      begin
        if ( Components[i] is TStringGrid ) then init_stringgrid(Components[i] as TStringGrid);
        if ( Components[i] is TEdit )       then init_edit(Components[i]       as TEdit);
        if ( Components[i] is TMemo )       then init_memo(Components[i]       as TMemo);
      end;
end;

function  clock : integer;
{ nombre de millisecondes }
begin
  clock := 0;
end; 

function  s_ecri_t_exec(ms : integer) : string;
var h,mn,sec : integer;
begin
  sec := ms div 1000;
  if ( sec = 0 ) then
    begin
      s_ecri_t_exec := IntToStr(ms) + ' ms';
      exit;
    end;
  if ( sec < 60 ) then
    begin
      s_ecri_t_exec := IntToStr(sec) + ' s';
      exit;
    end;
  mn := sec div 60;
  sec  := sec mod 60;
  if ( mn < 60 ) then
    begin
      s_ecri_t_exec := IntToStr(mn) + ' mn ' + IntToStr(sec) + ' s';
      exit;
    end;
  h  := mn div 60;
  mn := mn mod 60;
  s_ecri_t_exec := IntToStr(h)   + ' h '  + IntToStr(mn)  + ' mn';
end;

procedure resolution;
begin
  screen_ratio_h := Screen.Height/height_dev;
  screen_ratio_w := Screen.Width/width_dev;
  pixels_per_inch_dev := pixels_per_inch_dev0;
end;

procedure adjust(f : TForm);
begin
  if ( Screen.Width < width_dev ) then exit;
  with f do
    begin
      {Scaled := true;}
      Left   := round(screen_ratio_w*Left);
      Top    := round(screen_ratio_h*Top);
      Width  := round(screen_ratio_w*Width);
      Height := round(screen_ratio_h*Height);
      {PixelsPerInch := round(screen_ratio_w*pixels_per_inch_dev);
      ScaleBy(Screen.Width,width_dev);
      case Screen.Width of
        640  : Font.Size := 6;
        800  : Font.Size := 7;
        1024 : Font.Size := 8;
        1152 : Font.Size := 8;
        1280 : Font.size := 8;
        1365 : Font.size := 8;
        1600 : Font.Size := 10;
        else   Font.Size := 8;
      end; }
      {iwriteln('Form ' + Name);
      iwriteln('  Left   = ' + IntToStr(Left));
      iwriteln('  Top    = ' + IntToStr(Top));
      iwriteln('  Width  = ' + IntToStr(Width));
      iwriteln('  Height = ' + IntToStr(Height));
      iwriteln('  PixelsPerInch = ' + IntToStr(PixelsPerInch)); }
    end;
end;

end.