unit gtextset;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  Grids, ExtCtrls, ComCtrls, StdCtrls,
  gtext;

type
  tform_textset = class(TForm)
    StatusBar1: TStatusBar;
    sg_vartext: TStringGrid;
    ok_button: TButton;
    apply_button: TButton;
    cancel_button: TButton;
    dt_text_edit: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure status;
    procedure ok_buttonClick(Sender: TObject);
    procedure apply_buttonClick(Sender: TObject);
    procedure cancel_buttonClick(Sender: TObject);
    procedure sg_vartextSelectCell(Sender: TObject; ACol, ARow: Integer;
      var CanSelect: Boolean);
  private
    err_textset : boolean;
    procedure erreur(s : string);
  public
    ft : tform_text;
  end;

var  form_textset: tform_textset;

implementation

uses jglobvar,jutil,jsymb,jsyntax;

{$R *.lfm}

procedure tform_textset.erreur(s : string);
begin
  erreur_('Text Settings - ' + s);
  err_textset := true;
end;

procedure tform_textset.status;
begin
  statusbar1.Panels[0].Text :=
    'Nb_var Text = ' + IntToStr(ft.nb_vartext);
end;

procedure tform_textset.FormCreate(Sender: TObject);
begin
  Left   := 332;
  Top    := 444;
  Height := 146;
  Width  := 638;
  adjust(self);
end;

procedure tform_textset.FormActivate(Sender: TObject);
var k : integer;
begin
  with sg_vartext,ft do
    begin
      form_textset.Caption := 'TEXT SETTINGS <' + IntToStr(ift) + '>';
      ColCount  := 10;
      FixedCols := 1;
      RowCount  := 2;
      FixedRows := 1;
      for k := 0 to ColCount-1 do Cells[k,0] := IntToStr(k);
      for k := 0 to nb_vartext do
        Cells[k,1] := s_ecri_var(vartext[k]);
      for k := nb_vartext + 1 to ColCount-1 do
        Cells[k,1] := '';
      dt_text_edit.Text := IntToStr(dt_text);
    end;
  status;
end;

procedure tform_textset.ok_buttonClick(Sender: TObject);
begin
  apply_buttonClick(nil);
  if not err_textset then Close;
end;

procedure tform_textset.apply_buttonClick(Sender: TObject);
var n,k,x,tx,dt_text1 : integer;
    vartext1 : array[0..maxvartext] of integer;
    s : string;
begin
  err_textset := false;
  with ft,sg_vartext do
    begin
      if est_entier(dt_text_edit.Text,dt_text1) then
        if ( dt_text1 <= 0 ) then
          begin
            erreur('Sampling interval: positive integer expected');
            exit;
          end
        else
      else
        begin
          erreur('Sampling interval: positive integer expected');
          exit;
        end;
      n := 0;
      for k := 1 to maxvartext do
        begin
          s := Cells[k,1];
          if ( s <> '' )  then
            begin
              trouve_obj(s,x,tx);
              if ( tx = type_variable ) then
                begin
                  n := n + 1;
                  vartext1[n] := x;
                end
              else
                begin
                  erreur('unknown variable name');
                  exit;
                end;
            end;
        end;
      nb_vartext := n;
      for k := 1 to n do vartext[k] := vartext1[k];
      for k := n+1 to maxvartext do cells[k,1] := '';
      dt_text := dt_text1;
      init_text_run;
      FormActivate(nil);
    end;
  FormActivate(nil);
end;

procedure tform_textset.cancel_buttonClick(Sender: TObject);
begin
  Close;
end;

procedure tform_textset.sg_vartextSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( arow = 1 ) and ( acol > 0 );
end;

end.
