# ULM: Unified Life Models - Population Dynamics Modelling

The ULM computer program (Legendre & Clobert 1995, Ferrière et al. 1996) has been designed to study a large panel of population dynamics models, for research or teaching purposes:

- Ecology
- Management, conservation biology
- Deterministic and stochastic discrete time dynamical systems

[Project homepage](https://www.biologie.ens.fr/~legendre/ulm/ulm.html)

## File description

- `ulm.dpr`		Delphi project file	
		
The `ulm.dpr` file list the modules `*.pas` that contain Pascal objects
The names of these modules begins with `g`, e.g., `gulm.pas` 

Names of modules without Pascal objects begin wih `j`, e.g., `jmath.pas`

The files `*.xfm` are the Delphi description of each window ('`form`')
For example, the file gulm.xfm is associated with the module gulm.pas

- `gulm.pas`	window for main program

- `ggraph.pas`	window for graphics and graphic procedures
- `ggraphset.pas`	window for setting graphics
- `gabout.pas`	window about, about the ULM program
- `grunset.pas`	window for setting the run command
- `gviewvar.pas`	window for displaying variables of the ULM model
- `gcalc.pas`	window for online desktop calculator
- `gedit.pas`	window where the model file is displayed and can be edited
- `gviewall.pas`	window for displaying all ULM objects
- `gtext.pas`	window for displaying results	
- `gtextset.pas`	window for setting the text window
- `gsensib.pas`	window for computing sensitivities
- `gconfint.pas`	window for confidence interval
- `gconfint2.pas`	window for confidence interval 2
- `gprop.pas`	window for matrix properties
- `gmulti.pas`	window for properties of multisite matrix
- `gage.pas`	window for age computation in size-classified matrix 
- `gspec.pas`	window for computing power spectrum
- `gcorrel.pas`	window for computing 
- `glandscape.pas`	window for computing fitness landscape
- `glyap.pas`	window for computing lyapunov exponent
- `gstocsensib.pas`	window for computing stochastic sensitivities

---

- `jglobvar.pas`	declaration of global variables of the ULM program

- `jsymb.pas`	gestion of ULM objects
- `jsyntax.pas`	gestion of input and output data
- `jmath.pas`	mathematical library
- `jcompil.pas`	'compilation' of the ULM model input file
- `jeval.pas`	ULM evaluator
- `jmatrix`		computation of matrix descriptors
- `jinterp`		ULM command interpreter
- `jutil`		some utilitary procedures
- `jrun`		procedure for running the ULM model
- `jcarlo`		Monte Carlo procedure

---

- `ulm.ico`		icon of the ULM program

## Contributors 

ULM is developped by Stéphane Legendre with contribution from Jean
Clobert, Régis Ferrière, Frédéric Gosselin, Jean-Dominique Lebreton and
François Sarrazin.

## Contact
- Email: legendre (at) ens (dot) fr
- Homepage: https://www.biologie.ens.fr/~legendre/index.html

Address:

    Stéphane Legendre
    Laboratoire d'Ecologie
    Ecole Normale Supérieure
    46 rue d'Ulm
    75230 Paris Cedex 05
    France

## License

Copyright (c) 1995-2016, Stéphane Legendre 

ULM is free software: you can redistribute it and/or modify it under
the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your
option) any later version.

ULM is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
for more details.

You should have received a copy of the GNU General Public License
along with ULM.  If not, see <http://www.gnu.org/licenses/>.
