unit glandscape;

{$MODE Delphi}

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     ExtCtrls,
     jmath;

type
  tform_landscape = class(TForm)
    button_exec: TButton;
    label_lambdamin: TLabel;
    edit_lambdamin: TEdit;
    label_lambdamax: TLabel;
    edit_lambdamax: TEdit;
    label_mat: TLabel;
    edit_mat: TEdit;
    label_lambda: TLabel;
    edit_lambda: TEdit;
    Panel1: TPanel;
    label_x: TLabel;
    edit_x: TEdit;
    label_xmin: TLabel;
    edit_xmin: TEdit;
    label_xmax: TLabel;
    edit_xmax: TEdit;
    label_y: TLabel;
    edit_y: TEdit;
    label_ymin: TLabel;
    edit_ymin: TEdit;
    label_ymax: TLabel;
    edit_ymax: TEdit;
    procedure erreur(s : string);
    procedure init_landscape;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure button_execClick(Sender: TObject);
    procedure exec_landscape(m,x,y : integer;xmi,xma,ymi,yma : extended);
    procedure eigenval(var err : boolean);
    function  check_param : boolean;
  private
     x_mat : integer; { matrice }
     x_var : integer; { variable X }
     y_var : integer; { variable Y }
     lambdamin,lambdamax : extended; { bornes lambda }
     lambda : cvec_type;
     lambda1 : extended; { valeur propre dominante }
     x0,y0       : extended; { valeur actuelle }
     xminl,xmaxl : extended; { bornes X }
     yminl,ymaxl : extended; { bornes Y }
  public
  end;

var form_landscape: tform_landscape;

implementation

uses jglobvar,jsymb,jutil,jsyntax,jeval,jmatrix,
     ggraph, gulm;

{$R *.lfm}

procedure tform_landscape.erreur(s : string);
begin
  erreur_('Landscape - ' + s);
end;

procedure tform_landscape.FormCreate(Sender: TObject);
begin
  Left   := 0;
  Top    := 260;
  Height := 206;
  Width  := 526;
  adjust(self);
  Caption := 'FITNESS LANDSCAPE';
  xminl := 0.0;
  xmaxl := 1.0;
  yminl := 0.0;
  ymaxl := 1.0;
  x_mat := 0;
  x_var := 0;
  y_var := 0;
end;

procedure tform_landscape.init_landscape;
var i : integer;
begin
  init_form(form_landscape);
  x_mat := 0;
  x_var := 0;
  y_var := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        break;
      end;
  if ( x_mat <> 0 ) then edit_mat.Text := s_ecri_mat(x_mat)
end;

procedure tform_landscape.eigenval(var err : boolean);
begin
  err := true;
  with mat[x_mat] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_landscape.FormActivate(Sender: TObject);
var err : boolean;
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      edit_lambda.Text := '';
      exit;
    end;
  edit_mat.Text := s_ecri_mat(x_mat);
  eigenval(err);
  if err then exit;
  edit_lambda.Text := Format('%10.6f',[lambda1]);
  if mattimedep(x_mat) then
    begin
      erreur('matrix is time-dependent');
      exit;
    end;
  if not matnonneg(mat[x_mat].size,mat[x_mat].val) then
    begin
      erreur('matrix is negative');
      exit;
    end;
  if matvecdep(x_mat,modele[mat[x_mat].xmodele].xvec) then
    begin
      erreur('matrix is vector-dependent');
      exit;
    end;
  if matrandom(x_mat) then
    begin
      erreur('matrix is random');
      exit;
    end;
  if ( x_var = 0 ) then
    begin
      edit_x.Text := '';
      edit_xmin.Text := '';
      edit_xmax.Text := '';
    end
  else
    begin
      edit_x.Text := s_ecri_var(x_var);
      edit_xmin.Text := Format('%10.2f',[xminl]);
      edit_xmax.Text := Format('%10.2f',[xmaxl]);
    end;
  if ( y_var = 0 ) then
    begin
      edit_y.Text := '';
      edit_ymin.Text := '';
      edit_ymax.Text := '';
    end
  else
    begin
      edit_y.Text := s_ecri_var(y_var);
      edit_ymin.Text := Format('%10.2f',[yminl]);
      edit_ymax.Text := Format('%10.2f',[ymaxl]);
    end;
end;

procedure tform_landscape.exec_landscape(m,x,y : integer;xmi,xma,ymi,yma : extended);
{ m indice matrice non negative   }
{ x,y variables  }
label 1,2;
const maxtab = 100;
var i,j,k,iter,maxniv,niv,h,i0,ms : integer;
    x_exp_sav,x_exp_type_sav,y_exp_sav,y_exp_type_sav,x_ree,y_ree : integer;
    lambda : cvec_type;
    dx,dy,xx,yy,eps,ex,ey,y1,y2,dlamb,lamb1,lamb,lamb0 : extended;
    nbk : ismalltab_type;
    val_lab : rsmalltab_type;
begin
  form_graph.status_landscape(x_var,y_var);
  form_ulm.status_run('Fitness landscape');
  ms := clock;
  with mat[m],form_graph do
    begin
      if not gplus then efface(nil);
      matvalprop(size,val,lambda);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then exit;
      lamb  := lambda[1].re;
      lamb0 := lamb;
      x_ree := ree_reg1;
      with variable[x] do
        begin
          x_exp_sav := exp;
          x_exp_type_sav := exp_type;
          exp_type := type_ree;
          exp := x_ree;
        end;
      y_ree := ree_reg2;
      with variable[y] do
        begin
          y_exp_sav := exp;
          y_exp_type_sav := exp_type;
          exp_type := type_ree;
          exp := y_ree;
        end;
      ex := xma - xmi;
      ey := yma - ymi;
      dx := ex/maxtab;
      dy := ey/maxtab;
      ree[x_ree].val := xmi;
      ree[y_ree].val := ymi;
      init_eval; {init_eval1 marche pas}
      eval_mat(m);
      matvalprop(size,val,lambda);
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamin := lambda[1].re;
      ree[x_ree].val := xma;
      ree[y_ree].val := ymi;
      init_eval;
      eval_mat(m);
      matvalprop(size,val,lambda);
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamin := min(lambdamin,lambda[1].re);
      ree[x_ree].val := xma;
      ree[y_ree].val := yma;
      init_eval;
      eval_mat(m);
      matvalprop(size,val,lambda);
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamax := lambda[1].re;
      ree[x_ree].val := xmi;
      ree[y_ree].val := yma;
      init_eval;
      eval_mat(m);
      matvalprop(size,val,lambda);
      if err_math then
        begin
          err_math := false;
          goto 2;
        end;
      if ( lambda[1].im <> 0.0 ) then goto 2;
      lambdamax := max(lambdamax,lambda[1].re);
      calcul_echelle(lambdamin,lambdamax,dlamb,lamb1);
      maxniv := trunc((lambdamax - lambdamin)/dlamb);
      eps := 0.00005;
      j := 0;
      if ( lamb0 > lambdamin ) and ( lamb0 < lambdamax ) then
        i0 := 0
      else
        i0 := 1;
      for niv := i0 to maxniv do
        begin
          if ( niv = 0 ) then
            lamb := lamb0
          else
            lamb := lamb1 + niv*dlamb;
          k := 0;
          for h := 0 to maxtab do
            begin
              xx := xmi + h*dx;
              ree[x_ree].val := xx;
              y1 := ymi;
              y2 := yma;
              ree[y_ree].val := y1;
              init_eval;
              eval_mat(m);
              matvalprop(size,val,lambda);
              if err_math then
                begin
                  err_math := false;
                  goto 1;
                end;
              if ( lambda[1].im <> 0.0 ) or ( lambda[1].re >= lamb ) then goto 1;
              ree[y_ree].val := y2;
              init_eval;
              eval_mat(m);
              matvalprop(size,val,lambda);
              if err_math then
                begin
                  err_math := false;
                  goto 1;
                end;
              if ( lambda[1].im <> 0.0 ) or ( lambda[1].re <= lamb ) then goto 1;
              iter := 0;
              repeat
                yy := (y1 + y2)/2.0;
                ree[y_ree].val := yy;
                init_eval;
                eval_mat(m);
                matvalprop(size,val,lambda);
                if err_math then
                  begin
                    err_math := false;
                    goto 1;
                  end;
                if ( lambda[1].im <> 0.0 ) then goto 1;
                if ( lambda[1].re <= lamb ) then
                  y1 := yy
                else
                  y2 := yy;
                iter := iter + 1;
                if ( iter > 30 ) then goto 1;
              until ( abs(lambda[1].re - lamb) <= eps );
              valgraph_x[j] := xx;
              valgraph_y[1][j] := yy;
              j := j + 1;
              k := k + 1;
1:
              with form_ulm do
                begin
                  procproc;
                  if runstop then
                    begin
                      runstop := false;
                      goto 2;
                    end;
                end;
            end;{h}
          nbk[niv] := k;
          val_lab[niv] := lamb;
        end;{niv}
      gland(i0,maxniv,nbk,val_lab,xmi,xma,ymi,yma,x0,y0);
    end;
2 :
  with variable[x] do
    begin
      exp := x_exp_sav;
      exp_type := x_exp_type_sav;
    end;
  with variable[y] do
    begin
      exp := y_exp_sav;
      exp_type := y_exp_type_sav;
    end;
  form_ulm.run_initExecute(nil);
  form_ulm.status_t_exec(clock - ms);
end;

procedure tform_landscape.button_execClick(Sender: TObject);
begin
  if not check_param then exit;
  if ( x_mat = 0 ) or ( x_var = 0 ) or ( y_var = 0 ) then exit;
  edit_lambdamin.Text := '';
  edit_lambdamax.Text := '';
  exec_landscape(x_mat,x_var,y_var,xminl,xmaxl,yminl,ymaxl);
  edit_lambdamin.Text := Format('%10.4g',[lambdamin]);
  edit_lambdamax.Text := Format('%10.4g',[lambdamax]);
end;

function  tform_landscape.check_param : boolean;
var x,tx : integer;
    a,b  : extended;
begin
  check_param := false;
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  if not test_variable(edit_x.Text,x) then
    begin
      erreur('Variable X: unknown variable name');
      if ( x_var <> 0 ) then
        edit_x.Text := s_ecri_var(x_var)
      else
        edit_x.Text := '';
      exit;
    end;
  x_var := x;
  x0 := variable[x_var].val;
  if not test_variable(edit_y.Text,x) then
    begin
      erreur('Variable Y: unknown variable name');
      if ( y_var <> 0 ) then
        edit_y.Text := s_ecri_var(y_var)
      else
        edit_y.Text := '';
      exit;
    end;
  y_var := x;
  y0 := variable[y_var].val;
  if ( edit_xmin.Text = '' ) and ( edit_xmax.Text = '' ) then
    begin
      xminl := 0.5*x0;
      edit_xmin.Text := Format('%10.2f',[xminl]);
      xmaxl := 1.5*x0;
      if ( xmaxl = xminl ) then xmaxl := 1.0;
      edit_xmax.Text := Format('%10.2f',[xmaxl]);
    end;
  if ( edit_ymin.Text = '' ) and ( edit_ymax.Text = '' ) then
    begin
      yminl := 0.5*y0;
      edit_ymin.Text := Format('%10.2f',[yminl]);
      ymaxl := 1.5*y0;
      if ( ymaxl = yminl ) then ymaxl := 1.0;
      edit_ymax.Text := Format('%10.2f',[ymaxl]);
    end;
  xminl := StrToFloat(edit_xmin.Text);
  xmaxl := StrToFloat(edit_xmax.Text);
  yminl := StrToFloat(edit_ymin.Text);
  ymaxl := StrToFloat(edit_ymax.Text);
  FormActivate(nil);
  check_param := true;
end;

end.
