unit glyap;

{$MODE Delphi}

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,Grids,
     jglobvar;

type
  tform_lyap = class(TForm)
    label_model: TLabel;
    edit_modele: TEdit;
    label_tlag: TLabel;
    edit_tlag: TEdit;
    button_run: TButton;
    button_init: TButton;
    stringgrid1: TStringGrid;
    label_nb_cyc: TLabel;
    edit_nb_cyc: TEdit;
    procedure erreur(s : string);
    procedure init_lyap;
    {procedure construc_jac(m : integer); }
    procedure maj_lyap(u : integer; val : extended);
    procedure run_lyap(m,nb_cyc,tlag : integer);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    function  check_param : boolean;
    procedure button_runClick(Sender: TObject);
    procedure button_initClick(Sender: TObject);
  private
    nb_cyc   : integer; { nb de pas de temps }
    tlag     : integer;
    x_modele : integer;
    {jac_exp,jac_exp_type : imat_type;} { jacobien }
    {jac_constructed : boolean;}
  public
    interp_ : boolean;
  end;

var form_lyap: tform_lyap;

implementation

uses jutil,jsymb,jmath,jsyntax,jeval,
     gulm;

{$R *.lfm}

procedure tform_lyap.erreur(s : string);
begin
  erreur_('Lyapunov - ' + s);
end;

procedure tform_lyap.init_lyap;
begin
  with stringgrid1 do RowCount := 1;
  edit_modele.Text := s_ecri_model(x_modele);
  edit_nb_cyc.Text := IntToStr(nb_cyc);
  edit_tlag.Text   := IntToStr(tlag);
  {jac_constructed := false; }
  interp_ := false;
end;

procedure tform_lyap.FormCreate(Sender: TObject);
begin
  Left   := 490;
  Top    := 0;
  Height := 260;
  Width  := 390;
  adjust(self);
  Caption := 'LYAPUNOV EXPONENT';
  x_modele := 1;
  nb_cyc := 1000;
  tlag   := 100;
  with stringgrid1 do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := 2;
      RowCount  := 1;
      Cells[0,0] := 't';
      Cells[1,0] := 'Lyapunov'
    end;
end;

procedure tform_lyap.maj_lyap(u : integer; val : extended);
var r : integer;
begin
  with stringgrid1 do
    begin
      r := RowCount;
      RowCount := RowCount + 1;
      Cells[0,r] := IntToStr(u);
      Cells[1,r] := s_ecri_val(val);
    end;
end;

procedure tform_lyap.FormActivate(Sender: TObject);
begin
  edit_modele.Text := s_ecri_model(x_modele);
  edit_nb_cyc.Text := IntToStr(nb_cyc);
  edit_tlag.Text   := IntToStr(tlag);
end;

{procedure tform_lyap.construc_jac(m : integer);
var i,j,k,n,a,u,v,w,xx,tjac : integer;
    relat_exp,relat_exp_type,vector_exp : ivec_type;
begin
  init_eval1;
  jac_constructed := false;
  with modele[m] do
    begin
      n := size;
      a := xmat;
      v := xvec;
    end;
  if ( a <> 0 ) then
    begin
      for i := 1 to n do
        begin
          u := cons(mat[a].exp[i,1],mat[a].exp_type[i,1],list(vec[v].exp[1],vec[v].exp_type[1]));
          w := cons(op2_mult,type_op2,u);
          for k := 2 to n do
            begin
              push(w);
              u := cons(mat[a].exp[i,k],mat[a].exp_type[i,k],list(vec[v].exp[k],vec[v].exp_type[k]));
              u := cons(op2_mult,type_op2,u);
              push(u);
              w := cons(op2_plus,type_op2,cons(u,type_lis,list(w,type_lis)));
              xx := pop;
              xx := pop;
            end;
          relat_exp[i] := w;
          relat_exp_type[i] := type_lis;
          push(w);
          vector_exp[i] := vec[v].exp[i];
        end;
    end
  else
      for i := 1 to n do
        begin
          u := modele[m].xrel[i];
          w := rel[u].exp;
          relat_exp[i] := w;
          relat_exp_type[i] := rel[u].exp_type;
          push(w);
          vector_exp[i] := rel[u].xvar;
        end;
  for i := 1 to n do
    for j := 1 to n do
      begin
        deriv(relat_exp[i],relat_exp_type[i],vector_exp[j],jac_exp[i,j],tjac);
        if err_eval then
          begin
            err_eval := false;
            exit;
          end;
        jac_exp_type[i,j] := tjac;
        push(jac_exp[i,j]);
      end;
  for i := 1 to n do for j := 1 to n do xx := pop;
  for i := 1 to n do xx := pop;
  jac_constructed := true;
end;

{procedure tform_lyap.run_lyap(m,nb_cyc,tlag : integer);
label 1;
var i,j,k,niter,ms : integer;
    s,som,lyapunov : extended;
    vv : rvec_type;
    aaa : rmat_type;
begin
  if not jac_constructed then construc_jac(m);
  if not jac_constructed then
    begin
      erreur_('Lyapunov exponent cannot be computed');
      exit;
    end;
  form_ulm.status_run('Lyapunov ' + IntToStr(nb_cyc));
  ms := clock;
  with modele[m] do
    begin
      niter := 0;
      for i := 1 to size do vv[i] := 1.0;
      repeat
        som := 0.0;
        for k := 1 to tlag do
          begin
            niter := niter + 1;
            for i := 1 to size do
              for j := 1 to size do
                aaa[i,j] := eval(jac_exp[i,j],jac_exp_type[i,j]);
            matvec(size,aaa,vv,vv);
            run_t;
            s := 0.0;
            for i := 1 to size do
              begin
                s := s + abs(vv[i]);
                if ( s >= maxextended ) or ( s <= minextended ) then
                  begin
                    erreur('Error in Lyapunox exponent computation');
                    goto 1;
                  end;
              end;
            som := som + ln0(s)/niter;
            with form_ulm do
              begin
                procproc;
                if runstop then
                  begin
                    runstop := false;
                    goto 1;
                  end;
                if ( t__ mod 100 = 0 ) then status_time;
              end;
          end;
        lyapunov := som/tlag;
        if interp_ then
          iwriteln('   ' + IntToStr(t__) + Format('%10.4g',[lyapunov]))
        else
          maj_lyap(t__,lyapunov);
      until ( niter = nb_cyc );
    end;
1 :
  with form_ulm do
    begin
      status_t_exec(clock - ms);
      status_time;
    end;
end;}

procedure tform_lyap.run_lyap(m,nb_cyc,tlag : integer);
const eps = 0.00001;
label 1;
var i,j,icyc,ms : integer;
    lyap,lyapunov,d0,dd : extended;
    v,v_eps,dv : rvec_type;
    aaa : rmat_type;

procedure maj_var;
var e : integer;
begin
  for e := 1 to ordre_eval_nb do with variable[ordre_eval[e]] do
    val := eval(exp,exp_type);
end;

begin
  form_ulm.status_run('Lyapunov ' + IntToStr(nb_cyc));
  ms := clock;
  with modele[m] do
    begin
      lyap := 0.0;
      if ( xmat <> 0 ) then with vec[xvec] do
        for i := 1 to size do v_eps[i] := val[i] + eps/size
      else
        for i := 1 to size do with rel[xrel[i]] do v_eps[i] := val + eps/size;
      d0 := eps;
      for icyc := 1 to nb_cyc do
        begin
          run_t;
          if ( xmat <> 0 ) then
            begin
              for i := 1 to size do with vec[xvec] do
                begin
                  v[i] := variable[exp[i]].val;
                  variable[exp[i]].val := v_eps[i];
                end;
              maj_var;
             { pour reevaluer les variables qui dependent
               du vecteur de population on reevalue tout }
              for i := 1 to size do
                for j := 1 to size do with mat[xmat] do
                  aaa[i,j] := eval(exp[i,j],exp_type[i,j]);
              matvec(size,aaa,v_eps,v_eps);
            end
          else
            begin
              for i := 1 to size do with rel[xrel[i]] do
                begin
                  v[i] := variable[xvar].val;
                  variable[xvar].val := v_eps[i];
                end;
              maj_var;
              for i := 1 to size do with rel[xrel[i]] do
                v_eps[i] := eval(exp,exp_type);
            end;
          for i := 1 to size do dv[i] := v[i] - v_eps[i];
          dd := vecnorm(size,dv);
          if ( dd = 0.0 ) then dd := d0;
          for i := 1 to size do v_eps[i] := v[i] + d0*dv[i]/dd;
          lyap := lyap + ln0(dd/d0);
          lyapunov := lyap/icyc;
          if ( xmat <> 0 ) then
            for i := 1 to size do with vec[xvec] do
              variable[exp[i]].val := v[i]
          else
            for i := 1 to size do with rel[xrel[i]] do
              variable[xvar].val := v[i];
          maj_var;
          if ( t__ mod tlag = 0 ) then
            if interp_ then
              iwriteln('   ' + IntToStr(t__) + Format('%10.4f',[lyapunov]))
            else
              maj_lyap(t__,lyapunov);
          with form_ulm do
            begin
              procproc;
              if runstop then
                begin
                  runstop := false;
                  goto 1;
                end;
              if ( t__ mod 100 = 0 ) then status_time;
            end;
        end;
    end;
1 :
  with form_ulm do
    begin
      status_t_exec(clock - ms);
      status_time;
    end;
end;

function  tform_lyap.check_param : boolean;
var x,tx,n :integer;
begin
  check_param := false;
  trouve_obj(edit_modele.Text,x,tx);
  if ( x = 0 ) or ( tx <> type_modele ) then
    begin
      erreur('Unknown model name');
      edit_modele.Text := s_ecri_model(x_modele);
      exit;
    end;
  {if ( x <> x_modele ) then construc_jac(x); }
  x_modele := x;
  if not est_entier(edit_nb_cyc.Text,n) then
    begin
      erreur('Number of time steps: integer expected');
      edit_nb_cyc.Text := IntToStr(nb_cyc);
      exit;
    end;
  if ( n <= 0 ) then
    begin
      erreur('Number of time steps: positive integer expected');
      edit_nb_cyc.Text := IntToStr(nb_cyc);
      exit;
    end;
  nb_cyc := n;
  if not est_entier(edit_tlag.Text,n) then
    begin
      erreur('Time lag: integer expected');
      edit_tlag.Text := IntToStr(tlag);
      exit;
    end;
  if ( n <= 0 ) then
    begin
      erreur('Time lag: positive integer expected');
      edit_tlag.Text := IntToStr(tlag);
      exit;
    end;
  {if ( n > nb_cyc ) or ( nb_cyc mod tlag <> 0 ) then
    begin
      erreur('Time lag: must divide number of time steps');
      edit_tlag.Text := IntToStr(tlag);
      exit;
    end;}
  tlag := n;
  FormActivate(nil);
  check_param := true;
end;

procedure tform_lyap.button_runClick(Sender: TObject);
begin
  if not check_param then exit;
  if ( t__ = 0 ) then stringgrid1.RowCount := 1;
  run_lyap(x_modele,nb_cyc,tlag);
end;

procedure tform_lyap.button_initClick(Sender: TObject);
begin
  with form_ulm do run_initExecute(nil);
  with stringgrid1 do RowCount := 1;
end;

end.
