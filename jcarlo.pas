unit jcarlo;

{$MODE Delphi}

{  @@@@@@   Monte Carlo   @@@@@@  }

interface

procedure carlo(nb_cycle_carlo,nb_run_carlo: integer;
                seuil_ext,seuil_div : extended);

implementation

uses  SysUtils,
      jglobvar,
      jutil,
      jmath,
      jsyntax,
      jeval,
      jrun,
      jinterp,
      gulm,ggraph,gtext;

const maxtexte_interp = 100;

type  tab_texte_interp_type = array[0..maxtexte_interp] of extended;

var   ext,dvg,ext_time,div_time : array[1..modele_nb_max] of integer;
      ext_time_moy,ext_time_var,div_time_moy,div_time_var,
      nsom,nsom2,nmoy,nmoy2,nmoy_ne,nvar_ne,nmax_ne,nmin_ne,
      lamb_moy,lamb_var,
      lamb2_moy,lamb2_var,lnlamb_moy,lnlamb_var : array[1..modele_nb_max] of extended;
      fini,diverg : array[1..modele_nb_max] of boolean;
      wmoy : array[1..modele_nb_max] of rvec_type;
      ext_t : array[1..modele_nb_max] of array[0..maxtexte_interp] of integer;
      nt_moy,nt_var,
      nt_moy_ne,nt_var_ne : array[1..modele_nb_max] of tab_texte_interp_type;
      varmoy,varvar,varmoy_ne,varvar_ne : array[1..maxvartexte_interp] of extended;

procedure carlo(nb_cycle_carlo,nb_run_carlo: integer;
                    seuil_ext,seuil_div : extended);
label 1;
var irun,icyc,t_texte_interp,delta_texte_interp : integer;

{ ------  graphique  ------ }

procedure carlo_init_graph_distrib(fg : tform_graph);
var i,k : integer;
begin
  with fg do
    begin
      for i := 0 to maxgraph do
        begin
          valgraph_x[i] := i*d_distrib;
          for k := 1 to nb_vargraph_y do valgraph_y[k][i] := 0.0;
        end;
    end;
end;

procedure carlo_init_graph_traj(fg : tform_graph);
var i,k,j : integer;
    a : extended;
begin
  with fg do
  begin
  dt_graph := (nb_cycle_carlo + maxgraph - 1) div maxgraph;
  for i := 1 to imin(nb_cycle_carlo,maxgraph) do
    begin
      valgraph_x[i] := 0.0;
        if gsigma then valgraph_x_sigma[i] := 0.0;
        if gminmax then
          begin
            valgraph_x_min[i] := maxextended;
            valgraph_x_max[i] := -maxextended;
          end;
    end;
  j := t_graph_traj;
  a := variable[vargraph_x].val;
  valgraph_x[j] := a;
  if gsigma then valgraph_x_sigma[j] := 0.0;
  if gminmax then
    begin
      valgraph_x_min[j] := a;
      valgraph_x_max[j] := a;
    end;
  for i := 1 to imin(nb_cycle_carlo,maxgraph) do
    for k := 1 to nb_vargraph_y do
      begin
        valgraph_y[k][i] := 0.0;
        if gsigma then valgraph_y_sigma[k][i] := 0.0;
        if gminmax then
          begin
            valgraph_y_min[k][i] := maxextended;
            valgraph_y_max[k][i] := -maxextended;
          end;
      end;
  for k := 1 to nb_vargraph_y do
    begin
      a := variable[vargraph_y[k]].val;
      valgraph_y[k][j] := a;
      if gsigma then valgraph_y_sigma[k][j] := 0.0;
      if gminmax then
        begin
          valgraph_y_min[k][j] := a;
          valgraph_y_max[k][j] := a;
        end;
    end;
  end;
end;

procedure carlo_init_graph;
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then
        begin
          status_carlo(nb_cycle_carlo,nb_run_carlo);
          t_graph_traj  := 0;
          if distrib then
            carlo_init_graph_distrib(fg)
          else
            carlo_init_graph_traj(fg);
          status_dt_graph;
        end;
    end;
end;

procedure carlo_run_init_graph;
var f : integer;
begin
  for f := 1 to maxform_graph do
    with tab_form_graph[f] do if Visible then t_graph_traj  := 0;
end;

procedure carlo_graph_distrib(fg : tform_graph);
begin
  with fg do
    if ( icyc = nb_cycle_carlo ) then run_graph_distrib(fg);
end;

procedure run_graph_scatter(fg : tform_graph);
var k : integer;
begin
  with fg do
    begin
      if ( irun <= maxgraph) then
        begin
          valgraph_x[irun] := variable[vargraph_x].val;
          for k := 1 to nb_vargraph_y do
            valgraph_y[k][irun] := variable[vargraph_y[k]].val;
        end;
    end;
end;

procedure carlo_graph_scatter(fg : tform_graph);
begin
  with fg do
    if ( icyc = nb_cycle_carlo ) then run_graph_scatter(fg);
end;

procedure carlo_graph_traj(fg : tform_graph);
var j,k : integer;
    a : extended;
begin
  with fg do
    begin
      {if ( t_graph_traj > maxgraph ) then exit;}
      j := t_graph_traj;
      a := variable[vargraph_x].val;
      valgraph_x[j] := valgraph_x[j] + a;
      if gsigma then valgraph_x_sigma[j] := valgraph_x_sigma[j] + a*a;
      if gminmax then
        begin
          valgraph_x_min[j] := min(a,valgraph_x_min[j]);
          valgraph_x_max[j] := max(a,valgraph_x_max[j]);
        end;
      for k := 1 to nb_vargraph_y do
        begin
          a := variable[vargraph_y[k]].val;
          valgraph_y[k][j] := valgraph_y[k][j] + a;
          if gsigma then valgraph_y_sigma[k][j] := valgraph_y_sigma[k][j] + a*a;
          if gminmax then
            begin
              valgraph_y_min[k][j] := min(a,valgraph_y_min[k][j]);
              valgraph_y_max[k][j] := max(a,valgraph_y_max[k][j]);
            end;
        end;
    end;
end;

procedure carlo_graph(icyc : integer);
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then
        begin
          if ( icyc mod dt_graph = 0 ) then
            begin
              t_graph_traj  := t_graph_traj  + 1;
              if distrib then
                carlo_graph_distrib(fg)
              else
                if not gscatter then carlo_graph_traj(fg);
            end;
          if gscatter then carlo_graph_scatter(fg);
        end;
    end;
end;

procedure carlo_fin_graph_distrib(fg : tform_graph);
begin
  run_fin_graph_distrib(fg);
end;

procedure carlo_fin_graph_scatter(fg : tform_graph);
begin
  with fg do gscat(imin(nb_run_carlo,maxgraph));
end;

procedure carlo_fin_graph_traj(fg : tform_graph);
var i,k : integer;
    a,b : extended;
begin
  with fg do
    begin
      if ( t_graph_traj < 1 ) then exit;
        for i := 1 to imin(t_graph_traj,maxgraph) do
          begin
            a := valgraph_x[i]/nb_run_carlo;
            valgraph_x[i] := a;
            if gsigma then
              begin
                b := valgraph_x_sigma[i]/nb_run_carlo - a*a;
                b := sqrt(max(b,0.0));
                valgraph_x_sigma[i] := 2.0*b;
              end;
            for k := 1 to nb_vargraph_y do
              begin
                a := valgraph_y[k][i]/nb_run_carlo;
                valgraph_y[k][i] := a;
                if gsigma then
                  begin
                    b := valgraph_y_sigma[k][i]/nb_run_carlo - a*a;
                    b := sqrt(max(b,0.0));
                    valgraph_y_sigma[k][i] := 2.0*b;
                  end;
              end;
          end;
      gcarlo(imin(t_graph_traj,maxgraph));
    end;
end;

procedure carlo_fin_graph;
var f : integer;
    fg : tform_graph;
begin
  for f := 1 to maxform_graph do
    begin
      fg := tab_form_graph[f];
      with fg do if Visible then
        begin
          if not gplus then efface(nil);
          if distrib then
            carlo_fin_graph_distrib(fg)
          else
            if gscatter then
              carlo_fin_graph_scatter(fg)
            else
              carlo_fin_graph_traj(fg);
        end;
    end;
end;

{ ------  texte fic  ------ }

procedure carlo_fic(irun : integer);
var i,k : integer;
begin
  for i := 1 to fic_nb do with fic[i] do
    begin
      write(f,irun:6);
      for k := 1 to var_fic_nb do with variable[var_fic[k]] do
        write(f,hortab,val:1:precis[k]);
      writeln(f);
      Flush(f);
    end;
end;

{ ------  texte interp  ------ }

procedure carlo_init_texte_interp;
var k : integer;
begin
  for k := 1 to nb_vartexte_interp do
    begin
      varmoy[k]    := 0.0;
      varvar[k]    := 0.0;
      varmoy_ne[k] := 0.0;
      varvar_ne[k] := 0.0;
    end;
end;

procedure init_delta_texte_interp;
begin
  with form_ulm do
  if ( nb_cycle_carlo <= 50 )  then 
    delta_texte_interp := 1
  else 
  if ( nb_cycle_carlo <= 500 ) then 
    delta_texte_interp := 10
  else
  if ( nb_cycle_carlo <= 5000 ) then 
    delta_texte_interp := 100
  else 
  if ( nb_cycle_carlo <= 50000 ) then 
    delta_texte_interp := 1000
  else
    delta_texte_interp := nb_cycle_carlo div maxtexte_interp;
end;

procedure carlo_init_texte_interp2;
var x,i : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    for i := 0 to maxtexte_interp do
      begin
        ext_t[x][i]     := 0;
        nt_moy[x][i]    := 0.0;
        nt_var[x][i]    := 0.0;
        nt_moy_ne[x][i] := 0.0;
        nt_var_ne[x][i] := 0.0;
      end;
  init_delta_texte_interp;
end;

procedure carlo_texte_interp;
var k,x : integer;
    a : extended;
begin
  {if texte_interp then
    begin
      iwriteln('run ' +IntToStr(irun));
      for k := 1 to nb_vartexte_interp do
        iwriteln(s_ecri_variable(vartexte_interp[k]))
    end; }
  for k := 1 to nb_vartexte_interp do
    begin
      a := variable[vartexte_interp[k]].val;
      varmoy[k] := varmoy[k] + a;
      varvar[k] := varvar[k] + a*a;
      x := 1; {jjjjj}
      if not fini[x] then
        begin
          varmoy_ne[k] := varmoy_ne[k] + a;
          varvar_ne[k] := varvar_ne[k] + a*a;
        end;
    end;
end;

procedure carlo_run_init_texte_interp2;
begin
  t_texte_interp := 0;
end;

{procedure carlo_ext_text_interp(x : integer);
begin
  if texte_interp then
    iwriteln('run ' + IntToStr(irun) +
             '  model ' + s_ecri_model(x) +
             '  ext time = ' + IntToStr(icyc));
end;

procedure carlo_div_text_interp(x : integer);
begin
  if texte_interp then
    iwriteln('run ' + IntToStr(irun) +
             '  model ' + s_ecri_model(x) +
             '  div time = ' + IntToStr(icyc));
end;}

procedure carlo_texte_interp2(icyc : integer);
var x,j : integer;
    a : extended;
begin
  if ( icyc mod delta_texte_interp = 0 ) then
    begin
      t_texte_interp := t_texte_interp + 1;
      j := t_texte_interp;
      for x := 1 to modele_nb do with modele[x] do
        begin
          a := pop;
          nt_moy[x][j] := nt_moy[x][j] + a;
          nt_var[x][j] := nt_var[x][j] + a*a;
          if fini[x] then
            ext_t[x][j] := ext_t[x][j] + 1
          else
            begin
              nt_moy_ne[x][j] := nt_moy_ne[x][j] + a;
              nt_var_ne[x][j] := nt_var_ne[x][j] + a*a;
            end;
        end;
    end;
end;

procedure carlo_fin_texte_interp;
var k,x,n_ext : integer;
    a,b : extended;
begin
  iwriteln('Mean value [SE]:');
  for k := 1 to nb_vartexte_interp do
    begin
      a := varmoy[k]/nb_run_carlo;
      b := varvar[k]/nb_run_carlo - a*a;
      if ( b >= 0.0 ) then
        iwriteln(s_ecri_var(vartexte_interp[k]) + ' = ' +
                 Format('%10.4f',[a]) + ' [' +
                 Format('%1.4f',[sqrt(b/nb_run_carlo)]) + ']')
      else
        iwriteln(s_ecri_var(vartexte_interp[k]) + ' = ' +
                 Format('%10.4f',[a]) + ' [-]');
    end;
  iwriteln('Mean value over non extinct trajectories [SE]:');
  for k := 1 to nb_vartexte_interp do
    begin
      x := 1; {jjjjj}
      n_ext := nb_run_carlo - ext[x];
      if ( n_ext > 0.0 ) then
        begin
          a := varmoy_ne[k]/n_ext;
          b := varvar_ne[k]/n_ext - a*a;
          if ( b >= 0.0 ) then
            iwriteln(s_ecri_var(vartexte_interp[k]) + '* = ' +
                 Format('%10.4f',[a]) + ' [' +
                 Format('%1.4f',[sqrt(b/n_ext)]) + ']')
          else
            iwriteln(s_ecri_var(vartexte_interp[k]) + '* = ' +
                 Format('%10.4f',[a]) + ' [-]');
        end;
    end;
end;

procedure carlo_fin_texte_interp2(x : integer);
var it,icyc,n_ext : integer;
    a,b,pe : extended;
    s : string;
begin
  lines_run.Clear;
  it := 0;
  s := Format('%10s%14s%10s%10s%10s%10s',['t','pe(t)','pop(t)','SE','pop*(t)','SE']);
  lines_run.Add(s);
  for icyc := 1 to nb_cycle_carlo do
    if ( icyc mod delta_texte_interp = 0 ) then
      begin
        it := it + 1;
        n_ext := nb_run_carlo - ext_t[x][it];
        pe := ext_t[x][it]/nb_run_carlo;
        a  := nt_moy[x][it]/nb_run_carlo;
        s  := Format('%10d  %10.4f %10.1f ',[icyc,pe,a]);
        b  := nt_var[x][it]/nb_run_carlo - a*a;
        if ( b >= 0.0 ) then
          s := s + Format('%10.1f ',[sqrt(b/nb_run_carlo)])
        else
          s := s + Format('%10s ',['-']);
        if ( n_ext > 0 ) then
          begin
            a := nt_moy_ne[x][it]/n_ext;
            s := s + Format('%10.1f ',[a]);
            b := nt_var_ne[x][it]/n_ext - a*a;
            if ( b >= 0.0 ) then
              s := s + Format('%10.1f ',[sqrt(b/n_ext)])
            else
              s := s + Format('%10s ',['-']);
          end
        else
          s := s + Format('%10s %10s ',['-','-']);
        lines_run.Add(s);
      end;
  bwriteln(lines_run);
end;

{ ------  text ------ }

procedure carlo_init_text;
var k,i,f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then
        begin
          for i := 1 to maxtext do ext_tt[i] := 0;
          for k := 1 to nb_vartext_carlo do
            for i := 0 to maxtext do
              begin
                valtext_moy[k][i]    := 0.0;
                valtext_var[k][i]    := 0.0;
                valtext_moy_ne[k][i] := 0.0;
                valtext_var_ne[k][i] := 0.0;
              end;
          for k := 1 to nb_vartext_carlo do
            with variable[vartext[k]] do
              begin
                valtext_moy[k][0]    := val;
                valtext_moy_ne[k][0] := val;
              end;
          init_text_carlo;
        end;
    end;
end;

procedure carlo_text(icyc : integer);
var k,x,j,f : integer;
    a : extended;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then if ( icyc mod dt_text  = 0 ) then
        begin
          if ( t_text >= maxtext ) then exit;
          t_text := t_text + 1;
          j := t_text;
          for k := 1 to nb_vartext_carlo do
            begin
              a := variable[vartext[k]].val;
              valtext_moy[k][j] := valtext_moy[k][j] + a;
              valtext_var[k][j] := valtext_var[k][j] + a*a;
            end;
          x := 1; {jjjjj}
          if fini[x] then
          {if ( modele[x].pop = 0.0 ) then}
          { @@@ modif : compte les trajectoires nulles, et non pas les trajectoires eteintes @@@ }
            ext_tt[j] := ext_tt[j] + 1
          else
            for k := 1 to nb_vartext_carlo do
              begin
                a := variable[vartext[k]].val;
                valtext_moy_ne[k][j] := valtext_moy_ne[k][j] + a;
                valtext_var_ne[k][j] := valtext_var_ne[k][j] + a*a;
              end;
        end;
    end;
end;

procedure carlo_run_init_text;
var f : integer;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then t_text := 0;
    end;
end;

procedure carlo_fin_text;
var k,i,it,n_ext,f : integer;
    a,b,pe : extended;
    ft : tform_text;
begin
  for f := 1 to maxform_text do
    begin
      ft := tab_form_text[f];
      with ft do if Visible then
        begin
          it := 0;
          for i := 1 to nb_cycle_carlo do
            if ( i mod dt_text = 0 ) then
              begin
                if ( it >= maxtext ) then break;
                it := it + 1;
                n_ext := nb_run_carlo - ext_tt[it];
                pe := ext_tt[it]/nb_run_carlo;
                { proba d'extinction ou -- modif -- proportion de zeros }
                for k := 1 to nb_vartext_carlo do
                  begin
                    a := valtext_moy[k][it]/nb_run_carlo;
                    valtext_moy[k][it] := a;
                    b := valtext_var[k][it]/nb_run_carlo - a*a;
                    if ( b >= 0.0 ) then
                      valtext_var[k][it] := sqrt(b/nb_run_carlo) { SE }
                    else
                      valtext_var[k][it] := 0.0; {iiiii}
                      {write('-':10,' ');}
                    if ( n_ext > 0 ) then
                      begin
                        a := valtext_moy_ne[k][it]/n_ext;
                        valtext_moy_ne[k][it] := a;
                        b := valtext_var_ne[k][it]/n_ext - a*a;
                        if ( b >= 0.0 ) then
                          valtext_var_ne[k][it] := sqrt(b/n_ext)  { SE }
                          {valtext_var_ne[k][it] := sqrt(b)/a}  { CVZ }
                        else
                          valtext_var_ne[k][it] := 0.0; {iiiii}
                          {write('-':10,' '); }
                      end
                    else
                      begin
                        valtext_moy_ne[k][it] := 0.0;
                        valtext_var_ne[k][it] := 0.0;
                        {write('-':10,' ','-':10,' ');}
                      end;
                  end;
                maj_text_carlo(it,pe);
              end;
        fin_text_run;
      end;
    end;
end;

{ ------ procedure MonteCarlo ------ }

procedure test_extinct;
var x: integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    if not fini[x] then
      begin 
        nsom[x] := nsom[x] + pop; 
        if ( pop < seuil_ext ) or ( pop = 0.0 ) then { extinction }
          begin 
            fini[x] := true;
            ext[x]  := ext[x] + 1;
            ext_time[x] := icyc;
            {carlo_ext_text_interp(x); }
          end
        else 
          if ( not diverg[x] and ( pop > seuil_div )) then { divergence }
            begin 
              diverg[x] := true; 
              dvg[x]    := dvg[x] + 1;
              div_time[x] := icyc;
              {carlo_div_text_interp(x); }
            end;
      end;
end;

procedure carlo_run;
var x,i : integer;
    a : extended;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin 
      nmoy[x] := nmoy[x] + pop;
      a := (ln0(pop) - ln0(pop0))/nb_cycle_carlo;
      lnlamb_moy[x] := lnlamb_moy[x] + a;
      lnlamb_var[x] := lnlamb_var[x] + a*a;
      a := exp(a);
      lamb_moy[x] := lamb_moy[x] + a; 
      lamb_var[x] := lamb_var[x] + a*a;
      if not fini[x] then
        begin 
          nmoy2[x] := nmoy2[x] + pop; 
          nsom2[x] := nsom2[x] + nsom[x]; 
          nmoy_ne[x] := nmoy_ne[x] + pop; 
          nvar_ne[x] := nvar_ne[x] + pop*pop; 
          nmax_ne[x] := max(nmax_ne[x],pop); 
          nmin_ne[x] := min(nmin_ne[x],pop); 
          if ( nsom[x] = pop ) then 
            a := 0.0 
          else 
            a := (nsom[x] - pop0)/(nsom[x] - pop);
          lamb2_moy[x] := lamb2_moy[x] + a; 
          lamb2_var[x] := lamb2_var[x] + a*a;
        end
      else 
        begin 
          a := ext_time[x];
          ext_time_moy[x] := ext_time_moy[x] + a;
          ext_time_var[x] := ext_time_var[x] + a*a;
        end; 
      if diverg[x] then 
        begin 
          a := div_time[x];
          div_time_moy[x] := div_time_moy[x] + a; 
          div_time_var[x] := div_time_var[x] + a*a; 
        end; 
      if not fini[x] then
        if ( xvec <> 0 ) then
          for i := 1 to size do
            wmoy[x][i] := wmoy[x][i] + vec[xvec].val[i]/pop
        else
          for i := 1 to size do
            wmoy[x][i] := wmoy[x][i] + rel[xrel[i]].val/pop;
    end;
end;

procedure fin_carlo;
var x,i : integer;
    a,b,n_ext : extended;
    s : string;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin 
      n_ext := nb_run_carlo - ext[x];
      iwriteln('Model ' + s_ecri_model(x) +
               Format(' (extinction threshold = %1.2f;',[seuil_ext]) +
               Format(' escape threshold = %1.2f)',[seuil_div]) + ')');
      if ( n_ext > 0.0 ) then
        begin
          iwriteln('  Non extinct population size (pop*):');
          iwriteln(Format('    min = %1.2f',[nmin_ne[x]]));
          iwriteln(Format('    max = %1.2f',[nmax_ne[x]]));
          a := nmoy_ne[x]/n_ext;
          iwriteln(Format('    mean = %1.2f',[a]));
          b := nvar_ne[x]/n_ext - a*a;
          if ( b >= 0.0 ) then 
            begin
              iwriteln(Format('      sigma = %1.4f',[sqrt(b)]));
              iwriteln(Format('      SE = %1.4f',[sqrt(b/n_ext)]));
            end;
        end;
      with form_graph do if gregress then
        iwriteln('  Regression: slope = ' + s_ecri_val(regress_a) +
                         ' intercept = ' + s_ecri_val(regress_b));
      iwriteln(Format('  Probability of escape: %1.4f',[dvg[x]/nb_run_carlo]));
      if ( dvg[x] > 0.0 ) then 
        begin
          a := div_time_moy[x]/nb_run_carlo;
          b := div_time_var[x]/nb_run_carlo - a*a;
          if ( b >= 0.0 ) then
            iwriteln(Format('  Mean escape time [SE]: %1.4f [%1.4f]',
                            [a,sqrt(b/nb_run_carlo)]))
          else
            iwriteln(Format('  Mean escape time [SE]: %1.4f [-]',[a]));
          a := div_time_moy[x]/dvg[x];
          b := div_time_var[x]/dvg[x] - a*a;
          if ( b >= 0.0 ) then
            iwriteln(Format('  Mean escape time over escape trajectories [SE]: %1.4f [%1.4f]',
                            [a,sqrt(b/nb_run_carlo)]))
          else
            iwriteln(Format('  Mean escape time over escape trajectories [SE]: %1.4f [-]',[a]));
          end;
      iwriteln(Format('  Probability of extinction: %1.4f',[ext[x]/nb_run_carlo]));
      if ( ext[x] > 0.0 ) then
        begin
          a := ext_time_moy[x]/nb_run_carlo;
          b := ext_time_var[x]/nb_run_carlo - a*a;
          if ( b >= 0.0 ) then
            iwriteln(Format('  Mean extinction time [SE]: %1.4f [%1.4f]',
                            [a,sqrt(b/nb_run_carlo)]))
          else
            iwriteln(Format('  Mean extinction time [SE]: %1.4f [-]',[a]));
          a := ext_time_moy[x]/ext[x];
          b := ext_time_var[x]/ext[x] - a*a;
          if ( b >= 0.0 ) then
            iwriteln(Format('  Mean extinction time over extinct trajectories [SE]: %1.4f [%1.4f]',
                     [a,sqrt(b/nb_run_carlo)]))
          else
            iwriteln(Format('  Mean extinction time over extinct trajectories [SE]: %1.4f [-]',[a]));
        end;
      a := lnlamb_moy[x]/nb_run_carlo;
      b := lnlamb_var[x]/nb_run_carlo - a*a;
      iwriteln(Format('  Stochastic growth rate: %1.6f',[exp(a)]));
      if ( b >= 0.0 ) then
        iwriteln(Format('      Logarithmic growth rate [SE]: %1.6f [%1.4f]',
                 [a,sqrt(b/nb_run_carlo)]))
      else
        iwriteln(Format('      Logarithmic growth rate [SE]: %1.6f [-]',[a]));
      a := lamb_moy[x]/nb_run_carlo;
      b := lamb_var[x]/nb_run_carlo - a*a;
      if ( b >= 0.0 ) then
        iwriteln(Format('  Mean growth rate [SE]: %1.6f [%1.4f]',
                 [a,sqrt(b/nb_run_carlo)]))
      else 
        iwriteln(Format('  Mean growth rate [SE]: %1.6f [-]',[a]));
      a := exp((ln0(nmoy[x]/nb_run_carlo) - ln0(pop0))/nb_cycle_carlo);
      iwriteln(Format('  Growth rate of the mean pop: %1.6f',[a]));
      if ( n_ext > 0.0 ) then
        begin 
          a := lamb2_moy[x]/n_ext;
          b := lamb2_var[x]/n_ext - a*a;
          if ( b >= 0.0 ) then 
            iwriteln(Format('  Mean growth rate2 [SE]: %1.6f [%1.6f]',
                            [a,sqrt(b/n_ext)]))
          else 
            iwriteln(Format('  Mean growth rate2 [SE]: %1.6f [-]',[a]));
          if ( (nsom2[x] - nmoy2[x]) > 0.0 ) then 
            begin 
              a := (nsom2[x] - n_ext*pop0)/(nsom2[x] - nmoy2[x]);
              iwriteln(Format('  Growth rate2 of the mean pop: %1.6f',[a]));
            end; 
        end; 
      if ( n_ext > 0.0 ) then
        begin
          for i := 1 to size do wmoy[x][i] := wmoy[x][i]/n_ext;
          iwriteln('  Mean scaled population structure:');
          s := '';
          for i := 1 to size do s := s + Format('%8.4f',[wmoy[x][i]]);
          iwriteln(s);
        end;
      carlo_fin_texte_interp2(x);
    end;
end;

procedure init_traj;
var x: integer;
begin
  for x := 1 to modele_nb do
    begin
      fini[x]   := false;
      diverg[x] := false;
      nsom[x]   := modele[x].pop0;
    end;
end;

procedure init_carlo;
var x,i : integer;
begin
  for x := 1 to modele_nb do with modele[x] do
    begin
      ext[x] := 0;
      dvg[x] := 0;
      ext_time_moy[x] := 0.0; 
      ext_time_var[x] := 0.0; 
      div_time_moy[x] := 0.0;
      div_time_var[x] := 0.0;
      nmoy[x]  := 0.0; 
      nmoy2[x] := 0.0;
      nsom2[x] := 0.0; 
      nmoy_ne[x]    := 0.0;
      nvar_ne[x]    := 0.0;
      nmax_ne[x]    := 0.0;
      nmin_ne[x]    := maxextended;
      lnlamb_moy[x] := 0.0;
      lnlamb_var[x] := 0.0;
      lamb_moy[x]   := 0.0;
      lamb_var[x]   := 0.0;
      lamb2_moy[x]  := 0.0;
      lamb2_var[x]  := 0.0;
      for i := 1 to size do wmoy[x][i] := 0.0;
    end;
end;


{ccccc}

{procedure carlo_stoc_n0(irun : integer);

var x : integer;

begin
  if ( irun <= 10000 ) then
    begin
      x := modele[1].xrel[1];
      x := rel[x].xvar;
      if not carlo_n0 then
        carlo_tab_n0[irun] := variable[x].val;
    end;
end;}

begin
  init_eval1;
  init_carlo;
  carlo_init_texte_interp;
  carlo_init_texte_interp2;
  carlo_init_text;
  carlo_init_graph;
  for irun := 1 to nb_run_carlo do
    begin
      graine := graine0 + irun-1;
      init_eval1;
      init_traj;
      traj__ := irun;
      if ( irun mod 100 = 0 ) then with form_ulm do status_traj;
      carlo_run_init_texte_interp2;
      carlo_run_init_text;
      carlo_run_init_graph;
      for icyc := 1 to nb_cycle_carlo do
        begin
          run_t;
          test_extinct;
          carlo_graph(icyc);
          carlo_text(icyc);
          carlo_texte_interp2(icyc);
          with form_ulm do
            begin
              procproc;
              if runstop then
                begin
                  runstop := false;
                  goto 1;
                end;
            end;
        end;
      carlo_texte_interp;
      carlo_run;
      carlo_fic(irun);
      {carlo_stoc_n0(irun);} {ccccc}
    end;
  carlo_fin_graph;
  carlo_fin_texte_interp;
  carlo_fin_text;
  fin_carlo;
1:
  graine := graine0;
  init_eval1;
  form_ulm.status_run('Init');
  form_ulm.status_time;
end;

end.
