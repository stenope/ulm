unit gviewall;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  ComCtrls, Menus, ExtCtrls, StdCtrls;

type
  tform_view_all = class(TForm)
    TreeView1: TTreeView;
    StatusBar1: TStatusBar;
    Memo1: TMemo;
    procedure tree;
    procedure display;
    procedure TreeView1Change(Sender: TObject; Node: TTreeNode);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_viewall;
  private
    procedure status(s : string);
  public
  end;

var  form_view_all: tform_view_all;

implementation

uses jglobvar,jutil,jsymb,jsyntax;

{$R *.lfm}

procedure tform_view_all.status(s : string);
begin
  statusbar1.Panels[0].Text := s;
end;

procedure tform_view_all.tree;
var treenode,tn : TTreeNode;
    z,x,i : integer;
begin
  with treeview1.Items do
    begin
      treenode := nil;
      for z := 1 to modele_nb do with modele[z] do
        begin
          treenode := Add(treenode,s_ecri_model(z));
          if ( xmat <> 0 ) then
            begin
              AddChild(treenode,s_ecri_mat(xmat));
              AddChild(treenode,s_ecri_vec(xvec));
            end
          else
            begin
              AddChild(treenode,'Relations');
              tn := treenode.GetLastChild;
              for i := 1 to size do
                AddChild(tn,s_ecri_rel(xrel[i]));
            end;
        end;
      if ( fun_nb - fun_nb_predef > 0 ) then
        begin
          treenode := Add(treenode,'Functions');
          for x := fun_nb_predef+1 to fun_nb do
            AddChild(treenode,s_ecri_fun(x));
        end;
      i := 0;
      for x := 1 to rel_nb do with rel[x] do
        if ( xmodele = 0 ) then i := i + 1;
      if ( i > 0 ) then
        begin
          treenode := Add(treenode,'Other relations');
          for x := 1 to rel_nb do with rel[x] do
            if ( xmodele = 0 ) then
              AddChild(treenode,s_ecri_rel(x));
        end;
      treenode := Add(treenode,'Variables');
      for x := 1 to variable_nb do
        AddChild(treenode,s_ecri_var(x));
    end;
end;

procedure tform_view_all.display;
var x,tx,z,tz : integer;
    s : string;
begin
  with treeview1 do
    if Visible and Assigned(Selected) then
      begin
        s := Selected.Text;
        if ( s = 'Variables' ) then b_ecri_list_variable
        else
        if ( s = 'Relations' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_list_rel_model(z);
          end
        else
        if ( s = 'Functions' ) then b_ecri_list_fun
        else
        if ( s = 'Other relations') then b_ecri_list_rel_indep
        else
        if ( s = 'Matrix' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_mat(modele[z].xmat);
          end
        else
        if ( s = 'Vector' ) then
          begin
            trouve_obj(Selected.Parent.Text,z,tz);
            b_ecri_vec(modele[z].xvec);
          end
        else
          begin
            trouve_obj(s,x,tx);
            b_ecri(x,tx);
          end;
        memo1.Clear;
        memo1.Lines := lines_syntax;
      end;
end;

procedure tform_view_all.TreeView1Change(Sender: TObject; Node: TTreeNode);
begin
  display;
end;

procedure tform_view_all.FormCreate(Sender: TObject);
begin
  Left   := 300;
  Top    := 50;
  Height := 530;
  Width  := 706;
  adjust(self);
  memo1.ReadOnly := true;
  memo1.Clear;
end;

procedure tform_view_all.FormActivate(Sender: TObject);
begin
  {treeview1.Items.Clear;
  tree; }
  display;
end;

procedure tform_view_all.init_viewall;
begin
  treeview1.Items.Clear;
  tree; {voir a reconstruire l'arbre si changevar, newvar }{iiiii}
  with treeview1 do Selected := Items[1];
  display;
end;

end.
