# A simple makefile for ULM.
# This file is part of the ULM program,
# (c) 2016 The Ulm Development team / License GPLv3+
# This is just aliases to build ULM without firing the whole lazarus IDE.

ulm: ulm.lpr
	lazbuild ulm.lpr
clean:
	rm *.ppu *.o ulm.compiled ulm ulm.or
