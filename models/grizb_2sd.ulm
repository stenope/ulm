{ Two-sex model for Grizzly bear population (Ursus arctus horriblis)
{ with demographic stochasticity and population ceiling.

{ **** decreasing population ****

{ See related files grizb_0.ulm, grizb_2s.ulm, grizb_2se.ulm.

{ Mills LS, SG Hayes, C Baldwin, MJ Wisdom, J Citta, DJ Mattson & K Murphy. 1996.
{ Factors leading to different viability predictions for a Grizzly bear data set. 
{ Conservation Biology 10:863-873.

{ Example of commands: to be compared with results of Mills et al.
{	decreasing population (lambda = 0.93)
{	initial population size: 125 males + 125 females
{
{	graph t n
{	text  n	
{	montecarlo 48 500 2     ( run Monte Carlo simulation
{				( 48 time steps, 500 repetitions,
{				( extinction threshold = 2 (0 or 1 individual left)
{	montecarlo 48 500 100	( run Monte Carlo simulation
{				( 48 time steps, 500 repetitions,
{				( extinction threshold = 100 individuals
{ Results:
{	growth rate estimator (mean growth rate) = 0.9165
{	probability of extinction = 0.328
{	mean time to extinction = 42.8 years
{	average population size (standard error) = 6.2 (0.3)
{	average non extinct population size (standard error) = 8.9 (0.4)
{	probability of quasi-extinction (< 100 individuals) = 1.0
{	mean time to quasi-extinction (< 100 individuals) = 12.7 years

defmod grizb(60)
rel:f1,f2,f3,f4,f5,f6,f7,f8,f9,f10,f11,f12,f13,f14,f15,f16,f17,f18,f19,f20,f21,f22,f23,f24,f25,f26,f27,f28,f29,f30,m1,m2,m3,m4,m5,m6,m7,m8,m9,m10,m11,m12,m13,m14,m15,m16,m17,m18,m19,m20,m21,m22,m23,m24,m25,m26,m27,m28,m29,m30

{ ---- relations for females ----

defrel f1
nf1 = if(nd,nf1a,trunc(k*nf1/n))

defrel f2
nf2 = if(nd,binomf(nf1,s1),trunc(k*nf2/n))

defrel f3
nf3 = if(nd,binomf(nf2,s1),trunc(k*nf3/n))

defrel f4
nf4 = if(nd,binomf(nf3,s1),trunc(k*nf4/n))

defrel f5
nf5 = if(nd,binomf(nf4,s1),trunc(k*nf5/n))

defrel f6
nf6 = if(nd,binomf(nf5,sa),trunc(k*nf6/n))

defrel f7
nf7 = if(nd,binomf(nf6,sa),trunc(k*nf7/n))

defrel f8
nf8 = if(nd,binomf(nf7,sa),trunc(k*nf8/n))

defrel f9
nf9 = if(nd,binomf(nf8,sa),trunc(k*nf9/n))

defrel f10
nf10 = if(nd,binomf(nf9,sb),trunc(k*nf10/n))

defrel f11
nf11 = if(nd,binomf(nf10,sb),trunc(k*nf11/n))

defrel f12
nf12 = if(nd,binomf(nf11,sb),trunc(k*nf12/n))

defrel f13
nf13 = if(nd,binomf(nf12,sb),trunc(k*nf13/n))

defrel f14
nf14 = if(nd,binomf(nf13,sb),trunc(k*nf14/n))

defrel f15
nf15 = if(nd,binomf(nf14,sb),trunc(k*nf15/n))

defrel f16
nf16 = if(nd,binomf(nf15,sc),trunc(k*nf16/n))

defrel f17
nf17 = if(nd,binomf(nf16,sc),trunc(k*nf17/n))

defrel f18
nf18 = if(nd,binomf(nf17,sc),trunc(k*nf18/n))

defrel f19
nf19 = if(nd,binomf(nf18,sc),trunc(k*nf19/n))

defrel f20
nf20 = if(nd,binomf(nf19,sc),trunc(k*nf20/n))

defrel f21
nf21 = if(nd,binomf(nf20,sc),trunc(k*nf21/n))

defrel f22
nf22 = if(nd,binomf(nf21,sc),trunc(k*nf22/n))

defrel f23
nf23 = if(nd,binomf(nf22,sc),trunc(k*nf23/n))

defrel f24
nf24 = if(nd,binomf(nf23,sc),trunc(k*nf24/n))

defrel f25
nf25 = if(nd,binomf(nf24,sc),trunc(k*nf25/n))

defrel f26
nf26 = if(nd,binomf(nf25,sc),trunc(k*nf26/n))

defrel f27
nf27 = if(nd,binomf(nf26,sc),trunc(k*nf27/n))

defrel f28
nf28 = if(nd,binomf(nf27,sc),trunc(k*nf28/n))

defrel f29
nf29 = if(nd,binomf(nf28,sd),trunc(k*nf29/n))

defrel f30
nf30 = if(nd,binomf(nf29,se),trunc(k*nf30/n))

{ ---- relations for males ----

defrel m1
nm1 = if(nd,nm1a,trunc(k*nm1/n))

defrel m2
nm2 = if(nd,binomf(nm1,s1),trunc(k*nm2/n))

defrel m3
nm3 = if(nd,binomf(nm2,s1),trunc(k*nm3/n))

defrel m4
nm4 = if(nd,binomf(nm3,s1),trunc(k*nm4/n))

defrel m5
nm5 = if(nd,binomf(nm4,s1),trunc(k*nm5/n))

defrel m6
nm6 = if(nd,binomf(nm5,sa),trunc(k*nm6/n))

defrel m7
nm7 = if(nd,binomf(nm6,sa),trunc(k*nm7/n))

defrel m8
nm8 = if(nd,binomf(nm7,sa),trunc(k*nm8/n))

defrel m9
nm9 = if(nd,binomf(nm8,sa),trunc(k*nm9/n))

defrel m10
nm10 = if(nd,binomf(nm9,sb),trunc(k*nm10/n))

defrel m11
nm11 = if(nd,binomf(nm10,sb),trunc(k*nm11/n))

defrel m12
nm12 = if(nd,binomf(nm11,sb),trunc(k*nm12/n))

defrel m13
nm13 = if(nd,binomf(nm12,sb),trunc(k*nm13/n))

defrel m14
nm14 = if(nd,binomf(nm13,sb),trunc(k*nm14/n))

defrel m15
nm15 = if(nd,binomf(nm14,sb),trunc(k*nm15/n))

defrel m16
nm16 = if(nd,binomf(nm15,sc),trunc(k*nm16/n))

defrel m17
nm17 = if(nd,binomf(nm16,sc),trunc(k*nm17/n))

defrel m18
nm18 = if(nd,binomf(nm17,sc),trunc(k*nm18/n))

defrel m19
nm19 = if(nd,binomf(nm18,sc),trunc(k*nm19/n))

defrel m20
nm20 = if(nd,binomf(nm19,sc),trunc(k*nm20/n))

defrel m21
nm21 = if(nd,binomf(nm20,sc),trunc(k*nm21/n))

defrel m22
nm22 = if(nd,binomf(nm21,sc),trunc(k*nm22/n))

defrel m23
nm23 = if(nd,binomf(nm22,sc),trunc(k*nm23/n))

defrel m24
nm24 = if(nd,binomf(nm23,sc),trunc(k*nm24/n))

defrel m25
nm25 = if(nd,binomf(nm24,sc),trunc(k*nm25/n))

defrel m26
nm26 = if(nd,binomf(nm25,sc),trunc(k*nm26/n))

defrel m27
nm27 = if(nd,binomf(nm26,sc),trunc(k*nm27/n))

defrel m28
nm28 = if(nd,binomf(nm27,sc),trunc(k*nm28/n))

defrel m29
nm29 = if(nd,binomf(nm28,sd),trunc(k*nm29/n))

defrel m30
nm30 = if(nd,binomf(nm29,se),trunc(k*nm30/n))

{ initial population size according to stable age distribution:
defvar nf1 = 17

defvar nf2 = 15

defvar nf3 = 12

defvar nf4 = 11
     
defvar nfj = nf1+nf2+nf3+nf4

defvar nf5 = 9

defvar nf6 = 8

defvar nf7 = 6

defvar nf8 = 6

defvar nfa = nf5+nf6+nf7+nf8

defvar nf9 = 5

defvar nf10 = 4

defvar nf11 = 4

defvar nf12 = 3

defvar nf13 = 3

defvar nf14 = 2

defvar nfb = nf9+nf10+nf11+nf12+nf13+nf14

defvar nf15 = 2

defvar nf16 = 2

defvar nf17 = 2

defvar nf18 = 2

defvar nf19 = 2

defvar nf20 = 1

defvar nf21 = 1

defvar nf22 = 1

defvar nf23 = 1

defvar nf24 = 1

defvar nf25 = 1

defvar nf26 = 1

defvar nf27 = 1

defvar nf28 = 1

defvar nf29 = 1

defvar nf30 = 0

defvar nfc = nf15+nf16+nf17+nf18+nf19+nf20+nf21+nf22+nf23+nf24+nf25+nf26+nf27+nf28+nf29+nf30

defvar nf = nfj+nfa+nfb+nfc

defvar nm1 = 17

defvar nm2 = 15

defvar nm3 = 12

defvar nm4 = 11
     
defvar nmj = nm1+nm2+nm3+nm4

defvar nm5 = 9

defvar nm6 = 8

defvar nm7 = 6

defvar nm8 = 6

defvar nma = nm5+nm6+nm7+nm8

defvar nm9 = 5

defvar nm10 = 4

defvar nm11 = 4

defvar nm12 = 3

defvar nm13 = 3

defvar nm14 = 2

defvar nmb = nm9+nm10+nm11+nm12+nm13+nm14

defvar nm15 = 2

defvar nm16 = 2

defvar nm17 = 2

defvar nm18 = 2

defvar nm19 = 2

defvar nm20 = 1

defvar nm21 = 1

defvar nm22 = 1

defvar nm23 = 1

defvar nm24 = 1

defvar nm25 = 1

defvar nm26 = 1

defvar nm27 = 1

defvar nm28 = 1

defvar nm29 = 1

defvar nm30 = 0

defvar nmc = nm15+nm16+nm17+nm18+nm19+nm20+nm21+nm22+nm23+nm24+nm25+nm26+nm27+nm28+nm29+nm30

defvar nm = nmj+nma+nmb+nmc

defvar n = nm + nf

{ carrying capacity
defvar k = 250

{ density dependence with ceiling
defvar nd = n < (k+1)

{ survival rates:
defvar s1 = 0.74

defvar sa = 0.76

defvar sb = 0.85

defvar sc = 0.75

defvar sd = 0.40

defvar se = 0.20

{ fecundity rates:
defvar ca1 = 0.25

defvar ca2 = 0.40

defvar ca3 = 0.35

defvar ga = 0.32

{ proportion of reproductive females
defvar nfar = binomf(nfa,ga)

{ offspring
defvar pa = tabf(0.0,ca1,ca2,ca3) @ nfar

{ female sex ratio
defvar sigma = 0.5

defvar cb1 = 0.20

defvar cb2 = 0.40

defvar cb3 = 0.40

defvar gb = 0.37

{ proportion of reproductive females
defvar nfbr = binomf(nfb,gb)

{ offspring
defvar pb = tabf(0.0,cb1,cb2,cb3) @ nfbr

defvar cc1 = 0.25

defvar cc2 = 0.40

defvar cc3 = 0.35

defvar gc = 0.27

{ proportion of reproductive females
defvar nfcr = binomf(nfc,gc)

{ offspring
defvar pc = tabf(0.0,cc1,cc2,cc3) @ nfcr

{ total number of offspring
defvar p = pa + pb + pc

{ female offspring
defvar nf1a = binomf(p,sigma)

{ male offspring
defvar nm1a = p - nf1a





      

