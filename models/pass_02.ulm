{ Deterministic two-sex model for passerine with mating system inducing frequency dependence
{ See related female-based model file pass_0.ulm

{ Example of commands: transient effect of sex ratio
{ 	property			( give growth rate
{ 	run 20				( run 20 time steps
{					( display transient dynamics
{	property			( give growth rate		
{	run 20				( run 20 time steps
{	property			( growth rate has stabilized

{ Example of commands: two-sex growth rate
{	newvar g lambdaf(1,1)		( create new variable whose value is 
{					( dominant eigenvalue of the matrix
{	graph sigma g			( display growth rate function of sex ratio sigma
{	xscale 0 1			( fix bounds in X
{	yscale 0 2			( fix bounds in Y
{	line				( line off
{	addgraph			( superimpose graphs
{	skip 40				( don't display first 40 time steps
{	parameter sigma 0 1 0.01	( vary sex ratio by increment 0.01
{	run 50				( 2-sex growth rate is displayed

{ The exact formula for g(sigma) is given in:	

{ Legendre S, J Clobert, AP Moller & G Sorci. 1999.
{ Demographic stochasticty and social mating system in the process of extinction
{ of small populations: The case of passerines introduced to New Zealand.
{ American Naturalist 153:449-463.

defmod passerine_2(4)
mat: a
vec: w

defvec w(4)
nm1,nm2,nf1,nf2

defmat a(4)
 0,   0, (1-sigma)*sm0*f1*alpha1, (1-sigma)*sm0*f2*alpha2
sm,  vm,                       0,                       0
 0,   0,     sigma*sf0*f1*alpha1,     sigma*sf0*f2*alpha2
 0,   0,                      sf,                      vf

{ population sizes:
defvar nm1 = 0
    
defvar nm2 = 50
  
defvar nf1 = 0
    
defvar nf2 = 100

defvar nm = nm1 + nm2

defvar nf = nf1 + nf2

defvar n = nm + nf
 
{ male and female survival rates: 
defvar s0 = 0.2

defvar s = 0.35

defvar v = 0.5

defvar sf0 = s0

defvar sf = s

defvar vf = v

defvar sm0 = s0

defvar sm = s

defvar vm = v

{ female fecundities
defvar f1 = 7

defvar f2 = 7

{ primary sex-ratio
defvar sigma = 0.5

{ number of matings according to various mating functions:
{ monogamous
defvar matmf = min(nm,nf)

{ polygynous with harem size of 2
{defvar matmf = min(2*nm,nf)

{ harmonic mean 
{defvar matmf = min(2*nm*nf/(nm+nf),nf)

{ matings are dispatched equitably among adults and subadults:
defvar alpha1 = matmf/nf

defvar alpha2 = matmf/nf

{ breeding sex ratio 
defvar rho = nf/n

