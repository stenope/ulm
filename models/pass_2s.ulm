{ Two-sex model for passerine with demographic stochasticity.
{ See related constant models pass_0.ulm and pass_02.ulm
{
{ *** This model file is used as example in the reference manual ulmref.pdf
{
{ The model shows the importance of the mating system for short-lived bird species.
{
{ Legendre S, J Clobert, AP Moller & G Sorci. 1999. Demographic stochasticty
{ and social mating system in the process of extinction of small populations:
{ The case of passerines introduced to New Zealand.
{ American Naturalist 153:449-463.

{ Example of commands:
{	? graph t n		        ( population size
{	? text t n
{	? montecarlo 50 100	        ( give probability of extinction
{	? change matmf min(2*nm,nf)	( change mating system
{					( polygynous with harem size of 2
{	? montecarlo 50 100	        ( almost no extinction occurs

defmod pass_2s(4)
rel : rm1,rm2,rf1,rf2

{ relation for male juvenile survival
defrel rm1
nm1 = binomf(pf1m+pf2m,sm0)

{ relation for male subadult and adult survival
defrel rm2
nm2 = binomf(nm1,sm) + binomf(nm2,vm)

{ relation for female juvenile survival
defrel rf1
nf1 = binomf(pf1f+pf2f,sf0)

{ relation for male subadult and adult survival
defrel rf2
nf2 = binomf(nf1,sf) + binomf(nf2,vf)

{ initial number of males
defvar nm1 = 12

defvar nm2 = 12

{ total number of males
defvar nm = nm1 + nm2

{ initial number of females
defvar nf1 = 12

defvar nf2 = 12

{ total number of females
defvar nf = nf1 + nf2

{ total population size
defvar n = nm + nf

{ juvenile survival rate
defvar s0 = 0.2

{ subadult survival rate
defvar s = 0.35

{ adult survival rate
defvar v = 0.5

{ male survival rates:
defvar sm0 = s0

defvar sm = s

defvar vm = v

{ female survival rates:
defvar sf0 = s0

defvar sf = s

defvar vf = v 

{ subadult female fecundity 
defvar f1 = 7.0

{ adult female fecundity
defvar f2 = 7.0

{ primary female sex ratio: proportion of females at birth
defvar sigma = 0.5

{ reduction coefficient in the number of matings
defvar cc = 0.95

{ number of matings:

{ monogamous mating function
{defvar matmf = min(nm,nf)

{ monogamous mating function with reduction
defvar matmf = trunc(cc*min(nm,nf))

{ polygynous mating function: unrestricted harem size
{defvar matmf = if(nm,nf,0)

{ polygynous mating function: harem size of 2
{defvar matmf = min(2*nm,nf)

{ number of matings involving subadult females
defvar matmf1 = if(matmf,binomf(matmf,nf1/nf),0)

{ number of matings involving adult females
defvar matmf2 = matmf - matmf1

{ subadult females production
defvar pf1 = poissonf(matmf1,f1)

{ adult females production
defvar pf2 = poissonf(matmf2,f2)

{ number of females produced by subadult females
defvar pf1f = binomf(pf1,sigma)

{ number of males produced by subadult females
defvar pf1m = pf1 - pf1f

{ number of females produced by adult females
defvar pf2f = binomf(pf2,sigma)

{ number of males produced by subadult females
defvar pf2m = pf2 - pf2f



