{ Size-classified constant model for Killer whale

{ Reference:
{ Brault, S., and H. Caswell. 1993. Pod-specific demography
{ of killer whales (Orcinus orca). Ecology 74:1444-1454.

{ Example of commands:
{	property		( give demographic quantities
{				( growth rate, stable age distribution
{	sensitivity		( sensitivities of matrix entries
{
{ sensitivities to lower level demographic parameter:
{
{	sensitivity sigma1
{	sensitivity sigma2
{	sensitivity sigma3
{	sensitivity sigma4
{	sensitivity gamma2
{	sensitivity gamma3
{	sensitivity m

defmod kwhale_0g(4)
mat : a
vec : w

defvec w(4)
n1, n2, n3, n4

defmat a(4)
 0, f2, f3,  0
g1, p2,  0,  0
 0, g2, p3,  0
 0,  0, g3, p4

defvar n1 n2 n3 n4 = 10

defvar n = n1 + n2 +n3 + n4

defvar g1 = sig1

defvar g2 = gamma2*sigma2

defvar g3 = gamma3*sigma3

defvar p2 = (1-gamma2)*sigma2

defvar p3 = (1-gamma3)*sigma3

defvar p4 = sigma4

defvar f2 = sig1*g2*m/2

defvar f3 = sig1*(1+p3)*m/2

defvar sigma1 = 0.9554

defvar sig1 = sigma1^(1/2)

defvar sigma2 = 0.9847

defvar sigma3 = 0.9986

defvar sigma4 = 0.9804

defvar gamma2 = 0.0747

defvar gamma3 = 0.0453

defvar m = 0.1186
