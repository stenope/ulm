unit gabout;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Graphics, Controls, Forms, Dialogs,
  ExtCtrls, Buttons, StdCtrls;

type
  tform_about = class(TForm)
    AboutPanel: TPanel;
    Version: TLabel;
    label_copyright: TLabel;
    label_comments: TLabel;
    OKButton: TButton;
    label_contrib: TLabel;
    label_contrib1: TLabel;
    label_contrib2: TLabel;
    Image1: TImage;
    Image2: TImage;
    label_ULM: TLabel;
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var  form_about: tform_about;

implementation

uses jutil;

{$R *.lfm}

procedure tform_about.FormCreate(Sender: TObject);
begin
  Left   := 500;
  Top    := 50;
  Height := 386;
  Width  := 354;

  adjust(self);
  with aboutpanel do
    begin
      Font.Size := 8;
      Font.Style := [];
    end;
  with label_copyright do
    begin
      Font.Style := [fsBold];
      Caption := 'Copyright 2002 - Stéphane Legendre';
    end;
  with label_comments do
    begin
      Font.Style := [];
      Caption := 'Eco-evolutionary Team - Ecole Normale Supérieure - Paris';
    end;
  with label_contrib do
    begin
      Font.Style := [fsBold];
      Caption := 'Contribution';
    end;
  with label_contrib1 do
    begin
      Font.Style := [];
      Caption := 'Jean Clobert, Régis Ferrière, Frédéric Gosselin,';
    end;
  with label_contrib2 do
    begin
      Font.Style := [];
      Caption := 'Jean-Dominique Lebreton, François Sarrazin';
    end;
end;

end.
