unit gage;

{$MODE Delphi}

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     Grids,ExtCtrls,ComCtrls,
     jglobvar,jmath;

type
  Tform_age = class(TForm)
    StringGrid1: TStringGrid;
    label_matrix: TLabel;
    edit_mat: TEdit;
    label_lambda: TLabel;
    edit_lambda: TEdit;
    panel_x: TPanel;
    panel_a: TPanel;
    panel_b: TPanel;
    panel_c: TPanel;
    panel_w: TPanel;
    panel_l: TPanel;
    procedure init_age;
    procedure erreur(s : string);
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure eigenval(x : integer;var err : boolean);
    procedure proprietes_age;
    procedure edit_matReturnPressed(Sender: TObject);
  private
    lambda  : cvec_type; { valeurs propres complexes }
    lambda1 : extended;  { dominant eigenvalue }
    pi : rvec_type; { survies }
    gi : rvec_type; { recrutements }
    e_x,v_x : rvec_type; { time spent in stage }
    e_a,v_a : rvec_type; { age in stage mean,var }
    e_b,v_b : rvec_type; { age in stage at stable distribution mean,var }
    e_c,v_c : rvec_type; { age from first stage mean,var }
    e_w,v_w : rvec_type; { remaining life span mean,var }
    e_l,v_l : rvec_type; { total conditional life span mean,var }
  public
    x_mat : integer;
  end;

var form_age: Tform_age;

implementation

uses jutil,jsymb,jmatrix,jsyntax;

{$R *.lfm}

procedure tform_age.erreur(s : string);
begin
  erreur_('Age - ' + s);
end;

procedure Tform_age.FormCreate(Sender: TObject);
begin
  Left   := 110;
  Top    := 220;
  Height := 330;
  Width  := 846;
  adjust(self);
  Caption := 'AGE';
  x_mat := 0;
  with stringgrid1 do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := 13;
      RowCount  := 11;
      Cells[0,0] := 'Stage';
      Cells[1,0] := 'Mean';
      Cells[2,0] := 'Variance';
      Cells[3,0] := 'Mean';
      Cells[4,0] := 'Variance';
      Cells[5,0] := 'Mean';
      Cells[6,0] := 'Variance';
      Cells[7,0] := 'Mean';
      Cells[8,0] := 'Variance';
      Cells[9,0] := 'Mean';
      Cells[10,0] := 'Variance';
      Cells[11,0] := 'Mean';
      Cells[12,0] := 'Variance';
    end;
end;

procedure Tform_age.init_age;
var i : integer;
begin
  x_mat := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        exit;
      end;
end;

procedure tform_age.eigenval(x : integer;var err : boolean);
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then exit;
    end;
  err := false;
end;

procedure Tform_age.proprietes_age;
var i,j,k : integer;
    u : extended;
    e_z,v_z : rvec_type;
begin
  with mat[x_mat] do
    begin
      for i := 1 to size   do pi[i] := val[i,i];
      for i := 1 to size-1 do gi[i] := val[i+1,i];
      gi[size] := 1.0;
      for i := 1 to size do e_x[i] := 1.0/(1.0 - pi[i]);{ on suppose 0 <= p < 1 }
      for i := 1 to size do v_x[i] := pi[i]*sqr(e_x[i]);
      for i := 1 to size do
        begin
          e_a[i] := 0.0;
          v_a[i] := 0.0;
          for j := 1 to i do
            begin
              e_a[i] := e_a[i] + e_x[j];
              v_a[i] := v_a[i] + v_x[j];
            end;
        end;
      for i := 1 to size do e_z[i] := 1.0/(1.0 - pi[i]/lambda1);{ lambda1 > 0 }
      for i := 1 to size do v_z[i] := (pi[i]/lambda1)*sqr(e_z[i]);
      for i := 1 to size do
        begin
          e_b[i] := 0.0;
          v_b[i] := 0.0;
          for j := 1 to i do
            begin
              e_b[i] := e_b[i] + e_z[j];
              v_b[i] := v_b[i] + v_z[j];
            end;
        end;
      e_c[1] := 1.0;
      v_c[1] := 0.0;
      for i := 2 to size do
        begin
          e_c[i] := 1.0 + e_a[i-1];
          v_c[i] := v_a[i-1];
        end;
      for i := 1 to size do
        begin
          e_w[i] := 0.0;
          v_w[i] := 0.0;
          for j := i to size do
            begin
              u := 1.0;
              for k := i to j do u := u*gi[k]*e_x[k];
              u := u/gi[j]; { on suppose 0 < g <= 1 }
              e_w[i] := e_w[i] + u;
              v_w[i] := v_w[i] + ((1.0 + pi[j])/(1.0 - pi[j]))*u - sqr(u);
            end;
        end;
      for i := 1 to size do
        begin
          e_l[i] := e_w[i] + e_c[i];
          v_l[i] := v_w[i] + v_a[i];
        end;
    end;
end;

procedure Tform_age.FormActivate(Sender: TObject);
var i : integer;
    err : boolean;
begin
  init_age;
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      exit;
    end;
  with mat[x_mat] do
    begin
      edit_mat.Text := s_ecri_mat(x_mat);
      eigenval(x_mat,err);
      if err then exit;
      edit_lambda.Text := Format('%10.6g',[lambda1]);
      if not matleslie(size,val) and
         not matleslie2(size,val) and
         not matclassetaille(size,val) then
        begin
          erreur('Matrix is not size classified');
          exit;
        end;
      proprietes_age;
      with stringgrid1 do
        begin
          RowCount  := size + 1;
          for i := 1 to size do
            begin
              Cells[0,i] := IntToStr(i);
              Cells[1,i] := Format('%10.4f',[e_x[i]]);
              Cells[2,i] := Format('%10.4f',[v_x[i]]);
              Cells[3,i] := Format('%10.4f',[e_a[i]]);
              Cells[4,i] := Format('%10.4f',[v_a[i]]);
              Cells[5,i] := Format('%10.4f',[e_b[i]]);
              Cells[6,i] := Format('%10.4f',[v_b[i]]);
              Cells[7,i] := Format('%10.4f',[e_c[i]]);
              Cells[8,i] := Format('%10.4f',[v_c[i]]);
              Cells[9,i] := Format('%10.4f',[e_w[i]]);
              Cells[10,i] := Format('%10.4f',[v_w[i]]);
              Cells[11,i] := Format('%10.4f',[e_l[i]]);
              Cells[12,i] := Format('%10.4f',[v_l[i]]);
            end;
        end;
    end;
end;

procedure Tform_age.edit_matReturnPressed(Sender: TObject);
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx = type_mat )then
    begin
      erreur('unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

end.
