unit gconfint2;

{$MODE Delphi}

{ computation of confidence interval for growth rate lambda }
{ knowing sigma's of user defined demographic parameters }

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ExtCtrls,ComCtrls,Grids,
     jglobvar,jmath;

const maxdempar = 20;

type
  tform_confint2 = class(TForm)
    stringgrid_dempar: TStringGrid;
    label_dempar: TLabel;
    label_mat: TLabel;
    label_lambda: TLabel;
    Edit_mat: TEdit;
    label_sigma_lambda: TLabel;
    label_lambda_plus: TLabel;
    label_lambda_moins: TLabel;
    edit_z_alpha: TEdit;
    label_z_alpha: TLabel;
    label_confidence: TLabel;
    button_ok: TButton;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_confint2;
    procedure erreur(s : string);
    procedure Edit_matReturnPressed(Sender: TObject);
    procedure eigenval(x : integer; var err : boolean);
    procedure eigenvec(x : integer; var err : boolean);
    procedure confint2(m : integer);
    procedure sigma_lambda1(m : integer; var err : boolean);
    function  sens_x(m,x : integer; var err : boolean) : extended;
    procedure stringgrid_demparSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure edit_z_alphaReturnPressed(Sender: TObject);
    procedure button_okClick(Sender: TObject);
  private
    n : integer; { taille de la matrice }
    x_mat : integer;    { matrice courante }
    lambda : cvec_type; { valeurs propres complexes }
    v,w : rvec_type;    { vecteurs propres gauche, droite }
    lambda1 : extended; { dominant eigenvalue }
    mat_sens : rmat_type; { matrice des sensibilites }
    sigma_lambda : extended; { sigma de lambda }
    z_alpha : extended; { lambda +/- z_alpha*sigma_lambda }
    alpha : extended;  { % confidence interval }
    nb_dempar : integer; { nombre de parametres demographiques }
    dempar,dempar1 : array[0..maxdempar] of integer; { nom de ces parametres}
    dempar_val : array[0..maxdempar] of extended; { valeurs de ces parametres }
    dempar_sigma,dempar_sigma1 : array[0..maxdempar] of extended; { std dev de ces parametres }
  public
  end;

var form_confint2: tform_confint2;

implementation

uses jutil,jsymb,jmatrix,jsyntax,jeval;

{$R *.lfm}

procedure tform_confint2.erreur(s : string);
begin
  erreur_('Confidence Interval 2 - ' + s);
end;

procedure tform_confint2.FormCreate(Sender: TObject);
begin
  Left   := 200;
  Top    := 300;
  Height := 230;
  Width  := 800;
  adjust(self);
  Caption := 'CONFIDENCE INTERVAL 2';
  x_mat := 0;
  z_alpha := 1.0;
end;

procedure tform_confint2.eigenval(x : integer;var err : boolean);
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_confint2.eigenvec(x : integer; var err : boolean);
var i : integer;
    vv,ww : cvec_type;
begin
  err := true;
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
    end;
  err := false;
end;

function  tform_confint2.sens_x(m,x : integer; var err : boolean) : extended;
{ sensibilites de lambda par rapport a la variable x }
var i,j,y,ty : integer;
    som,a : extended;
begin
  err := true;
  with mat[m] do
    begin
      som := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            deriv(exp[i,j],exp_type[i,j],x,y,ty);
            if err_eval then
              begin
                err_eval := false;
                exit;
              end;
            a := eval(y,ty);
            som := som + mat_sens[i,j]*a;
          end;
      sens_x := som;
    end;
  err := false;
end;

procedure tform_confint2.sigma_lambda1(m : integer;var err : boolean);
var i,j,k,x : integer;
    h,a,sx : extended;
begin
  err := true;
  with mat[m] do
    begin
      h := vecpscal(size,v,w);
      if ( h = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/h;
      h := 0.0;
      for k := 1 to nb_dempar do
        begin
          x  := dempar[k];
          sx := sens_x(m,x,err);
          a  := dempar_sigma[k];
          h  := h + sqr(sx*a);
        end;
      if ( h > 0.0 ) then
        sigma_lambda := sqrt(h)
      else
        sigma_lambda := 0.0;
    end;
  err := false;
end;

procedure tform_confint2.confint2(m : integer);
var err : boolean;
begin
  n := mat[m].size;
  eigenval(m,err);
  if err then exit;
  eigenvec(m,err);
  if err then exit;
  label_lambda.Caption := 'lambda = ' + Format('%1.6g',[lambda1]);
  sigma_lambda1(m,err);
  if err then exit;
  label_sigma_lambda.caption := 'Sigma_lambda = ' + Format('%10.6g',[sigma_lambda]);
  label_lambda_plus.caption := 'Lambda + z*sigma = ' + Format('%10.6g',[lambda1 + z_alpha*sigma_lambda]);
  label_lambda_moins.caption := 'Lambda - z*sigma = ' + Format('%10.6g',[lambda1 - z_alpha*sigma_lambda]);
  alpha := 2.0*phinormal(z_alpha) - 1.0;
  label_confidence.caption := '---> with ' + Format('%1.2g',[alpha*100.0]) + '% confidence';
end;

procedure tform_confint2.FormActivate(Sender: TObject);
var i,k : integer;
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      exit;
    end;
  n := mat[x_mat].size;
  with stringgrid_dempar do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := 21;
      RowCount  := 4;
      for i := 1 to ColCount-1 do Cells[i,0] := IntToStr(i);
      Cells[0,1] := 'Name';
      Cells[0,2] := 'Value';
      Cells[0,3] := 'Sigma';
      for k := 1 to nb_dempar do
        begin
          Cells[k,1] := s_ecri_var(dempar[k]);
          Cells[k,2] := s_ecri_val(variable[dempar[k]].val);
          Cells[k,3] := s_ecri_val(dempar_sigma[k]);
        end;
      for k := nb_dempar+1 to maxdempar do
        begin
          Cells[k,1] := '';
          Cells[k,2] := '';
          Cells[k,3] := '';
        end;
    end;
  confint2(x_mat);
  edit_z_alpha.Text := FloatToStr(z_alpha);
end;

procedure Tform_confint2.init_confint2;
var k : integer;
begin
  init_form(form_confint2);
  x_mat := 0;
  z_alpha := 1.0;
  nb_dempar := 0;
  for k := 1 to maxdempar do dempar_sigma[k] := 0.0;
  for k := 1 to modele_nb do
    if ( modele[k].xmat <> 0 ) then
      begin
        x_mat := modele[k].xmat;
        n := mat[x_mat].size;
        exit;
      end;
end;

procedure Tform_confint2.Edit_matReturnPressed(Sender: TObject);
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Matrix: unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure tform_confint2.stringgrid_demparSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( acol > 0 ) and ( arow = 1 ) or ( arow = 3 );
end;

procedure tform_confint2.edit_z_alphaReturnPressed(Sender: TObject);
begin
  z_alpha := StrToFloat(edit_z_alpha.Text);
  FormActivate(nil);
end;

procedure tform_confint2.button_okClick(Sender: TObject);
var k,nb,x,tx : integer;
    err : boolean;
    s : string;
begin
  nb := 0;
  for k := 1 to maxdempar do
    begin
      s := stringgrid_dempar.Cells[k,1];
      if ( s <> '' )  then
        begin
          trouve_obj(s,x,tx);
          if ( tx = type_variable ) then
            begin
              nb := nb + 1;
              dempar1[nb] := x;
              dempar_sigma1[nb] := StrToFloat(stringgrid_dempar.Cells[k,3]);
            end
          else
            begin
              erreur('unknown variable name');
              exit;
            end;
        end;
    end;
  nb_dempar := nb;
  for k := 1 to nb_dempar do
    begin
      x := dempar1[k];
      dempar[k] := x;
      stringgrid_dempar.Cells[k,1] := s_ecri_var(x);
      dempar_sigma[k] := dempar_sigma1[k];
      stringgrid_dempar.Cells[k,2] := s_ecri_val(variable[x].val);
      stringgrid_dempar.Cells[k,3] := s_ecri_val(dempar_sigma[k]);
    end;
  for k := nb+1 to maxdempar do with stringgrid_dempar do
    begin
      Cells[k,1] := '';
      Cells[k,2] := '';
      Cells[k,3] := '';
    end;
  edit_mat.Text := s_ecri_mat(x_mat);
  confint2(x_mat);
end;

end.
