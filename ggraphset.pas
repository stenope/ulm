unit ggraphset;

{$MODE Delphi}

interface

uses
  SysUtils, Types, Classes, Variants, Graphics, Controls, Forms, Dialogs,
  StdCtrls, ExtCtrls,
  ggraph;

type
  Tform_graphset = class(TForm)
    colordialog: TColorDialog;
    panel1: TPanel;
    axis_box: TGroupBox;
    run_box: TGroupBox;
    x: TLabel;
    y1: TLabel;
    y2: TLabel;
    y3: TLabel;
    y4: TLabel;
    vargraph_x_edit: TEdit;
    vargraph_y1_edit: TEdit;
    vargraph_y2_edit: TEdit;
    vargraph_y3_edit: TEdit;
    vargraph_y4_edit: TEdit;
    xmin: TLabel;
    ymin: TLabel;
    xmax: TLabel;
    ymax: TLabel;
    xmin_edit: TEdit;
    ymin_edit: TEdit;
    xmax_edit: TEdit;
    ymax_edit: TEdit;
    GroupBox1: TGroupBox;
    ok_button: TButton;
    cancel_button: TButton;
    color_y1_button: TButton;
    color_y2_button: TButton;
    color_y3_button: TButton;
    color_y4_button: TButton;
    GroupBox2: TGroupBox;
    lineoff_check: TCheckBox;
    gplus_check: TCheckBox;
    distrib0_check: TCheckBox;
    bordoff_check: TCheckBox;
    black_and_white_check: TCheckBox;
    white_and_black_check: TCheckBox;
    d_distrib_edit: TEdit;
    d_distrib_label: TLabel;
    xscale_check: TCheckBox;
    yscale_check: TCheckBox;
    apply_button: TButton;
    skip_edit: TEdit;
    skip_label: TLabel;
    distrib_check: TCheckBox;
    carlo_box: TGroupBox;
    minmax_check: TCheckBox;
    sigma_check: TCheckBox;
    grid_check: TCheckBox;
    scatter_check: TCheckBox;
    regress_check: TCheckBox;
    procedure cancel_buttonClick(Sender: TObject);
    procedure ok_buttonClick(Sender: TObject);
    procedure apply_buttonClick(Sender: TObject);
    procedure color_y1_buttonClick(Sender: TObject);
    procedure color_y2_buttonClick(Sender: TObject);
    procedure color_y3_buttonClick(Sender: TObject);
    procedure color_y4_buttonClick(  Sender: TObject);
    procedure black_and_white_checkClick(Sender: TObject);
    procedure white_and_black_checkClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
  private
    err_graphset : boolean;
    procedure erreur(s : string);
  public
    fg : tform_graph;
  end;

var  form_graphset: tform_graphset;

implementation

uses jglobvar,jutil,jsymb,jsyntax;

{$R *.lfm}

procedure tform_graphset.erreur(s : string);
begin
  erreur_('Graph Settings - ' + s);
  err_graphset := true;
end;

procedure Tform_graphset.cancel_buttonClick(Sender: TObject);
begin
  Close;
end;

procedure Tform_graphset.ok_buttonClick(Sender: TObject);
begin
  apply_buttonClick(nil);
  if not err_graphset then Close;
end;

procedure Tform_graphset.apply_buttonClick(Sender: TObject);
var i,k : integer;
    vargraph_xa,nb_skipa : integer;
    vargraph_ya : array[1..4] of integer;
    d_distriba  : extended;
    xmina,xmaxa,ymina,ymaxa : extended;
    distriba,distrib0a : boolean;
    xscalea,yscalea : boolean;
begin
  err_graphset := false;
  with fg do
    begin
      if ( vargraph_x_edit.Text = '' ) then
        begin
          erreur('X-variable expected');
          exit;
        end
      else
        if not test_variable(vargraph_x_edit.Text,vargraph_xa) then
          begin
            erreur('X-variable unknown');
            exit;
          end;
      if ( vargraph_y1_edit.Text = '' ) then
        vargraph_ya[1] := 0
      else
        if not test_variable(vargraph_y1_edit.Text,vargraph_ya[1]) then
          begin
            erreur('Y1-variable unknown');
            exit;
          end;
      if ( vargraph_y2_edit.Text = '' ) then
        vargraph_ya[2] := 0
      else
        if not test_variable(vargraph_y2_edit.Text,vargraph_ya[2]) then
          begin
            erreur('Y2-variable unknown');
            exit;
          end;
      if ( vargraph_y3_edit.Text = '' ) then
        vargraph_ya[3] := 0
      else
        if not test_variable(vargraph_y3_edit.Text,vargraph_ya[3]) then
          begin
            erreur('Y3-variable unknown');
            exit;
          end;
      if ( vargraph_y4_edit.Text = '' ) then
        vargraph_ya[4] := 0
      else
        if not test_variable(vargraph_y4_edit.Text,vargraph_ya[4]) then
          begin
            erreur('Y4-variable unknown');
            exit;
          end;

      if ( distrib_check.State = cbChecked ) then
        if est_reel(d_distrib_edit.Text,d_distriba) then
          distriba := true
        else
          begin
            erreur('Delta_distrib: real value expected');
            exit;
          end
      else
        distriba := false;
      if ( distrib0_check.State = cbChecked ) then
        if est_reel(d_distrib_edit.Text,d_distriba) then
          distrib0a := true
        else
          begin
            erreur('Delta_distrib: real value expected');
            exit;
          end
      else
        distrib0a := false;

      if ( xscale_check.State = cbChecked ) then
        begin
          if not est_reel(xmin_edit.Text,xmina) then
            begin
              erreur('Xmin: real value expected');
              exit;
            end;
          if not est_reel(xmax_edit.Text,xmaxa) then
            begin
              erreur('Xmax: real value expected');
              exit;
            end;
          if ( xmina >= xmaxa ) then
            begin
              erreur('Xscale: Xmin >= Xmax');
              exit;
            end;
          xscalea := true;
        end
      else
        xscalea := false;
      if ( yscale_check.state = cbChecked ) then
        begin
          if not est_reel(ymin_edit.Text,ymina) then
            begin
              erreur('Ymin: real value expected');
              exit;
            end;
          if not est_reel(ymax_edit.Text,ymaxa) then
            begin
              erreur('Ymax: real value expected');
              exit;
            end;
          if ( ymina >= ymaxa ) then
            begin
              erreur('Yscale: Ymin >= Ymax');
              exit;
            end;
          yscalea := true;
        end
      else
        yscalea := false;

      for i := 1 to 3 do
        if ( vargraph_ya[i] = 0 ) and ( vargraph_ya[i+1] <> 0 ) then
          begin
            vargraph_ya[i] := vargraph_ya[i+1];
            vargraph_ya[i+1] := 0;
          end;
      k := 0;
      for i := 1 to 4 do
        if ( vargraph_ya[i] <> 0 ) then k := k+1;
      if k = 0 then
        begin
          erreur('Y-variable expected');
          exit;
        end;
      nb_vargraph_y := k;
      for i := 1 to maxvargraph do vargraph_y[i] := vargraph_ya[i];
      vargraph_x := vargraph_xa;

      vargraph_y1_edit.Text := s_ecri_var(vargraph_y[1]);
      if vargraph_y[2] <> 0 then
        vargraph_y2_edit.Text := s_ecri_var(vargraph_y[2])
      else
        vargraph_y2_edit.Text := '';
      if vargraph_y[3] <> 0 then
        vargraph_y3_edit.Text := s_ecri_var(vargraph_y[3])
      else
        vargraph_y3_edit.Text := '';
      if vargraph_y[4] <> 0 then
        vargraph_y4_edit.Text := s_ecri_var(vargraph_y[4])
      else
        vargraph_y4_edit.Text := '';
      vargraph_y_col[1] := color_y1_button.Color;
      if ( vargraph_y[2] <> 0 ) then
        vargraph_y_col[2] := color_y2_button.Color
      else
        color_y2_button.Color := clBackground;
      if ( vargraph_y[3] <> 0 ) then
        vargraph_y_col[3] := color_y3_button.Color
      else
        color_y3_button.Color := clBackground;
      if ( vargraph_y[4] <> 0 ) then
        vargraph_y_col[4] := color_y4_button.Color
      else
        color_y4_button.Color := clBackground;

      if not est_entier(skip_edit.Text,nb_skipa) then
        begin
          erreur('Skip: integer expected');
          exit;
        end;
      if ( nb_skipa < 0.0 ) then
        begin
          erreur('Skip: non negative integer expected');
          exit;
        end;

      distrib := distriba;
      if distrib then d_distrib := d_distriba;
      distrib0 := distrib0a;
      if distrib0 then d_distrib := d_distriba;

      xscale := xscalea;
      if xscale then
        begin
          xmin := xmina;
          xmax := xmaxa;
        end;
      yscale := yscalea;
      if yscale then
        begin
          ymin := ymina;
          ymax := ymaxa;
        end;

      line0 := lineoff_check.state = cbUnchecked;
      gplus := gplus_check.state = cbChecked;
      grid  := grid_check.State = cbChecked;
      bord  := bordoff_check.state = cbUnchecked;
      black_and_white := black_and_white_check.state = cbChecked;
      white_and_black := white_and_black_check.state = cbChecked;
      gminmax  := minmax_check.state = cbChecked;
      gsigma   := sigma_check.state = cbChecked;
      gscatter := scatter_check.state = cbChecked;
      gregress := regress_check.state = cbChecked;
      nb_skip := nb_skipa;
      status;
    end;
end;

procedure tform_graphset.FormCreate(Sender: TObject);
begin
  Left   := 372;
  Top    := 0;
  Height := 478;
  Width  := 514;
  adjust(self);
  {with colordialog,form_graph do
    for i := 0 to 15 do
      CustomColors.Append(IntToStr(colors[i])); }
end;

procedure Tform_graphset.FormActivate(Sender: TObject);
begin
  with fg do
    begin
      form_graphset.Caption := 'GRAPHIC SETTINGS <' + IntToStr(ifg) + '>';
      vargraph_x_edit.Text  := s_ecri_var(vargraph_x);
      vargraph_y1_edit.Text := s_ecri_var(vargraph_y[1]);
      if vargraph_y[2] <> 0 then
        vargraph_y2_edit.Text := s_ecri_var(vargraph_y[2])
      else
        vargraph_y2_edit.Text := '';
      if vargraph_y[3] <> 0 then
        vargraph_y3_edit.Text := s_ecri_var(vargraph_y[3])
      else
        vargraph_y3_edit.Text := '';
      if vargraph_y[4] <> 0 then
        vargraph_y4_edit.Text := s_ecri_var(vargraph_y[4])
      else
        vargraph_y4_edit.Text := '';
      color_y1_button.Color := vargraph_y_col[1];
      if vargraph_y[2] <> 0 then
        color_y2_button.Color := vargraph_y_col[2]
      else
        color_y2_button.Color := clBackground;
      if vargraph_y[3] <> 0 then
        color_y3_button.Color := vargraph_y_col[3]
      else
        color_y3_button.Color := clBackground;
      if vargraph_y[4] <> 0 then
        color_y4_button.Color := vargraph_y_col[4]
      else
        color_y4_button.Color := clBackground;
      xmin_edit.Text := Format('%1.2f',[xmin]);
      xmax_edit.Text := Format('%1.2f',[xmax]);
      ymin_edit.Text := Format('%1.2f',[ymin]);
      ymax_edit.Text := Format('%1.2f',[ymax]);
      StrToFloatDef(ymax_edit.Text,ymax);
      xscale_check.Checked := xscale;
      yscale_check.Checked := yscale;
      bordoff_check.Checked  := not bord;
      distrib_check.Checked  := distrib;
      distrib0_check.Checked := distrib0;
      d_distrib_edit.Text := Format('%1.2f',[d_distrib]);
      gplus_check.Checked := gplus;
      lineoff_check.Checked := not line0;
      grid_check.Checked := grid;
      black_and_white_check.Checked := black_and_white;
      white_and_black_check.Checked := white_and_black;
      minmax_check.Checked := gminmax;
      sigma_check.Checked  := gsigma;
      scatter_check.Checked := gscatter;
      regress_check.Checked := gregress;
      err_graphset := false;
      skip_edit.Text := IntToStr(nb_skip);
    end;
end;

procedure Tform_graphset.black_and_white_checkClick(Sender: TObject);
begin
  with white_and_black_check do
    if State = cbChecked then Checked := false; {State := cbUnchecked;}
end;

procedure Tform_graphset.white_and_black_checkClick(Sender: TObject);
begin
  with black_and_white_check do
    if State = cbChecked then Checked := false; {State := cbUnchecked;}
end;

procedure Tform_graphset.color_y1_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y1_button.Color := colordialog.Color;
end;

procedure Tform_graphset.color_y2_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y2_button.Color := colordialog.Color;
end;

procedure Tform_graphset.color_y3_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y3_button.Color := colordialog.Color;
end;

procedure Tform_graphset.color_y4_buttonClick(Sender: TObject);
begin
  colordialog.execute;
  color_y4_button.Color := colordialog.Color;
end;

end.
