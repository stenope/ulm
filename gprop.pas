unit gprop;

{$MODE Delphi}

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,StdCtrls,
     ExtCtrls,ComCtrls,Grids,
     jglobvar,jmath;

type
  Tform_prop = class(TForm)
    groupbox_prop: TGroupBox;
    Label_matrix: TLabel;
    Edit_matrix: TEdit;
    Label_nonneg: TLabel;
    Label_pos: TLabel;
    label_prim: TLabel;
    Label_irr: TLabel;
    Label_leslie: TLabel;
    Label_leslie2: TLabel;
    Label_multi: TLabel;
    Label_sizeclass: TLabel;
    Label_random: TLabel;
    Label_vecdep: TLabel;
    Label_timedep: TLabel;
    GroupBox_eigen: TGroupBox;
    StringGrid_eigenval: TStringGrid;
    StringGrid_eigenvec: TStringGrid;
    edit_lambda: TEdit;
    label_lambda: TLabel;
    groupbox_dem: TGroupBox;
    label_growth_rate: TLabel;
    label_r: TLabel;
    label_rho: TLabel;
    label_p: TLabel;
    label_r0: TLabel;
    label_t0: TLabel;
    label_tc: TLabel;
    label_tb: TLabel;
    label_s: TLabel;
    label_e: TLabel;
    label_h: TLabel;
    label_phi: TLabel;
    label_sigma2: TLabel;
    label_gamma: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure init_prop;
    procedure erreur(s : string);
    procedure proprietes(x : integer);
    procedure prop_gene(x : integer);
    procedure eigenval(x : integer;var err : boolean);
    procedure eigenvec(x : integer);
    procedure dem_leslie(x : integer);
    procedure dem_leslie2(x : integer);
    procedure dem_qq(x : integer);
    procedure entropy(x : integer;var err : boolean);
    procedure returntime(x : integer);
    procedure gentime(x : integer);
    procedure netreprorate(x : integer);
    procedure Edit_matrixReturnPressed(Sender: TObject);
  private
    lambda : cvec_type; { valeurs propres complexes }
    v,w : rvec_type;    { vecteurs propres gauche, droite }
    lambda1 : extended; { dominant eigenvalue }
    r : extended;       { intrinsic rate of natural increase }
    rho : extended;     { damping ratio }
    pp : extended;      { period }
    e0 : extended;      { life expectancy }
    r0 : extended;      { net reproductive rate }
    t0 : extended;      { R0 generation time }
    tc : extended;      { cohort generation time }
    tb : extended;      { mean generation length }
    h  : extended;      { entropy rate }
    fi : extended;      { phi Demetrius }
    sigma2 : extended;  { variance Demetrius }
    gamma  : extended;  { parametre Demetrius }
    s  : extended;      { demographic entropy }
    e  : extended;      { demographic uncertainty }
    leslie   : boolean; { indicateur matrice Leslie }
    leslie2  : boolean; { indicateur matrice extended Leslie }
    multi    : boolean; { indicateur matrice multisite }
    sizeclass : boolean; { indicateur matrice classe taille }
  public
    x_mat : integer;    { matrice }
    psite : integer;    { nombre de sites si multi }
    interp_ : boolean;  { indicateur ecriture interp }
  end;

var form_prop: Tform_prop;
    mp : rmat_type;  // markov matrices
    matf,mats,matn : rmat_type; // A = S + F, N = (I - S)^{-1}
    vp : rvec_type; // left eigenvector  mp

implementation

uses jutil,jsymb,jmatrix,jsyntax;

{$R *.lfm}

procedure tform_prop.erreur(s : string);
begin
  erreur_('Properties - ' + s);
end;

procedure tform_prop.dem_leslie(x : integer);
var i,j,ic : integer;
    f,p,phi,pp,ll : rvec_type;
    a,b,q,s1,s2,s3,kappa,maxp,maxv,suml,summ,hl,hm : extended;
begin
  with mat[x] do
    begin
      for j := 1 to size   do f[j] := val[1,j]; { fertilities }
      for i := 1 to size-1 do p[i] := val[i+1,i]; { survival probabilities }
      a := 1.0;
      q := 1.0;
      maxp := 0.0;
      maxv := 0.0;
      ic   := 0;
      for i := 1 to size do
        begin
          if ( i >= 2 ) then q := q*p[i-1];
          ll[i] := q;
          phi[i] := f[i]*q; { maternity functions }
          a := a/lambda1;
          pp[i] := phi[i]*a;
          if ( phi[i] > maxp ) then
            begin
              ic := i;
              maxp := phi[i];
              maxv := pp[i];
            end;
        end;
      e0 := 0.0;
      r0 := 0.0;
      tb := 0.0;
      tc := 0.0;
      s  := 0.0;
      e  := 0.0;
      for i := 1 to size do
        begin
          e0 := e0 + ll[i];    { life expectancy voir }
          r0 := r0 + phi[i];   { net reproductive rate }
          tc := tc + i*phi[i]; { cohort generation time }
          tb := tb + i*pp[i];  { mean generation length }
          s  := s + pp[i]*ln0(pp[i]); { demographic entropy }
          e  := e + pp[i]*ln0(phi[i]); { demographic uncertainty }
        end;
      tc := tc/r0;
      s := -s;
      h  := s/tb;
      fi := e/tb;
      s1 := 0.0;
      s2 := 0.0;
      s3 := 0.0;
      for i := 1 to size do
        begin
          q  := -i*fi + ln0(phi[i]);
          s1 := s1 + i*pp[i]*q;
          s2 := s2 + pp[i]*q*q;
          s3 := s3 + pp[i]*q*q*q;
        end;
      sigma2 := s2/tb;
      kappa  := s3 - 3.0*s3*s1/tb + 2.0*s2;
      gamma  := kappa + 2.0*sigma2;
      //label_e0.Caption := 'Life expectancy = ' + Format('%1.4g',[e0]);
      if ( r <> 0.0 ) then t0 := ln(r0)/r else t0 := 0.0; { T0 }
      label_r0.Caption := 'Net reproductive rate R0 = ' + Format('%1.4g',[r0]);
      if interp_ then iwriteln(label_r0.Caption);
      label_t0.Caption := 'R0 generation time T0 = ' + Format('%1.4g',[t0]);
      if interp_ then iwriteln(label_t0.Caption);
      label_tc.Caption := 'Cohort generation time Tc = ' + Format('%1.4g',[tc]);
      if interp_ then iwriteln(label_tc.Caption);
      label_tb.Caption := 'Mean generation length T = ' + Format('%1.4g',[tb]);
      if interp_ then iwriteln(label_tb.Caption);
      label_s.Caption := ' Demographic entropy S = ' + Format('%1.4g',[s]);
      if interp_ then iwriteln(label_s.Caption);
      label_e.Caption := ' Demographic uncertainty E = ' + Format('%1.4g',[e]);
      if interp_ then iwriteln(label_e.Caption);
      label_h.Caption := ' Entropy rate H = ' + Format('%1.4g',[h]);
      if interp_ then iwriteln(label_h.Caption);
      label_phi.Caption := ' Reproductive potential Phi = ' + Format('%1.4g',[fi]);
      if interp_ then iwriteln(label_phi.Caption);
      label_sigma2.Caption := ' Entropic variance Sigma2 = ' + Format('%1.4g',[sigma2]);
      if interp_ then iwriteln(label_sigma2.Caption);
      label_gamma.Caption := ' Correlation index Gamma = ' + Format('%1.4g',[gamma]);
      if interp_ then iwriteln(label_gamma.Caption);
      if interp_ then
          iwriteln(' Age at max progeny = ' + IntToStr(ic) +
                   ', MaxP = ' + Format('%1.4g',[maxp]) +
                   ', MaxV = ' + Format('%1.4g',[maxv]));
      {suml := 0.0;
      summ := 0.0;
      hl := 0.0;
      for i := 1 to size do
        begin
          suml := suml + ll[i];
          summ := summ + f[i];
          hl := hl + ll[i]*ln0(ll[i]);
        end;
      if ( suml > 0.0 ) then hl := -hl/suml;
      hm := 0.0;
      if ( summ > 0.0 ) then
        for i := 1 to size do
          begin
            b := f[i]/summ;
            hm := hm + b*ln0(b);
          end;
      hm := -hm;
      if interp_ then
        begin
          iwriteln('Life expectation = ' + Format('%1.4g',[suml]));
          iwriteln('Entropy rate survivorship Hl = ' + Format('%1.4g',[hl]));
          iwriteln('Entropy rate fecundity Hm = ' + Format('%1.4g',[hm]));
        end;}
    end;
end;

procedure tform_prop.dem_leslie2(x : integer);
{ on suppose survie adulte 0 < a(n,n) < 1 }
const eps = 0.00001;
var i,j,n : integer;
    f,p,phi,pp,ll : rvec_type;
    a,b,d,q,s1,s2,s3,kappa,d2,d3,suml,summ,hl,hm : extended;
begin
  with mat[x] do
    begin
      n := size;
      for j := 1 to n   do f[j] := val[1,j]; { fertilities }
      for i := 1 to n-1 do p[i] := val[i+1,i];
      p[n] := val[n,n]; { survival probabilities }
      a := 1.0;
      q := 1.0;
      for i := 1 to n do
        begin
          if ( i >= 2 ) then q := q*p[i-1];
          ll[i] := q;
          phi[i] := f[i]*q; { net fertility functions }
          a := a/lambda1;
          pp[i] := phi[i]*a;
        end;
      e0 := 0.0;
      r0 := 0.0;
      tc := 0.0;
      tb := 0.0;
      s  := 0.0;
      e  := 0.0;
      for i := 1 to n-1 do
        begin
          e0 := e0 + ll[i];    { life expectancy voir pre post breeding census }
          r0 := r0 + phi[i];   { net reproductive rate }
          tc := tc + i*phi[i]; { cohort generation time }
          tb := tb + i*pp[i];  { mean generation length }
          s  := s + pp[i]*ln0(pp[i]);  { demographic entropy }
          e  := e + pp[i]*ln0(phi[i]); { demographic uncertainty }
        end;
      e0 := e0 + ll[n]/(1.0 - p[n]);
      r0 := r0 + phi[n]/(1.0 - p[n]); { net reproductive rate }
      tc := tc + phi[n]*(n - (n-1)*p[n])/sqr(1 - p[n]);
      tc := tc/r0; { mean age of the mothers }
      q  := p[n]/lambda1;
      {if ( lambda1 > p[n] ) then }
        begin
          tb := tb + pp[n]*(n - (n-1)*q)/sqr(1.0 - q);  { mean generation length }
          s  := s + pp[n]*(ln0(pp[n])/(1.0 - q) + q*ln0(q)/sqr(1.0 - q));
          e  := e + pp[n]*(ln0(phi[n])/(1.0 - q) + q*ln0(p[n])/sqr(1.0 - q));
        end;
      s  := -s;
      h  := s/tb;
      fi := e/tb;
      s1 := 0.0;
      s2 := 0.0;
      s3 := 0.0;
      for i := 1 to n do
        begin
          q := -i*fi + ln0(phi[i]);
          s1 := s1 + i*pp[i]*q;
          s2 := s2 + pp[i]*q*q;
          s3 := s3 + pp[i]*q*q*q;
        end;
      a := -n*fi + ln0(phi[n]);
      b := pp[n];
      i := 0;
      repeat
        i := i + 1;
        q := a + i*ln0(p[n]- fi);
        b := b*p[n]/lambda1;
        s1 := s1 + (n + i)*b*q;
        d2 := s2;
        s2 := s2 + b*q*q;
        d3 := s3;
        s3 := s3 + b*q*q*q;
        d := max(abs(s2 - d2),abs(s3 - d3));
      until ( i > 1000 ) or ( d < eps );
      sigma2 := s2/tb;
      kappa  := s3 - 3.0*s3*s1/tb + 2.0*s2;
      gamma  := kappa + 2.0*sigma2;
      //label_e0.Caption := 'Life expectancy = ' + Format('%1.4g',[e0]);
      if ( r <> 0.0 ) then t0 := ln(r0)/r else t0 := 0.0;
      label_r0.Caption := 'Net reproductive rate R0 = ' + Format('%1.4g',[r0]);
      if interp_ then iwriteln(label_r0.Caption);
      label_t0.Caption := 'R0 generation time T0 = ' + Format('%1.4g',[t0]);
      if interp_ then iwriteln(label_t0.Caption);
      label_tc.Caption := 'Cohort generation time Tc = ' + Format('%1.4g',[tc]);
      if interp_ then iwriteln(label_tc.Caption);
      label_tb.Caption := 'Mean generation length T = ' + Format('%1.4g',[tb]);
      if interp_ then iwriteln(label_tb.Caption);
      label_s.Caption := ' Demographic entropy S = ' + Format('%1.4g',[s]);
      if interp_ then iwriteln(label_s.Caption);
      label_e.Caption := ' Demographic uncertainty E = ' + Format('%1.4g',[e]);
      if interp_ then iwriteln(label_e.Caption);
      label_h.Caption := ' Entropy rate H = ' + Format('%1.4g',[h]);
      if interp_ then iwriteln(label_h.Caption);
      label_phi.Caption := ' Reproductive potential Phi = ' + Format('%1.4g',[fi]);
      if interp_ then iwriteln(label_phi.Caption);
      label_sigma2.Caption := ' Entropic variance Sigma2 = ' + Format('%1.4g',[sigma2]);
      if interp_ then iwriteln(label_sigma2.Caption);
      label_gamma.Caption := ' Correlation index Gamma = ' + Format('%1.4g',[gamma]);
      if interp_ then iwriteln(label_gamma.Caption);
      {suml := 0.0;
      summ := 0.0;
      hl := 0.0;
      for i := 1 to size do
        begin
          suml := suml + ll[i];
          summ := summ + f[i];
          hl := hl + ll[i]*ln0(ll[i]);
        end;
      hm := 0.0;
      if ( summ > 0.0 ) then
        for i := 1 to size do
          begin
            b := f[i]/summ;
            hm := hm + b*ln0(b);
          end;
      hm := -hm;
      a := ll[n];
      i := 0;
      repeat
        i := i + 1;
        a := a*p[n];
        suml := suml + a;
        d2 := hl;
        hl := hl + a*ln0(a);
        d := abs(hl - d2);
      until ( i > 1000 ) or ( d < eps );
      if ( suml > 0.0 ) then hl := -hl/suml;
      if interp_ then
        begin
          iwriteln('Life expectation = ' + Format('%1.4g',[suml]));
          iwriteln('Entropy rate survivorship Hl = ' + Format('%1.4g',[hl]));
          iwriteln('Entropy rate fecundity Hm = ' + Format('%1.4g',[hm]));
        end;}
    end;
end;

{procedure entropy_gentime(n : integer; mat : rmat_type);
// Inutilisee
var i,j,k,l,a,b,nq : integer;
    lambda1,hi,tbi,h,sum : extended;
    w : rvec_type;
    mq : rmat_type;
begin
  matvalvecd1(n,mat,w,lambda1);
  if ( lambda1 <= 0.0 ) then
    begin
      iwriteln('Entropy: lambda  <= 0 ');
      exit;
    end;
  for i := 1 to n do
    if ( w[i] = 0.0 ) then
      begin
        iwriteln('Entropy: reducible matrix');
        exit;
      end;
  for i := 1 to n do
    for j := 1 to n do
      mp[i,j] := mat[i,j]*w[j]/(lambda1*w[i]);
  matkovvecg(n,mp,vp);
  h := 0.0;
  for i := 1 to n do
    begin
      hi := 0.0;
      for j := 1 to n do hi := hi + mp[i,j]*ln0(mp[i,j]);
      h := h + vp[i]*hi;
    end;
  h := -h;
  // temps moyen de premier retour sur les arcs reproducteurs
  // on calcule une matrice de Markov sur les arcs a = i->j du graphe d'origine
  a := 0;
  nq := 0;
  for i := 1 to n do
    for j := 1 to n do
      if ( mp[i,j] > 0.0 ) then
        begin
          a := a + 1;
          nq := nq + 1;
          b := 0;
          for k := 1 to n do
            for l := 1 to n do
              if ( mp[k,l] > 0.0 ) then
                begin
                  b := b + 1;
                  if ( j = k ) then
                    mq[a,b] := mp[j,l]
                  else
                    mq[a,b] := 0.0;
                end;
        end;
end;}

procedure tform_prop.returntime(x : integer);
var i,j,a,n : integer;
    vq : rvec_type;
begin
  with mat[x] do
    begin
      n := size;
      for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]);
      matkovvecg(n,mp,vp);
      iwriteln('Computation of return times:');
      a := 0;
      for i := 1 to n do
        for j := 1 to n do
          if ( mp[i,j] > 0.0 ) then
            begin
              a := a + 1;
              vq[a] := mp[i,j]*vp[i];
              if ( vq[a] = 0.0 ) then
                iwriteln(IntToStr(a) + ' i = ' + IntToStr(i) +
                         ' vp = ' + FloatToStr(vp[i]))
              else
                iwriteln('   ' + IntToStr(a) + '   ' +
                         IntToStr(j) + '->' + IntToStr(i) +
                         '    Ta'  + IntToStr(a) + ' = ' + s_ecri_val(1.0/vq[a]) + ' ' +
                         '   1/Ta' + IntToStr(a) + ' = ' + s_ecri_val(vq[a]));
            end;
      iwriteln('Return time TZ to set of transitions Z is given by 1/TZ = 1/Ta + 1/Tb + ... with a, b, ... in Z');
    end;
end;

procedure  tform_prop.gentime(x : integer);
var i,j,a,n : integer;
    sum : extended;
begin
  with mat[x] do
    begin
      n := size;
      // deja calcule dans entropy
      {for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]);
      matkovvecg(n,mp,vp);}
      sum := 0.0;
      for a := 1 to repro_nb do
        sum := sum + mp[repro_i[a],repro_j[a]]*vp[repro_i[a]];
      if ( sum > 0.0 ) then
        tb := 1.0/sum
      else
        tb := 0.0;
      label_tb.Caption := 'Mean generation length T = ' + Format('%1.4g',[tb]);
      if interp_ then iwriteln(label_tb.Caption);
    end;
end;

(*procedure  tform_prop.gentime(x : integer);
// formule T = lambda*vw/vFw
var i,j,a,n : integer;
    sum,pscal : extended;
begin
  with mat[x] do
    begin
      n := size;
      { deja fait dans netreprorate
      // A = S + F
      for i := 1 to n do
        for j := 1 to n do
          begin
            mats[i,j] := val[i,j];
            matf[i,j] := 0.0;
          end;
      for a := 1 to repro_nb do
        begin
          matf[repro_i[a],repro_j[a]] := val[repro_i[a],repro_j[a]];
          mats[repro_i[a],repro_j[a]] := 0.0;
        end;}
      pscal := 0.0;
      for i := 1 to n do pscal := pscal + v[i]*w[i];
      for i := 1 to n do
        begin
          sum := 0.0;
          for j := 1 to n do sum := sum + matf[i,j]*w[j];
          vp[i] := sum;
        end;
      sum := 0;
      for j := 1 to n do sum := sum + v[j]*vp[j];
      if ( sum > 0.0 ) then
        tb := lambda1*pscal/sum
      else
        tb := 0.0;
      label_tb.Caption := 'Mean generation length T = ' + Format('%1.4g',[tb]);
      if interp_ then iwriteln(label_tb.Caption);
    end;
end;*)

procedure tform_prop.netreprorate(x : integer);
// calcul net reproductive rate R0
// valeur propre dominante de FN
const eps = 1.0E-12;
var i,j,k,a,n : integer;
    sum,norm,norm1 : extended;
    ma,mb : rmat_type;
    lamb : cvec_type;
begin
  with mat[x] do
    begin
      n := size;
      // A = S + F
      for i := 1 to n do
        for j := 1 to n do
          begin
            mats[i,j] := val[i,j];
            matf[i,j] := 0.0;
          end;
      for a := 1 to repro_nb do
        begin
          matf[repro_i[a],repro_j[a]] := val[repro_i[a],repro_j[a]];
          mats[repro_i[a],repro_j[a]] := 0.0;
        end;
      // calcul N = I + S + S^2 + ... = (I - S)^{-1}
      for i := 1 to n do
        for j := 1 to n do
          if ( i = j ) then
            matn[i,j] := 1.0
          else
            matn[i,j] := 0.0; // N = I
      for i := 1 to n do
        for j := 1 to n do
          ma[i,j] := matn[i,j]; // ma = I
      norm := 1.0;
      a := 0;
      repeat
        a := a + 1;
        for i := 1 to n do
          for j := 1 to n do
            begin
              sum := 0.0;
              for k := 1 to n do sum := sum + ma[i,k]*mats[k,j];
              mb[i,j] := sum;
            end;
        for i := 1 to n do
          for j := 1 to n do
            ma[i,j] := mb[i,j]; // ma = S^k
        norm1 := norm;
        norm := 0.0;
        for i := 1 to n do
          for j := 1 to n do
            norm := max(norm,abs(ma[i,j]));
        for i := 1 to n do
          for j := 1 to n do
            matn[i,j] := matn[i,j] + ma[i,j]; // N = I + S + S^2 + ...
      until ( a >= 10000 ) or ( norm < eps );
      if ( norm1 = norm ) then // pb convergence }
        begin
          iwriteln('R0 - convergence');
          exit;
        end;
      for i := 1 to n do
        for j := 1 to n do
          begin
            sum := 0.0;
            for k := 1 to n do sum := sum + matf[i,k]*matn[k,j];
            mb[i,j] := sum; // mb = FN
          end;
      matvalprop(n,mb,lamb);
      if err_math then
        begin
          erreur('R0 - Error in eigenvalue computation FN');
          err_math := false;
          exit;
        end;
      if ( lamb[1].im <> 0.0 ) then
        begin
          erreur('R0 - No real dominant eigenvalue found');
          exit;
        end;
      r0 := lamb[1].re;
      if ( r0 <= 0.0 ) then
        begin
          r0 := 0.0;
          exit;
        end;
      label_r0.Caption := 'Net reproductive rate R0 = ' + Format('%1.4g',[r0]);
      if interp_ then iwriteln(label_r0.Caption);
      if ( r <> 0.0 ) then t0 := ln(r0)/r else t0 := 0.0; // T0
      label_t0.Caption := 'R0 generation time T0 = ' + Format('%1.4g',[t0]);
      if interp_ then iwriteln(label_t0.Caption);
    end;
end;

procedure tform_prop.entropy(x : integer;var err : boolean);
{ compute entropy rate H }
var n,i,j : integer;
    hi : extended;
begin
  with mat[x] do
    begin
      n := size;
      for i := 1 to n do
        if ( w[i] = 0.0 ) then // matrice reductible
          begin
            err := true;
            iwriteln('matrix is reducible');
            exit;
          end;
      for i := 1 to n do
        for j := 1 to n do
          mp[i,j] := val[i,j]*w[j]/(lambda1*w[i]);
      matkovvecg(n,mp,vp);
      h := 0.0;
      for i := 1 to n do
        begin
          hi := 0.0;
          for j := 1 to n do hi := hi + mp[i,j]*ln0(mp[i,j]);
          h := h + vp[i]*hi;
        end;
      h := -h;
      label_h.Caption := ' Entropy rate H = ' + Format('%1.4g',[h]);
      if interp_ then iwriteln(label_h.Caption);
    end;
end;

procedure tform_prop.dem_qq(x : integer);
var err : boolean;
begin
  with mat[x] do
    begin
      label_r0.Caption := '';
      label_t0.Caption := '';
      label_tc.Caption := '';
      label_tb.Caption := '';
      label_s.Caption  := '';
      label_e.Caption  := '';
      err := false;
      entropy(x,err);
      if err then label_h.Caption := '';
      label_phi.Caption := '';
      label_sigma2.Caption := '';
      label_gamma.Caption  := '';
      if ( repro_nb = 0 ) then exit;
      netreprorate(x);
      gentime(x);
    end;
end;

procedure tform_prop.prop_gene(x : integer);
var  bool : boolean;

function s_ecri_bool(b : boolean) : string;
begin
  if b then s_ecri_bool := 'YES' else s_ecri_bool := 'NO';
end;

begin
  with mat[x] do
    begin
      edit_matrix.Text := s_ecri_mat(x_mat);
      if interp_ then iwriteln('Matrix ' + edit_matrix.Text);
      label_timedep.Caption := 'Time dependent -> ' + s_ecri_bool(mattimedep(x));
      if interp_ then iwriteln(label_timedep.Caption);
      label_vecdep.Caption  := 'Vector dependent -> ' + s_ecri_bool(matvecdep(x,modele[xmodele].xvec));
      if interp_ then iwriteln(label_vecdep.Caption);
      label_random.Caption  := 'Random -> ' + s_ecri_bool(matrandom(x));
      if interp_ then iwriteln(label_random.Caption);
      bool := matnonneg(size,val);
      label_nonneg.Caption := 'Non negative -> ' + s_ecri_bool(bool);
      if interp_ then iwriteln(label_nonneg.Caption);
      if bool then
        begin
          label_pos.Caption  := 'Positive -> ' + s_ecri_bool(matpos(size,val));
          if interp_ then iwriteln(label_pos.Caption);
          label_prim.Caption := 'Primitive -> ' + s_ecri_bool(matprim(size,val));
          if interp_ then iwriteln(label_prim.Caption);
          label_irr.Caption  := 'Irreducible -> ' + s_ecri_bool(matirr(size,val));
          if interp_ then iwriteln(label_irr.Caption);
          leslie := matleslie(size,val);
          label_leslie.Caption := 'Leslie -> ' + s_ecri_bool(leslie);
          if interp_ then iwriteln(label_leslie.Caption);
          leslie2 := matleslie2(size,val);
          label_leslie2.Caption := 'Extended Leslie -> ' + s_ecri_bool(leslie2);
          if interp_ then iwriteln(label_leslie2.Caption);
          sizeclass := matclassetaille(size,val);
          label_sizeclass.Caption := 'Size classified -> ' + s_ecri_bool(sizeclass);
          if interp_ then iwriteln(label_sizeclass.Caption);
          multi := matmultisite(size,val,psite);
          if multi then
            label_multi.Caption := 'Multisite -> ' + s_ecri_bool(multi)+
                                   ' [' + IntToStr(psite) + ' sites]'
          else
            label_multi.Caption := 'Multisite -> ' + s_ecri_bool(multi);
            if interp_ then iwriteln(label_multi.Caption);
         end
      else
        begin
          label_pos.Caption := '';
          label_prim.Caption := '';
          label_irr.Caption  := '';
          label_leslie.Caption := '';
          label_leslie2.Caption := '';
          label_sizeclass.Caption := '';
          label_multi.Caption := '';
        end;
    end;
end;

procedure tform_prop.eigenval(x : integer;var err : boolean);
var i : integer;
    z : cmx;
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
    end;
  if ( lambda[1].im <> 0.0 ) then
    begin
      erreur('No real dominant eigenvalue found');
      exit;
    end;
  lambda1 := lambda[1].re;
  if ( lambda1 <= 0.0 ) then exit;
  if ( cmxmod(lambda[2]) <> 0.0 ) then
    rho := lambda1/cmxmod(lambda[2])
  else
    rho := 0.0;
  if ( cmxarg(lambda[2]) <> 0.0 ) then
    pp := dpi/cmxarg(lambda[2])
  else
    pp := 0.0;
  r := ln0(lambda1);
  edit_lambda.Text := Format('%10.6g',[lambda1]);
  with mat[x],stringgrid_eigenval do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := 5;
      RowCount  := size + 1;
      Cells[1,0] := 'Modulus';
      Cells[2,0] := 'Re';
      Cells[3,0] := 'Im';
      Cells[4,0] := 'Arg °';
      if interp_ then iwriteln('     Modulus     | Re        | Im         | Arg°');
      for i := 1 to size do
        begin
          z := lambda[i];
          Cells[0,i] := Format('%3d',[i]);
          Cells[1,i] := Format('%10.6f',[cmxmod(z)]);
          Cells[2,i] := Format('%10.4f',[z.re]);
          Cells[3,i] := Format('%10.4f',[z.im]);
          Cells[4,i] := Format('%10.1f',[cmxarg(z)*360.0/dpi]);
          if interp_ then
            iwriteln(Cells[0,i] + Cells[1,i] + Cells[2,i] + Cells[3,i] + Cells[4,i]);
        end;
    end;
  label_growth_rate.Caption := 'Growth rate lambda = ' + Format('%1.6g',[lambda1]);
  if interp_ then iwriteln(label_growth_rate.Caption);
  label_r.Caption := 'Intrinsic rate of increase r = ' + Format('%1.4g',[r]);
  if interp_ then iwriteln(label_r.Caption);
  label_rho.Caption := 'Damping ratio rho = ' + Format('%1.4g',[rho]);
  if interp_ then iwriteln(label_rho.Caption);
  label_p.Caption := 'Period P = ' + Format('%1.4g',[pp]) + ' (Rad)';
  if interp_ then iwriteln(label_p.Caption);
  err := false;
end;

procedure tform_prop.eigenvec(x : integer);
var i : integer;
    vv,ww : cvec_type;
begin
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
      with stringgrid_eigenvec do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := 3;
          RowCount  := size + 1;
          Cells[1,0] := 'Rep. Value';
          Cells[2,0] := 'Stable Dist.';
          if interp_ then iwriteln('     Reproductive value | Stable distribution');
          for i := 1 to size do
            begin
              Cells[0,i] := Format('%3d',[i]);
              Cells[1,i] := Format('%10.4f',[v[i]]);
              Cells[2,i] := Format('%10.4f',[w[i]]);
              if interp_ then
                iwriteln(Cells[0,i]+ '     ' + Cells[1,i] + '         ' + Cells[2,i]);
            end;
        end;
    end;
end;

procedure Tform_prop.FormActivate(Sender: TObject);
begin
  proprietes(x_mat);
end;

procedure Tform_prop.proprietes(x : integer);
var err : boolean;
begin
  if ( x = 0 ) then
    begin
      erreur('No matrix-type model');
      exit;
    end;
  prop_gene(x);
  eigenval(x,err);
  if err then exit;
  eigenvec(x);
  if leslie then
    dem_leslie(x)
  else
    if leslie2 then
      dem_leslie2(x)
    else
      dem_qq(x);
  if interp_ then returntime(x);
end;

procedure Tform_prop.FormCreate(Sender: TObject);
begin
  Left   := 20;
  Top    := 0;
  Height := 332;
  Width  := 1000;
  adjust(self);
  Caption := 'MATRIX PROPERTIES';
  x_mat := 0;
end;

procedure Tform_prop.init_prop;
var i : integer;
begin
  init_form(form_prop);
  x_mat := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        exit;
      end;
end;

procedure Tform_prop.Edit_matrixReturnPressed(Sender: TObject);
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_matrix.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('unknown matrix name');
      edit_matrix.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

end.
