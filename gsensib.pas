unit gsensib;

{$MODE Delphi}

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ExtCtrls,ComCtrls,Grids,
     jglobvar,jmath;

type
  tform_sensib = class(TForm)
    StringGrid_sens: TStringGrid;
    StringGrid_elas: TStringGrid;
    Label_sens: TLabel;
    Label_elas: TLabel;
    Label_sens_x: TLabel;
    Label_mat: TLabel;
    Label_lambda: TLabel;
    Edit_sens_x: TEdit;
    Label_sens_x_tot: TLabel;
    Label_elas_x_tot: TLabel;
    Edit_sens_x_tot: TEdit;
    Edit_elas_x_tot: TEdit;
    Edit_mat: TEdit;
    Edit_lambda: TEdit;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_sensib;
    procedure erreur(s : string);
    procedure Edit_sens_xReturnPressed(Sender: TObject);
    procedure Edit_matReturnPressed(Sender: TObject);
    procedure eigenval(x : integer; var err : boolean);
    procedure eigenvec(x : integer; var err : boolean);
    procedure sensibilites(m,x : integer);
    procedure sensibilites_interp(m,x : integer);
    procedure sens_lambda1(m : integer; var err : boolean);
    procedure sens_lambda1_x(m,x : integer; var err : boolean);
    procedure dynsens_x(m,x : integer; nb_cycle : integer; var err : boolean; var dn : rvec_type);
    procedure dynsens_interp(m,x : integer);
    procedure dynsens(m : integer; nb_cycle : integer;var err : boolean; var dn : rvec3_type);
  private
    x_mat : integer;    { matrice courante }
    x_var : integer;    { variable de sensibilite }
    lambda : cvec_type; { valeurs propres complexes }
    v,w : rvec_type;    { vecteurs propres gauche, droite }
    lambda1 : extended; { dominant eigenvalue }
    mat_sens : rmat_type; { matrice des sensibilites }
    mat_elas : rmat_type; { matrice des elasticites }
    sens_x_tot,elas_x_tot : extended; { sensibilite, elasticite totale }
    var_lambda,cv_lambda : extended; {variance et coeff de variation de lambda }
  public
  end;

var form_sensib: tform_sensib;

implementation

uses jutil,jsymb,jmatrix,jsyntax,jeval;

{$R *.lfm}

procedure tform_sensib.erreur(s : string);
begin
  erreur_('Sensitivities - ' + s);
end;

procedure tform_sensib.FormCreate(Sender: TObject);
begin
  Left   := 140;
  Top    := 360;
  Height := 294;
  Width  := 804;
  adjust(self);
  Caption := 'SENSITIVITIES';
  x_mat := 0;
  x_var := 0;
end;

procedure tform_sensib.eigenval(x : integer;var err : boolean);
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_sensib.eigenvec(x : integer; var err : boolean);
var i : integer;
    vv,ww : cvec_type;
begin
  err := true;
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
    end;
  err := false;
end;

var mvar : rmat_type;

procedure tform_sensib.sens_lambda1(m : integer;var err : boolean);
var i,j : integer;
    r : extended;
begin
  err := true;
  with mat[m] do
    begin
      r := vecpscal(size,v,w);
      if ( r = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/r;
      for i := 1 to size do
        for j := 1 to size do
          mat_elas[i,j] := val[i,j]*mat_sens[i,j]/lambda1;
      r := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          if ( val[i,j] > 0.0 ) then
            r := r + sqr(mat_sens[i,j]);
      var_lambda := r;
      r  := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          r := r + sqr(mat_elas[i,j]);
      cv_lambda  := sqrt(r);
    end;
  err := false;
end;

procedure tform_sensib.sens_lambda1_x(m,x : integer; var err : boolean);
{ sensibilites de la matrice m par rapport a la variable x }
var i,j,y,ty : integer;
    r,som : extended;
begin
  err := true;
  with mat[m] do
    begin
      r := vecpscal(size,v,w);
      if ( r = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/r;
      som := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            deriv(exp[i,j],exp_type[i,j],x,y,ty);
            if err_eval then
              begin
                err_eval := false;
                exit;
              end;
            r := eval(y,ty);
            mat_sens[i,j] := mat_sens[i,j]*r;
            mat_elas[i,j] := variable[x].val*mat_sens[i,j]/lambda1;
            som := som + mat_sens[i,j];
          end;
      sens_x_tot := som;
      elas_x_tot := som*variable[x].val/lambda1;
    end;
  err := false;
end;

procedure tform_sensib.dynsens_x(m,x : integer; nb_cycle : integer;
                                 var err : boolean; var dn : rvec_type);
{ sensibilites dynamiques du vecteur de population }
{ de la matrice m par rapport a la variable x }
{ calculees sur nb_cyle pas de temps }
{ condition initiale = 0 }
{ retourne n relations, une pour chaque entree du vecteur de population }
var i,k,icyc,y,ty : integer;
    r,sum : extended;
    dna : rvec_type;
    s : string;
begin
  err := true;
  with mat[m] do
    begin
      for i := 1 to size do dn[i] := 0.0;
      for icyc := 1 to nb_cycle do
        begin
          run_t;
          for i := 1 to size do
            begin
              sum := 0.0;
              for k := 1 to size do
                begin
                  deriv(exp[i,k],exp_type[i,k],x,y,ty);
                  if err_eval then
                    begin
                      err_eval := false;
                      iwriteln('err_eval');
                      exit;
                    end;
                  r := eval(y,ty);
                  {iwriteln('r  = ' + FloatToStr(r));
                  iwriteln('nk = ' + FloatToStr(vec[m].val[k]));}
                  sum := sum + val[i,k]*dn[k] + vec[m].val[k]*r;
                end;
              dna[i] := sum;
            end;
          for i := 1 to size do dn[i] := dna[i];
          iwriteln('t = ' + IntToStr(t__));
          s := '';
          for i := 1 to size do s := s + ' - ' + FloatToStr(dn[i]);
          iwriteln(s);
        end;
    end;
  err := false;
end;

procedure tform_sensib.dynsens(m : integer; nb_cycle : integer;
                                 var err : boolean; var dn : rvec3_type);
{ sensibilites dynamiques du vecteur de population aux entrees de la matrice m }
{ calculees sur nb_cyle pas de temps }
{ condition initiale = 0 }
{ retourne n^3 relation ou n est la taille de la matrice }
var i,k,p,q,icyc,n : integer;
    sum,ntot1,ntot2 : extended;
    dna : ^rvec3_type; { taille s^3 }
    s : string;
begin
  n  := mat[m].size;
  new(dna);
  for i := 1 to n do
    for p := 1 to n do
      for q := 1 to n do
      dn[i,p,q] := 0.0;
  for icyc := 1 to nb_cycle do
    begin
      run_t;
      for i := 1 to n do
        for p := 1 to n do
          for q := 1 to n do
            begin
              sum := 0.0;
              for k := 1 to n do
                sum := sum + mat[m].val[i,k]*dn[k,p,q];
              if ( p <> i ) then
                dna^[i,p,q] := sum
              else
                dna^[i,p,q] := sum + vec[m].val[q];
            end;
      for i := 1 to n do
        for p := 1 to n do
          for q := 1 to n do
            dn[i,p,q] := dna^[i,p,q];
    end;
  dispose(dna);
end;

procedure tform_sensib.sensibilites(m,x : integer);
var i,j : integer;
    err : boolean;
begin
  with mat[m] do
    begin
      edit_mat.Text := s_ecri_mat(m);
      eigenval(m,err);
      if err then exit;
      eigenvec(m,err);
      if err then exit;
      edit_lambda.Text := Format('%1.6g',[lambda1]);
      if ( x = 0 ) then
        begin
          edit_sens_x.Text := '';
          edit_sens_x_tot.Text := '';
          edit_elas_x_tot.Text := '';
          sens_lambda1(m,err);
          if err then exit;
        end
      else
        begin
          edit_sens_x.Text := s_ecri_var(x);
          sens_lambda1_x(m,x,err);
          if err then exit;
          edit_sens_x_tot.Text := Format('%10.4g',[sens_x_tot]);
          edit_elas_x_tot.Text := Format('%10.4g',[elas_x_tot]);
        end;
      with stringgrid_sens do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := size + 1;
          RowCount  := size + 1;
          for i := 1 to ColCount-1 do Cells[i,0] := IntToStr(i);
          for i := 1 to RowCount-1 do Cells[0,i] := IntToStr(i);
          for i := 1 to size do
            for j := 1 to size do
              Cells[j,i] := Format('%10.4f',[mat_sens[i,j]]);
        end;
      with stringgrid_elas do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := size + 1;
          FixedCols := 1;
          RowCount  := size + 1;
          FixedRows := 1;
          for i := 1 to size do Cells[i,0] := IntToStr(i);
          for i := 1 to size do Cells[0,i] := IntToStr(i);
          for i := 1 to size do
            for j := 1 to size do
              Cells[j,i] := Format('%10.4f',[mat_elas[i,j]]);
        end;
    end;
end;

procedure tform_sensib.dynsens_interp(m,x:integer);
var i,p,q,n,nb_cycle : integer;
    err : boolean;
    dn : rvec_type;
    dn1 : ^rvec3_type;
    s : string;
begin
  nb_cycle := 20;
  x := trouve_variable(trouve_dic('xxx'));
  iwriteln('avant dynsens ' + s_ecri_variable(x));
  dynsens_x(m,x,nb_cycle,err,dn);
  new(dn1);
  dynsens(m,nb_cycle,err,dn1^);
  n := mat[m].size;
  for i := 1 to n do
    begin
      iwriteln('i = ' + IntToStr(i));
      for p := 1 to n do
        begin
          s := '';
          for q := 1 to n do s := s + ' ' + FloatToStr(dn1^[i,p,q]);
          iwriteln(s);
        end;
    end;
  dispose(dn1);
end;

procedure tform_sensib.sensibilites_interp(m,x : integer);
var i,j : integer;
    err : boolean;
    s : string;
begin
  with mat[m] do
    begin
      iwriteln('Matrix ' + s_ecri_mat(m));
      eigenval(m,err);
      if err then exit;
      eigenvec(m,err);
      if err then exit;
      iwriteln('  Lambda = ' + Format('%1.6g',[lambda1]));
      if ( x = 0 ) then
        begin
          sens_lambda1(m,err);
          if err then exit;
          iwriteln('Assuming a same variance v on non-zero matrix entries:');
          iwriteln('  Var(lambda) =  ' + Format('%1.4g',[var_lambda]) + '*v');
          iwriteln('Assuming a same coeff. of variation c on matrix entries:');
          iwriteln('   CV(lambda) =  ' + Format('%1.4g',[cv_lambda]) + '*c');
          iwriteln('Sensitivities:');
          for i := 1 to size do
            begin
              s := '';
              for j := 1 to size do s := s + Format('%10.4f',[mat_sens[i,j]]);
              iwriteln(s);
            end;
          iwriteln('Elasticities:');
          for i := 1 to size do
            begin
              s := '';
              for j := 1 to size do s := s + Format('%10.4f',[mat_elas[i,j]]);
              iwriteln(s);
            end;
        end
      else
        begin
          sens_lambda1_x(m,x,err);
          if err then exit;
          iwriteln('Sensitivity lambda to ' + s_ecri_var(x) + ' = ' + Format('%1.4g',[sens_x_tot]));
          iwriteln('Elasticity lambda to ' + s_ecri_var(x) + ' = ' + Format('%1.4g',[elas_x_tot]));
        end;

      {dynsens_interp(m,x);}

    end;
end;

procedure tform_sensib.FormActivate(Sender: TObject);
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      edit_lambda.Text := '';
      exit;
    end;
  sensibilites(x_mat,x_var);
end;

procedure Tform_sensib.init_sensib;
var i : integer;
begin
  init_form(form_sensib);
  x_mat := 0;
  x_var := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        exit;
      end;
end;

procedure Tform_sensib.Edit_matReturnPressed(Sender: TObject);
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Matrix: unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure Tform_sensib.Edit_sens_xReturnPressed(Sender: TObject);
var x : integer;
    s : string;
begin
  s := edit_sens_x.Text;
  if ( s = '' ) then
    begin
      x_var := 0;
      FormActivate(nil);
      exit;
    end;
  if not test_variable(edit_sens_x.Text,x) then
    begin
      erreur('Variable: unknown variable name');
      edit_sens_x.Text := s_ecri_var(x_var);
      exit;
    end;
  x_var := x;
  FormActivate(nil);
end;

end.
