unit gulm;

{$MODE Delphi}

interface

uses LazFileUtils, SysUtils,Types,Classes,Variants,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ExtCtrls,ComCtrls,Menus,StdActns,ActnList,ImgList,
     ggraph,gtext, FileUtil;

type
  tform_ulm = class(TForm)
    Panel1: TPanel;
    Edit1: TEdit;
    Label1: TLabel;
    memo_interp: TMemo;
    StatusBar1: TStatusBar;
    ToolBar1: TToolBar;
    MainMenu1: TMainMenu;
    ActionList1: TActionList;
    filenew: TAction;
    fileopen: TAction;
    filesave: TAction;
    filesaveas: TAction;
    fileexit: TAction;
    filecompile: TAction;
    helpabout: TAction;
    EditCut1: TEditCut;
    EditCopy1: TEditCopy;
    EditPaste1: TEditPaste;
    run_init: TAction;
    run_run: TAction;
    run_settings: TAction;
    montecarlo_run: TAction;
    montecarlo_settings: TAction;
    view_variables: TAction;
    view_all: TAction;
    view_calculator: TAction;
    graph_new: TAction;
    graph_settings: TAction;
    text_new: TAction;
    text_settings: TAction;
    ImageList1: TImageList;
    File1: TMenuItem;
    New1: TMenuItem;
    Open1: TMenuItem;
    Save1: TMenuItem;
    SaveAs1: TMenuItem;
    nnn1: TMenuItem;
    Exit1: TMenuItem;
    Edit2: TMenuItem;
    Cut1: TMenuItem;
    Copy1: TMenuItem;
    Paste1: TMenuItem;
    Run1: TMenuItem;
    Init1: TMenuItem;
    MonteCarlo1: TMenuItem;
    Settings2: TMenuItem;
    View1: TMenuItem;
    variaBles1: TMenuItem;
    All1: TMenuItem;
    Text1: TMenuItem;
    Settings3: TMenuItem;
    Graphics1: TMenuItem;
    About1: TMenuItem;
    file_new_button: TToolButton;
    file_open_button: TToolButton;
    file_save_button: TToolButton;
    ToolButton6: TToolButton;
    cut_button: TToolButton;
    copy_button: TToolButton;
    paste_button: TToolButton;
    ToolButton10: TToolButton;
    init_button: TToolButton;
    compil_button: TToolButton;
    run_button: TToolButton;
    ToolButton14: TToolButton;
    montecarlo_button: TToolButton;
    ToolButton16: TToolButton;
    view_button: TToolButton;
    ToolButton18: TToolButton;
    text_button: TToolButton;
    Run2: TMenuItem;
    New2: TMenuItem;
    Calculator1: TMenuItem;
    Settings1: TMenuItem;
    MonteCarlo2: TMenuItem;
    Settings4: TMenuItem;
    graphnew1: TMenuItem;
    OpenDialog1: TOpenDialog;
    SaveDialog1: TSaveDialog;
    Compile1: TMenuItem;
    nnn2: TMenuItem;
    About2: TMenuItem;
    ToolButton1: TToolButton;
    graph_button: TToolButton;
    properties: TAction;
    sensitivities: TAction;
    multisite: TAction;
    matrix1: TMenuItem;
    properties1: TMenuItem;
    multisite1: TMenuItem;
    sensitivities1: TMenuItem;
    Landscape1: TMenuItem;
    prop_button: TToolButton;
    Age1: TMenuItem;
    age: TAction;
    landscape: TAction;
    tools: TMenuItem;
    spectrum1: TMenuItem;
    Correlation1: TMenuItem;
    Lyapunov1: TMenuItem;
    spectrum: TAction;
    correlation: TAction;
    lyapunov: TAction;
    Stochasticsensitivities1: TMenuItem;
    stocsensib: TAction;
    ConfidenceInterval1: TMenuItem;
    confint: TAction;
    confint2: TAction;
    Confidenceinterval21: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure start_model;
    procedure status_file(filename : string);
    procedure status_run(s : string);
    procedure status_t_exec(ms : integer);
    procedure status_time;
    procedure status_traj;
    procedure fileexitExecute(Sender: TObject);
    procedure filenewExecute(Sender: TObject);
    procedure fileopenExecute(Sender: TObject);
    procedure filesaveExecute(Sender: TObject);
    procedure filesaveasExecute(Sender: TObject);
    procedure filecompileExecute(Sender: TObject);
    procedure run_runExecute(Sender: TObject);
    procedure run_initExecute(Sender: TObject);
    procedure run_settingsExecute(Sender: TObject);
    procedure montecarlo_runExecute(Sender: TObject);
    procedure montecarlo_settingsExecute(Sender: TObject);
    procedure graph_settingsExecute(Sender: TObject);
    procedure graph_newExecute(Sender: TObject);
    procedure view_variablesExecute(Sender: TObject);
    procedure view_calculatorExecute(Sender: TObject);
    procedure edit1_returnpressed(Sender: TObject);
    procedure view_allExecute(Sender: TObject);
    procedure helpaboutExecute(Sender: TObject);
    procedure text_newExecute(Sender: TObject);
    procedure text_settingsExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure procproc;
    procedure Edit1DblClick(Sender: TObject);
    procedure propertiesExecute(Sender: TObject);
    procedure sensitivitiesExecute(Sender: TObject);
    procedure multisiteExecute(Sender: TObject);
    procedure ageExecute(Sender: TObject);
    procedure spectrumExecute(Sender: TObject);
    procedure correlationExecute(Sender: TObject);
    procedure lyapunovExecute(Sender: TObject);
    procedure landscapeExecute(Sender: TObject);
    procedure stocsensibExecute(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure confintExecute(Sender: TObject);
    procedure confint2Execute(Sender: TObject);
  private
    procedure erreur(s : string);
    procedure init_defaults;
    procedure interp_commands;
    procedure set_actions0;
    procedure set_actions1;
    procedure myIdleHandler(Sender: TObject; var Done: Boolean);
  public
  end;

var   form_ulm : tform_ulm;

const maxform_graph = 6;
      maxform_text  = 6;

type  tab_form_graph_type = array[1..maxform_graph] of tform_graph;
      tab_form_text_type  = array[1..maxform_text]  of tform_text;

var   tab_form_graph : tab_form_graph_type;
      nb_form_graph  : integer;
      i_form_graph   : integer; { numero fenetre graphique active }
      tab_form_text  : tab_form_text_type;
      nb_form_text   : integer;

const seuil_ext_def = 1.0;
      seuil_div_def = 10000000.0;

implementation

uses jglobvar,jsymb,jsyntax,jmath,jcompil,jeval,
     jmatrix,jinterp,jutil,jrun,jcarlo,
     grunset,ggraphset,gviewvar,gcalc,gedit,gviewall,gabout,gtextset,
     gsensib,gconfint,gconfint2,gstocsensib,gprop,gmulti,gage,
     gspec,gcorrel,glandscape,glyap;

{$R *.lfm}

const histmax = 30;

var  history : array[1..histmax] of string;
     phist,qhist : integer;

procedure tform_ulm.erreur(s : string);
begin
  erreur_('Commands - ' + s);
end;

procedure tform_ulm.init_defaults;
begin
  nb_cycle := 50;
  dt_texte_interp := 10;
  nb_cycle_carlo := 50;
  nb_run_carlo := 100;
  seuil_ext := seuil_ext_def;
  seuil_div := seuil_div_def;
  param  := false;
  notext := false;
  {carlo_n0 := false;} {ccccc}
  tobs := 0;
end;

procedure tform_ulm.set_actions0;
begin
  run_run.Enabled  := false;
  run_init.Enabled := false;
  run_settings.Enabled := false;
  montecarlo_run.Enabled := false;
  montecarlo_settings.Enabled := false;
  view_variables.Enabled := false;
  view_all.Enabled := false;
  view_calculator.Enabled := false;
  graph_new.Enabled := false;
  graph_settings.Enabled := false;
  text_new.Enabled := false;
  text_settings.Enabled := false;
  properties.Enabled := false;
  age.Enabled := false;
  sensitivities.Enabled := false;
  confint.Enabled := false;
  confint2.Enabled := false;
  stocsensib.Enabled := false;
  multisite.Enabled := false;
  landscape.Enabled := false;
  spectrum.Enabled := false;
  correlation.Enabled := false;
  lyapunov.Enabled := false;
end;

procedure tform_ulm.set_actions1;
begin
  run_run.Enabled  := true;
  run_init.Enabled := true;
  run_settings.Enabled := true;
  montecarlo_run.Enabled := true;
  montecarlo_settings.Enabled := true;
  view_variables.Enabled := true;
  view_all.Enabled := true;
  view_calculator.Enabled := true;
  graph_new.Enabled := true;
  graph_settings.Enabled := true;
  text_new.Enabled := true;
  text_settings.Enabled := true;
  properties.Enabled := true;
  age.Enabled := true;
  multisite.Enabled := true;
  sensitivities.Enabled := true;
  confint.Enabled := true;
  confint2.Enabled := true;
  stocsensib.Enabled := true;
  landscape.Enabled := true;
  spectrum.Enabled := true;
  correlation.Enabled := true;
  lyapunov.Enabled := true;
end;

procedure tform_ulm.start_model;
var f : integer;
begin
  init_math; { seed = 1 }
  init_symb;
  with form_edit do
    begin
      if not Visible then Show;
      lines_compil := memo1.Lines;
    end;
  compilation;
  if compiled then
    begin
      run_initExecute(nil);
      init_texte_interp;
      for f := 1 to maxform_text do
        with tab_form_text[f] do init_text;
      for f := 1 to maxform_graph do
        with tab_form_graph[f] do init_graph;
      status_file(nomfic);
      form_edit.status(nomfic);
      form_view_all.init_viewall;
      form_prop.init_prop;
      form_age.init_age;
      form_sensib.init_sensib;
      form_confint.init_confint;
      form_confint2.init_confint2;
      form_stocsensib.init_stoc_sensib;
      form_multi.init_multi;
      form_landscape.init_landscape;
      form_spec.init_spec;
      form_correl.init_correl;
      form_lyap.init_lyap;
      set_actions1;
    end;
end;

procedure tform_ulm.FormCreate(Sender: TObject);
begin
  Left   := 2;
  Top    := 172;
  Height := 542;
  Width  := 426;
  resolution;
  adjust(self);
  init_math;
  init_symb;
  init_syntax;
  init_defaults;
  init_compilation;
  init_interp;
  modelfileopened := false;
  inputfileopened := false;
  outputfile      := false;
  nomfic    := '';
  nomficin  := '';
  nomficout := '';
  if ( ParamCount > 0 ) and LazFileUtils.fileexistsUTF8(paramstr(1)) { *Converted from FileExists* } then
    begin
      nomfic := ParamStr(1);
      lines_compil.LoadFromFile(nomfic);
      modelfileopened := true;
    end;
  if ( ParamCount > 1 ) and LazFileUtils.fileexistsUTF8(paramstr(2)) { *Converted from FileExists* } then
    begin
      nomficin := ParamStr(2);
      lines_interp.LoadFromFile(nomficin);
      inputfileopened := true;
      if ( Paramcount > 2 ) then
        begin
          nomficout := ParamStr(3);
          outputfile := true;
          AssignFile(ficout,nomficout);
          rewrite(ficout);
        end;
    end;
  set_actions0;
  Application.OnIdle := myIdleHandler;
  KeyPreview := true;
  runstop := false;
  phist := 0;
end;

procedure tform_ulm.myIdleHandler(Sender: TObject; var Done: Boolean);
begin
end;

procedure tform_ulm.FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if ( shift = ([ssAlt,ssCtrl]) ) then
    begin
      runstop := true;
      status_run('Stopped');
    end;
end;

procedure tform_ulm.procproc;
begin
  Application.ProcessMessages;
end;

procedure tform_ulm.interp_commands;
var i : integer;
    s : string;
begin
  start_model;
  for i := 0 to lines_interp.Count - 1 do
    begin
      s := lines_interp[i];
      iwriteln('> ' + s);
      interp(s);
    end;
  if outputfile then CloseFile(ficout);
  fileexitExecute(nil);
end;

procedure create_form_graph;
var i : integer;
begin
  nb_form_graph := 1;
  i_form_graph  := 1;
  tab_form_graph[1] := form_graph;
   {FIXME: AccessViolation}
  {for i := 2 to maxform_graph do
    tab_form_graph[i] := tform_graph.Create(nil);
   for i := 1 to maxform_graph do with tab_form_graph[i] do
    begin
      ifg := i;
      Caption := 'GRAPHICS <' + IntToStr(ifg) +'>';
      Top  := Top  - 20*i;
      Left := Left - 20*i;
    end;}
   {/FIXME}
end;

procedure create_form_text;
var i : integer;
begin
  nb_form_text := 1;
  tab_form_text[1] := form_text;
  for i := 2 to maxform_text do
    tab_form_text[i] := tform_text.Create(nil);
  for i := 1 to maxform_text do with tab_form_text[i] do
    begin
      ift := i;
      Caption := 'TEXT <' + IntToStr(ift) +'>';
    end;
end;

procedure tform_ulm.FormShow(Sender: TObject);
begin
  create_form_graph;
  create_form_text;
  if modelfileopened then with form_edit do
    begin
      memo1.Lines := lines_compil;
      newpage(nomfic);
      status(nomfic);
      Show;
    end;
  form_graph.Show;
  if inputfileopened then interp_commands;
end;

procedure tform_ulm.status_file(filename : string);
begin
  statusbar1.Panels[0].Text := ExtractFileName(filename);
end;

procedure tform_ulm.status_run(s : string);
begin
  statusbar1.Panels[1].Text := s;
  iwriteln('> ' + s);
end;

procedure tform_ulm.status_t_exec(ms : integer);
begin
  statusbar1.Panels[2].Text := s_ecri_t_exec(ms);
  if outputfile then iwriteln(s_ecri_t_exec(ms));
end;

procedure tform_ulm.status_time;
begin
  if not notext then statusbar1.Panels[3].Text := 't = ' + IntToStr(t__);
end;

procedure tform_ulm.status_traj;
begin
  if not notext then statusbar1.Panels[3].Text := 'traj = ' + IntToStr(traj__);
end;

procedure tform_ulm.edit1_returnpressed(Sender: TObject);
var s : string;
begin
  if not compiled then exit;
  s := tronque(minuscule(edit1.Text));
  iwriteln('> ' + s);
  if ( s <> '' ) then
    begin
      phist := (phist + 1) mod (histmax + 1);
      if ( phist = 0 ) then phist := 1;
      history[phist] := s;
      qhist := phist;
    end;
  edit1.Clear;
  interp(s);
end;

procedure tform_ulm.Edit1DblClick(Sender: TObject);
begin
  edit1.Text := history[qhist];
  qhist := qhist - 1;
  if ( qhist = 0 ) then qhist := histmax;{jjjjj}
end;

procedure tform_ulm.filenewExecute(Sender: TObject);
begin
  with form_edit do
    begin
      nomfic := 'untitled.ulm';
      memo1.Clear;
      newpage(nomfic);
      if not Visible then Show;
    end;
end;

procedure tform_ulm.fileopenExecute(Sender: TObject);
begin
  if opendialog1.Execute then with form_edit do
    begin
      nomfic := opendialog1.FileName;
      memo1.Lines.LoadFromFile(nomfic);
      modelfileopened := true;
      newpage(nomfic);
      if not Visible then Show;
    end;
end;

procedure tform_ulm.filesaveExecute(Sender: TObject);
begin
  if ( nomfic = 'untitled.ulm' ) then
    filesaveasExecute(nil)
  else
    with form_edit do
      begin
        memo1.Lines.SaveToFile(nomfic);
        TMemo(pagecontrol1.ActivePage.Controls[0]).Modified := false;
      end;
end;

procedure tform_ulm.filesaveasExecute(Sender: TObject);
begin
  with savedialog1 do
    begin
      FileName := nomfic;
      InitialDir := ExtractFilePath(nomfic);
      if Execute then with form_edit do
        begin
          if LazFileUtils.fileexistsUTF8(FileName) { *Converted from FileExists* } then
            if MessageDlg('Overwrite file ' + ExtractFileName(FileName) + '?',
               mtConfirmation,[mbYes,mbNo],0) <> mrYes then exit;
          nomfic := FileName;
          memo1.Lines.SaveToFile(nomfic);
          status(nomfic);
          TMemo(pagecontrol1.ActivePage.Controls[0]).Modified := false;
          TMemo(pagecontrol1.ActivePage.Controls[0]).Hint := nomfic;
          pagecontrol1.ActivePage.Caption := ExtractFileName(nomfic);
        end;
    end;
end;

procedure tform_ulm.filecompileExecute(Sender: TObject);
begin
  start_model;
end;

procedure tform_ulm.run_initExecute(Sender: TObject);
begin
  init_eval;
  status_run('Init ' + IntToStr(graine0-graine00+1));
  status_time;
end;

procedure tform_ulm.run_runExecute(Sender: TObject);
var ms : integer;
begin
  status_run('Run ' + IntToStr(nb_cycle));
  ms := clock;
  if param then
    run_param(nb_cycle,xparam,param_min,param_max,param_pas)
  else
    run(nb_cycle);
  status_t_exec(clock - ms);
  status_time;
end;

procedure tform_ulm.run_settingsExecute(Sender: TObject);
begin
  form_runset.Show;
end;

procedure tform_ulm.montecarlo_runExecute(Sender: TObject);
var ms : integer;
begin
  status_run('Montecarlo ' + IntToStr(nb_cycle_carlo)+ ' ' +
             IntToStr(nb_run_carlo));
  ms := clock;
  carlo(nb_cycle_carlo,nb_run_carlo,seuil_ext,seuil_div);
  status_t_exec(clock - ms);
  status_time;
end;

procedure tform_ulm.montecarlo_settingsExecute(Sender: TObject);
begin
  run_settingsExecute(nil);
end;

procedure tform_ulm.graph_settingsExecute(Sender: TObject);
begin
  with form_graphset do
    begin
      fg := tab_form_graph[i_form_graph];
      Show;
    end;
end;

procedure tform_ulm.graph_newExecute(Sender: TObject);
var f : integer;
begin
  nb_form_graph := nb_form_graph + 1;
  if ( nb_form_graph > maxform_graph ) then
    begin
      erreur('no more than ' + IntToStr(maxform_graph) +
             ' graphic windows');
      nb_form_graph := maxform_graph;
      exit;
    end;
  for f := 1 to nb_form_graph do with tab_form_graph[f] do
    if not Visible then Show;
end;

procedure tform_ulm.view_calculatorExecute(Sender: TObject);
begin
  form_calc.Show;
end;

procedure tform_ulm.view_variablesExecute(Sender: TObject);
begin
  form_view_variables.Show;
end;

procedure tform_ulm.view_allExecute(Sender: TObject);
begin
  form_view_all.Show;
end;

procedure tform_ulm.helpaboutExecute(Sender: TObject);
begin
  form_about.ShowModal;
end;

procedure tform_ulm.text_newExecute(Sender: TObject);
var f : integer;
begin
  if ( nb_form_text = maxform_text ) then
    begin
      erreur('no more than ' + IntToStr(maxform_text) +
             ' text windows');
      exit;
    end;
  for f := 1 to maxform_text do with tab_form_text[f] do
    if ( not Visible ) then
      begin
        Visible := true;
        nb_form_text := nb_form_text + 1;
        break;
      end;
end;

procedure tform_ulm.text_settingsExecute(Sender: TObject);
begin
  with form_textset do
    begin
      ft := form_text;
      Show;
    end;
end;

procedure tform_ulm.fileexitExecute(Sender: TObject);
begin
  Close;
end;

procedure tform_ulm.FormClose(Sender: TObject; var action: TCloseAction);
var action1 : TCloseAction;
begin
  if form_edit = nil then exit;
  action := caFree;
  with form_edit do FormClose(nil,action1);
  if ( action1 = caNone ) then action := caNone else exit;
end;

procedure tform_ulm.propertiesExecute(Sender: TObject);
begin
  with form_prop do Show;
end;

procedure tform_ulm.sensitivitiesExecute(Sender: TObject);
begin
  with form_sensib do Show;
end;

procedure tform_ulm.confintExecute(Sender: TObject);
begin
  with form_confint do Show;
end;

procedure tform_ulm.confint2Execute(Sender: TObject);
begin
  with form_confint2 do Show;
end;

procedure tform_ulm.stocsensibExecute(Sender: TObject);
begin
  with form_stocsensib do Show;
end;

procedure tform_ulm.multisiteExecute(Sender: TObject);
begin
  with form_multi do Show;
end;

procedure tform_ulm.ageExecute(Sender: TObject);
begin
  with form_age do Show;
end;

procedure tform_ulm.spectrumExecute(Sender: TObject);
begin
  with form_spec do Show;
end;

procedure tform_ulm.correlationExecute(Sender: TObject);
begin
  with form_correl do Show;
end;

procedure tform_ulm.lyapunovExecute(Sender: TObject);
begin
  with form_lyap do Show;
end;

procedure tform_ulm.landscapeExecute(Sender: TObject);
begin
  with form_landscape do Show;
end;

procedure tform_ulm.FormDestroy(Sender: TObject);
var i : integer;
begin
  for i := 2 to maxform_text  do tab_form_text[i].Free;
  for i := 2 to maxform_graph do tab_form_graph[i].Free;
  for i := 1 to fic_nb do CloseFile(fic[i].f);
end;

end.
