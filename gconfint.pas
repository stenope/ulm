unit gconfint;

{$MODE Delphi}

{ computation of confidence interval for growth rate lambda }
{ knowing sigma's of matrix entries or demographic parameters }

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ExtCtrls,ComCtrls,Grids,
     jglobvar,jmath;

type
  tform_confint = class(TForm)
    stringgrid_sigma: TStringGrid;
    label_sens: TLabel;
    label_mat: TLabel;
    label_lambda: TLabel;
    Edit_mat: TEdit;
    label_sigma_lambda: TLabel;
    label_lambda_plus: TLabel;
    label_lambda_moins: TLabel;
    edit_z_alpha: TEdit;
    label_z_alpha: TLabel;
    label_confidence: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure init_confint;
    procedure erreur(s : string);
    procedure Edit_matReturnPressed(Sender: TObject);
    procedure eigenval(x : integer; var err : boolean);
    procedure eigenvec(x : integer; var err : boolean);
    procedure confint(m : integer);
    procedure sigma_lambda1(m : integer; var err : boolean);
    procedure stringgrid_sigmaSelectCell(Sender: TObject; ACol,
      ARow: Integer; var CanSelect: Boolean);
    procedure stringgrid_sigmaSetEditText(Sender: TObject; ACol,
      ARow: Integer; const Value: WideString);
    procedure edit_z_alphaReturnPressed(Sender: TObject);
  private
    n : integer; { taille de la matrice }
    x_mat : integer;    { matrice courante }
    lambda : cvec_type; { valeurs propres complexes }
    v,w : rvec_type;    { vecteurs propres gauche, droite }
    lambda1 : extended; { dominant eigenvalue }
    mat_sens : rmat_type; { matrice des sensibilites }
    mat_sigma : rmat_type; { matrice des ecarts_types }
    sigma_lambda : extended; { sigma de lambda }
    z_alpha : extended; { lambda +/- z_alpha*sigma_lambda }
    alpha : extended;   { % confidence interval }
  public
  end;

var form_confint: tform_confint;

implementation

uses jutil,jsymb,jmatrix,jsyntax,jeval;

{$R *.lfm}

procedure tform_confint.erreur(s : string);
begin
  erreur_('Confidence Interval - ' + s);
end;

procedure tform_confint.FormCreate(Sender: TObject);
begin
  Left   := 180;
  Top    := 400;
  Height := 294;
  Width  := 804;
  adjust(self);
  Caption := 'CONFIDENCE INTERVAL';
  x_mat := 0;
  z_alpha := 1.0;
end;

procedure tform_confint.eigenval(x : integer;var err : boolean);
begin
  err := true;
  with mat[x] do
    begin
      matvalprop(size,val,lambda);
      if err_math then
        begin
          erreur('Error in eigenvalues computation');
          err_math := false;
          exit;
        end;
      if ( lambda[1].im <> 0.0 ) then
        begin
          erreur('No real dominant eigenvalue found');
          exit;
        end;
      lambda1 := lambda[1].re;
      if ( lambda1 <= 0.0 ) then
        begin
          erreur('Dominant eigenvalue <= 0');
          exit;
        end;
    end;
  err := false;
end;

procedure tform_confint.eigenvec(x : integer; var err : boolean);
var i : integer;
    vv,ww : cvec_type;
begin
  err := true;
  with mat[x] do
    begin
      matvecpropdroite(size,val,lambda[1],ww);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do w[i] := ww[i].re;
      vecnormalise1(size,w);
      matvecpropgauche(size,val,lambda[1],vv);
      if err_math then
        begin
          err_math := false;
          exit;
        end;
      for i := 1 to size do v[i] := vv[i].re;
      vecnormalise1(size,v);
    end;
  err := false;
end;

procedure tform_confint.sigma_lambda1(m : integer;var err : boolean);
var i,j : integer;
    h,a : extended;
begin
  err := true;
  with mat[m] do
    begin
      h := vecpscal(size,v,w);
      if ( h = 0.0 ) then exit;
      for i := 1 to size do
        for j := 1 to size do
          mat_sens[i,j] := v[i]*w[j]/h;
      h := 0.0;
      for i := 1 to size do
        for j := 1 to size do
          begin
            a := mat_sigma[i,j];
            h := h + sqr(mat_sens[i,j]*a);
          end;
      if ( h > 0.0 ) then
        sigma_lambda := sqrt(h)
      else
        sigma_lambda := 0.0;
    end;
  err := false;
end;

procedure tform_confint.confint(m : integer);
var i,j : integer;
    err : boolean;
begin
  n := mat[m].size;
  for i := 1 to n do
     for j := 1 to n do
       mat_sigma[i,j] := StrToFloat(stringgrid_sigma.Cells[j,i]);
  edit_mat.Text := s_ecri_mat(m);
  eigenval(m,err);
  if err then exit;
  eigenvec(m,err);
  if err then exit;
  label_lambda.Caption := 'lambda = ' + Format('%1.6g',[lambda1]);
  sigma_lambda1(m,err);
  if err then exit;
  label_sigma_lambda.caption := 'Sigma_lambda = ' + Format('%10.6g',[sigma_lambda]);
  label_lambda_plus.caption := 'Lambda + z*sigma = ' + Format('%10.6g',[lambda1 + z_alpha*sigma_lambda]);
  label_lambda_moins.caption := 'Lambda - z*sigma = ' + Format('%10.6g',[lambda1 - z_alpha*sigma_lambda]);
  alpha := 2.0*phinormal(z_alpha) - 1.0;
  label_confidence.caption := '---> with ' + Format('%1.2g',[alpha*100.0]) + '% confidence';
end;

procedure tform_confint.FormActivate(Sender: TObject);
var i,j : integer;
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      edit_mat.Text := '';
      exit;
    end;
  n := mat[x_mat].size;
  with stringgrid_sigma do
    begin
      FixedCols := 1;
      FixedRows := 1;
      ColCount  := n + 1;
      RowCount  := n + 1;
      for i := 1 to ColCount-1 do Cells[i,0] := IntToStr(i);
      for i := 1 to RowCount-1 do Cells[0,i] := IntToStr(i);
      for i := 1 to n do
        for j := 1 to n do
          Cells[j,i] := Format('%10.4f',[mat_sigma[i,j]]);
    end;
  confint(x_mat);
  edit_z_alpha.Text := FloatToStr(z_alpha);
end;

procedure Tform_confint.init_confint;
var i,j,k : integer;
begin
  init_form(form_confint);
  x_mat := 0;
  z_alpha := 1.0;
  for k := 1 to modele_nb do
    if ( modele[k].xmat <> 0 ) then
      begin
        x_mat := modele[k].xmat;
        n := mat[x_mat].size;
        for i := 1 to n do
          for j := 1 to n do
            if ( mat[x_mat].val[i,j] > 0.0 ) then
              mat_sigma[i,j] := 0.1
            else
              mat_sigma[i,j] := 0.0;
        exit;
      end;
end;

procedure Tform_confint.Edit_matReturnPressed(Sender: TObject);
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_mat.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Matrix: unknown matrix name');
      edit_mat.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure tform_confint.stringgrid_sigmaSelectCell(Sender: TObject; ACol,
  ARow: Integer; var CanSelect: Boolean);
begin
  canselect := ( acol > 0 ) and ( arow > 0 )
end;

procedure tform_confint.stringgrid_sigmaSetEditText(Sender: TObject; ACol,
  ARow: Integer; const Value: WideString);
var i,j : integer;
begin
  i := acol;
  j := arow;
  mat_sigma[i,j] := StrToFloat(stringgrid_sigma.Cells[j,i]); { inutile en fait }
  confint(x_mat);
end;

procedure tform_confint.edit_z_alphaReturnPressed(Sender: TObject);
begin
  z_alpha := StrToFloat(edit_z_alpha.Text);
  FormActivate(nil);
end;

end.
