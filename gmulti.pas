unit gmulti;

{$MODE Delphi}

interface

uses SysUtils,Types,Classes,Graphics,Controls,Forms,Dialogs,
     StdCtrls,ExtCtrls,ComCtrls,Grids,
     jglobvar;

type
  tform_multi = class(TForm)
    Label_matrix: TLabel;
    Edit_matrix: TEdit;
    Label_psite: TLabel;
    Edit_psite: TEdit;
    GroupBox_renew: TGroupBox;
    Label_renew: TLabel;
    StringGrid_renew: TStringGrid;
    StringGrid_vs: TStringGrid;
    StringGrid_ws: TStringGrid;
    Label_ws: TLabel;
    Label_vs: TLabel;
    GroupBox_r0tb: TGroupBox;
    StringGrid_r0: TStringGrid;
    Label_r0: TLabel;
    Label_r0_tot: TLabel;
    Edit_r0_tot: TEdit;
    Label_tr0: TLabel;
    Edit_tr0: TEdit;
    StringGrid_tbar: TStringGrid;
    Label_tbar: TLabel;
    Label_tbar_tot: TLabel;
    Edit_tbar_tot: TEdit;
    GroupBox_newborn: TGroupBox;
    StringGrid_longterm: TStringGrid;
    Label_longterm: TLabel;
    StringGrid_v0_w0: TStringGrid;
    Label_v0_w0: TLabel;
    procedure FormActivate(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure init_multi;
    procedure Edit_matrixReturnPressed(Sender: TObject);
    procedure erreur(s : string);
    procedure Edit_psiteReturnPressed(Sender: TObject);
    procedure proprietes_multi;
  private
    v0 : rvec_type; { reproductive value of newborns }
    w0 : rvec_type; { stable site distribution of newborns }
    uu : rmat_type; { site by site long-term transitions of newborn }
    aa : rmat_type; { renewal matrix }
    vs : rmat_type; { age by site reproductive value }
    ws : rmat_type; { age by site stable distribution }
    rr : rmat_type; { site by site reproductive value }
    tt : rmat_type; { site by site mean generation length }
    tr0 : extended; { generation time associated with r0 }
    r0_tot : extended;   { multisite r0 }
    tbar_tot : extended; { multisite mean generation length }
    x_mat : integer; { indice matrice }
    psite : integer; { nombre de sites }
  end;

var form_multi: tform_multi;

implementation

uses jutil,jsymb,jmath,jmatrix,jsyntax;

{$R *.lfm}

procedure tform_multi.erreur(s : string);
begin
  erreur_('Multisite - ' + s);
end;
procedure tform_multi.proprietes_multi;
{ matrice multisite de taille n = size avec p = psite sites }
{ les blocs diagonaux sont tous de leslie ou de leslie2, de taille m = n/p }
label 10;
const nbsitemax = 10;
      matmax2 = matmax div 2; {nb max de classes d'âges sur chaque site }
      eps = 1.0e-6;
type  rmat2_type = array[1..nbsitemax,1..nbsitemax] of extended;
      rvec2_type = array[1..nbsitemax] of extended;
      tab_rmat2_type = array[1..matmax2] of rmat2_type;
      tab_rvec2_type = array[1..matmax2] of rvec_type;
var   m,i,j,h,k : integer;
      xx,zz,oo : ^rmat_type;
      ff,ss,ll,gg,hh,kk,ww,vv : ^tab_rmat2_type;
      lambda,vz,wz : cvec_type;
      z : cmx;
      e,lambda1,sum,l1 : extended;
      vr : rvec_type;

procedure transform_multi;
{ transforme la matrice multisite de taille size avec psite sites site*age }
{ en matrice xx age*site }
var m,i,j,sitei,agei,sitej,agej,ix,jx : integer;
begin
  with mat[x_mat] do
    begin
      m := size div psite;
      for i := 1 to size do
        begin
          sitei := (i-1) div m + 1;
          agei := i mod m;
          if ( i mod m = 0 ) then agei := m;
          ix := (agei-1)*psite + sitei;
          for j := 1 to size do
            begin
              sitej := (j-1) div m + 1;
              agej := j mod m;
              if (j mod m = 0) then agej := m;
              jx := (agej-1)*psite + sitej;
              xx^[ix,jx] := val[i,j];
            end;
        end;
    end;
end;

begin
  new(xx);
  new(oo);
  new(ff);
  new(ss);
  new(ll);
  new(gg);
  new(hh);
  new(kk);
  new(zz);
  new(ww);
  new(vv);
  transform_multi; { tester nb site max jjjjj}
  with mat[x_mat] do
  begin
  m := size div psite; { number of age classes in each block }
  { fertility matrices: }
  for h := 1 to m do
    for i := 1 to psite do
      for j := 1 to psite do
        ff^[h,i,j] := xx^[i,(h-1)*psite + j];
  { survival matrices: }
  for h := 1 to m-1 do
    for i := 1 to psite do
      for j := 1 to psite do
        ss^[h,i,j] := xx^[h*psite + i,(h-1)*psite + j];
  for i := 1 to psite do
    for j := 1 to psite do
      ss^[m,i,j] := xx^[(m-1)*psite + i,(m-1)*psite + j];
  { Li matrices: }
  for i := 1 to psite do
    for j := 1 to psite do
      ll^[1,i,j] := 0.0;
  for i := 1 to psite do ll^[1,i,i] := 1.0;
  for h := 2 to m do
    for i := 1 to psite do
      for j := 1 to psite do
        begin
          sum := 0.0;
          for k := 1 to psite do
            sum := sum + ss^[h-1,i,k]*ll^[h-1,k,j];
          ll^[h,i,j] := sum;
        end;
  { FiLi matrices: }
  for h := 1 to m do
    for i := 1 to psite do
      for j := 1 to psite do
        begin
          sum := 0.0;
          for k := 1 to psite do
            sum := sum + ff^[h,i,k]*ll^[h,k,j];
          gg^[h,i,j] := sum;
        end;
  matvalprop(size,val,lambda);
  if err_math then
    begin
      erreur('Error in eigenvalues computation');
      err_math := false;
      exit;
    end;
  if ( lambda[1].im <> 0.0 ) then
    begin
      erreur('No real dominant eigenvalue found');
      exit;
    end;
  lambda1 := lambda[1].re;
  if ( lambda1 <= 0.0 ) then exit;
  { renewal matrix: }
  for i := 1 to psite do
    for j := 1 to psite do
      aa[i,j] := 0.0;
  e := 1.0/lambda1;
  for h := 1 to m-1 do
    begin
      for i := 1 to psite do
        for j := 1 to psite do
          aa[i,j] := aa[i,j] + e*gg^[h,i,j];
      e := e/lambda1;
    end;
  for i := 1 to psite do
    for j := 1 to psite do
      zz^[i,j] := gg^[m,i,j];
  for h := 1 to 100 do
    begin
      for i := 1 to psite do
        for j := 1 to psite do
          aa[i,j] := aa[i,j] + e*zz^[i,j];
      e := e/lambda1;
      for i := 1 to psite do
        for j := 1 to psite do
          begin
            sum := 0.0;
            for k := 1 to psite do
              sum := sum + ss^[m,i,k]*zz^[k,j];
            zz^[i,j] := sum;
          end;
    end;
  matvalprop(psite,aa,lambda);
  if err_math then
    begin
      err_math := false;
      exit;
    end;
  if ( lambda[1].im <> 0.0 ) then
    begin
      erreur('Renewal matrix: no real dominant eigenvalue found');
      exit;
    end;
  l1 := lambda[1].re;
  if ( abs(l1-1.0) > eps ) then
    begin
      erreur('Renewal matrix: dominant eigenvalue <> 1');
      exit;
    end;
  matvecpropdroite(psite,aa,lambda[1],wz);
  if not err_math then
    begin
      for i := 1 to psite do w0[i] := wz[i].re;
      vecnormalise1(psite,w0);
    end
  else
    begin
      erreur('Error in renewal matrix eigenvector computation');
      err_math := false;
      exit;
    end;
  matvecpropgauche(psite,aa,lambda[1],vz);
  if not err_math then
    begin
      for i := 1 to psite do v0[i] := vz[i].re;
      vecnormalise1(psite,v0);
    end
  else
    begin
      erreur('Error in renewal matrix eigenvector computation');
      err_math := false;
      exit;
    end;
  e := vecpscal(psite,v0,w0);
  if ( e <= 0.0 ) then exit; {jjjjj}
  for i := 1 to psite do v0[i] := v0[i]/e;
  { long-term transitions of newborns: w0v0'}
  for i := 1 to psite do
    for j := 1 to psite do
      uu[i,j] := w0[i]*v0[j];
  { net reproductive rate matrix: }
  for i := 1 to psite do
    for j := 1 to psite do
      rr[i,j] := 0.0;
  for h := 1 to m-1 do
    for i := 1 to psite do
      for j := 1 to psite do
        rr[i,j] := rr[i,j] + gg^[h,i,j];
  for i := 1 to psite do
    for j := 1 to psite do
      zz^[i,j] := gg^[m,i,j];
  for h := 1 to 100 do
    begin
      for i := 1 to psite do
        for j := 1 to psite do
          rr[i,j] := rr[i,j] + zz^[i,j];
      for i := 1 to psite do
        for j := 1 to psite do
          begin
            sum := 0.0;
            for k := 1 to psite do
              sum := sum + ss^[m,i,k]*zz^[k,j];
            zz^[i,j] := sum;
          end;
    end;
  { multisite net reproductive rate r0: }
  for i := 1 to psite do
    begin
      sum := 0.0;
      for j := 1 to psite do sum := sum + rr[i,j]*w0[j];
      vr[i] := sum;
    end;
  r0_tot := vecpscal(psite,v0,vr);
  if ( lambda1 <> 1 ) then
    tr0 := ln(r0_tot)/ln(lambda1)
  else
    tr0 := 0.0;
  { mean generation length: }
  for i := 1 to psite do
    for j := 1 to psite do
      tt[i,j] := 0.0;
  e := 1.0/lambda1;
  for h := 1 to m-1 do
    begin
      for i := 1 to psite do
        for j := 1 to psite do
          tt[i,j] := tt[i,j] + h*e*gg^[h,i,j];
      e := e/lambda1;
    end;
  for i := 1 to psite do
    for j := 1 to psite do
      zz^[i,j] := gg^[m,i,j];
  for h := 1 to 100 do
    begin
      for i := 1 to psite do
        for j := 1 to psite do
          tt[i,j] := tt[i,j] + (m+h-1)*e*zz^[i,j];
      e := e/lambda1;
      for i := 1 to psite do
        for j := 1 to psite do
          begin
            sum := 0.0;
            for k := 1 to psite do
              sum := sum + ss^[m,i,k]*zz^[k,j];
            zz^[i,j] := sum;
          end;
    end;
  for i := 1 to psite do
    begin
      sum := 0.0;
      for j := 1 to psite do sum := sum + tt[i,j]*w0[j];
      vr[i] := sum;
    end;
  tbar_tot := vecpscal(psite,v0,vr);
  { stable age by site population structure: }
  for i := 1 to psite do
    for j := 1 to psite do
      ww^[1,i,j] := 0.0;
  for i := 1 to psite do ww^[1,i,i] := 1.0;
  e := 1.0/lambda1;
  for h := 2 to m-1 do
    for i := 1 to psite do
      for j := 1 to psite do
        begin
          sum := 0.0;
          for k := 1 to psite do
            sum := sum + ss^[h-1,i,k]*ww^[h-1,k,j];
          ww^[h,i,j] := e*sum;
        end;
  { calcul de zz = (lambda*I - Sn)^(-1) }
  for i := 1 to psite do
    for j := 1 to psite do
      oo[i,j] := 0.0;
  for i := 1 to psite do oo[i,i] := e;
  for i := 1 to psite do
    for j := 1 to psite do
      zz^[i,j] := oo^[i,j];
  for h := 1 to 100 do
    begin
      for i := 1 to psite do
        for j := 1 to psite do
          begin
            sum := 0.0;
            for k := 1 to psite do
              sum := sum + ss^[m,i,k]*oo^[k,j];
            xx^[i,j] := e*sum;
          end;
      for i := 1 to psite do
        for j := 1 to psite do
          begin
            oo^[i,j] := xx^[i,j];
            zz^[i,j] := zz^[i,j] + oo^[i,j];
          end;
    end;
  for i := 1 to psite do
    for j := 1 to psite do
      begin
        sum := 0.0;
        for k := 1 to psite do
          sum := sum + ww^[m-1,i,k]*ss^[m-1,k,j];
        oo^[i,j] := sum;
      end;
  for i := 1 to psite do
    for j := 1 to psite do
      begin
        sum := 0.0;
        for k := 1 to psite do
          sum := sum + oo^[i,k]*zz^[k,j];
        ww^[m,i,j] := sum;
      end;
  for h := 1 to m do
    for i := 1 to psite do
      begin
        sum := 0.0;
        for j := 1 to psite do sum := sum + ww^[h,i,j]*w0[j];
        ws[h,i] := sum;
      end;
  { stable age by site reproductive value: }
  for i := 1 to psite do
    for j := 1 to psite do
      vv^[1,i,j] := 0.0;
  for i := 1 to psite do vv^[1,i,i] := 1.0;
  for i := 1 to psite do
    for j := 1 to psite do
      begin
        sum := 0.0;
        for k := 1 to psite do
          sum := sum + ff^[m,i,k]*zz^[k,j];
        vv^[m,i,j] := sum;
      end;
  e := 1.0/lambda1;
  for h := m-1 downto 2 do
    for i := 1 to psite do
      for j := 1 to psite do
        begin
          sum := 0.0;
          for k := 1 to psite do
            sum := sum + ss^[h,i,k]*vv^[h+1,k,j];
          vv^[h,i,j] := e*(ff^[h,i,j] + sum);
        end;
  for h := 1 to m do
    for j := 1 to psite do
      begin
        sum := 0.0;
        for i := 1 to psite do sum := sum + v0[j]*vv^[h,i,j];
        vs[h,j] := sum;
      end;
10:
  dispose(xx);
  dispose(oo);
  dispose(ff);
  dispose(ss);
  dispose(ll);
  dispose(gg);
  dispose(hh);
  dispose(kk);
  dispose(zz);
  dispose(ww);
  dispose(vv);
  end;
end;

procedure tform_multi.FormActivate(Sender: TObject);
var i,j,m,p : integer;
begin
  if ( x_mat = 0 ) then
    begin
      erreur('No matrix-type model');
      exit;
    end;
  if ( psite = 0 ) then
    begin
      with mat[x_mat] do
        if matmultisite(size,val,p) then
          psite := p
        else
          begin
            erreur('Matrix is not multisite');
            exit;
          end;
    end;
  edit_matrix.Text := s_ecri_mat(x_mat);
  edit_psite.Text  := IntToStr(psite);
  with mat[x_mat] do
    begin
      m := size div psite;
      proprietes_multi;  { voir psite = 1 jjjjj}
      with stringgrid_v0_w0 do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := 3;
          RowCount  := psite + 1;
          Cells[1,0] := 'Rep. Value';
          Cells[2,0] := 'Stable Dist.';
          for i := 1 to psite do

            begin
              Cells[0,i] := IntToStr(i);
              Cells[1,i] := Format('%10.6f',[v0[i]]);
              Cells[2,i] := Format('%10.4f',[w0[i]]);
            end;
        end;
      with stringgrid_longterm do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := psite + 1;
          RowCount  := psite + 1;
          for i := 1 to psite do Cells[i,0] := IntToStr(i);
          for i := 1 to psite do Cells[0,i] := IntToStr(i);
          for i := 1 to psite do
            for j := 1 to psite do
              Cells[j,i] := Format('%10.4f',[uu[i,j]]);
        end;
      with stringgrid_renew do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := psite + 1;
          RowCount  := psite + 1;
          for i := 1 to psite do Cells[i,0] := IntToStr(i);
          for i := 1 to psite do Cells[0,i] := IntToStr(i);
          for i := 1 to psite do
            for j := 1 to psite do
              Cells[j,i] := Format('%10.4f',[aa[i,j]]);
        end;
      with stringgrid_vs do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := psite + 1;
          RowCount  := m + 1;
          Cells[0,0] := 'Age\Site';
          for i := 1 to psite do Cells[i,0] := IntToStr(i);
          for i := 1 to m     do Cells[0,i] := IntToStr(i);
          for i := 1 to m do
            for j := 1 to psite do
              Cells[j,i] := Format('%10.4f',[vs[i,j]]);
        end;
      with stringgrid_ws do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := psite + 1;
          RowCount  := m + 1;
          Cells[0,0] := 'Age\Site';
          for i := 1 to psite do Cells[i,0] := IntToStr(i);
          for i := 1 to m     do Cells[0,i] := IntToStr(i);
          for i := 1 to m do
            for j := 1 to psite do
              Cells[j,i] := Format('%10.4f',[ws[i,j]]);
        end;
      with stringgrid_r0 do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := psite + 1;
          RowCount  := psite + 1;
          for i := 1 to psite do Cells[i,0] := IntToStr(i);
          for i := 1 to psite do Cells[0,i] := IntToStr(i);
          for i := 1 to psite do
            for j := 1 to psite do
              Cells[j,i] := Format('%10.4f',[rr[i,j]]);
        end;
      edit_r0_tot.Text := Format('%10.4g',[r0_tot]);
      edit_tr0.Text := Format('%10.4g',[tr0]);
      with stringgrid_tbar do
        begin
          FixedCols := 1;
          FixedRows := 1;
          ColCount  := psite + 1;
          RowCount  := psite + 1;
          for i := 1 to psite do Cells[i,0] := IntToStr(i);
          for i := 1 to psite do Cells[0,i] := IntToStr(i);
          for i := 1 to psite do
            for j := 1 to size do
              Cells[j,i] := Format('%10.4f',[tt[i,j]]);
        end;
      edit_tbar_tot.Text := Format('%10.4g',[tbar_tot]);
    end;
end;

procedure tform_multi.FormCreate(Sender: TObject);
begin
  Left   := 28;
  Top    := 0;
  Height := 674;
  Width  := 980;
  adjust(self);
  Caption := 'MULTISITE';
  x_mat := 0;
  psite := 0;
end;

procedure Tform_multi.init_multi;
var i,p : integer;
begin
  init_form(form_multi);
  x_mat := 0;
  psite := 0;
  for i := 1 to modele_nb do
    if ( modele[i].xmat <> 0 ) then
      begin
        x_mat := modele[i].xmat;
        break;
      end;
  if ( x_mat = 0 ) then exit;
  with mat[x_mat] do if matmultisite(size,val,p) then psite := p;
end;

procedure tform_multi.Edit_matrixReturnPressed(Sender: TObject);
var x,tx : integer;
begin
  trouve_obj(tronque(minuscule(edit_matrix.Text)),x,tx);
  if ( x = 0 ) or ( tx <> type_mat ) then
    begin
      erreur('Unknown matrix name');
      edit_matrix.Text := s_ecri_mat(x_mat);
      exit;
    end;
  x_mat := x;
  FormActivate(nil);
end;

procedure tform_multi.Edit_psiteReturnPressed(Sender: TObject);
var p : integer;
begin
  if ( x_mat = 0 ) then exit;
  if not est_entier(edit_psite.Text,p) then
    begin
      erreur('Number of sites: integer expected');
      edit_psite.Text := IntToStr(psite);
      exit;
    end;
  if ( p < 1 ) then
    begin
      erreur('Number of sites: positive integer expected');
      edit_psite.Text := IntToStr(psite);
      exit;
    end;
  with mat[x_mat] do
    if not test_matmultisite(size,p,val) then
      begin
        erreur('Matrix is not multisite for ' + IntToStr(p) + ' sites');
        edit_psite.Text := IntToStr(psite);
        exit;
      end;
  psite := p;
  FormActivate(nil);
end; 

end.
